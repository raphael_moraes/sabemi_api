﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NetEscapades.Extensions.Logging.RollingFile;

namespace Sabemi.IoC
{
    public class LoggingConfig
    {
        private readonly IConfigurationSection _settings;

        public LoggingConfig(IConfigurationSection loggingSection, ILoggingBuilder builder)
        {
            _settings = loggingSection;
            BuildLogger(builder);
        }

        private void BuildLogger(ILoggingBuilder builder)
        {
            builder.ClearProviders()
                .AddConfiguration(_settings)
                .AddFile(options => ConfigureFileLogger(options));
        }

        private void ConfigureFileLogger(FileLoggerOptions options)
        {
            options.FileSizeLimit = _settings.GetValue("LogFileSizeLimitMb", 20) * 1024 * 1024;
            options.RetainedFileCountLimit = _settings.GetValue("LogFileCountLimit", 10);
            options.LogDirectory = _settings.GetValue("LogDirectory", "Logs");
            options.Extension = _settings.GetValue("LogFileExtension", "txt");
        }
    }
}
