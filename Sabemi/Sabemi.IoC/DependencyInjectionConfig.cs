﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Sabemi.Data.Repository;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services;
using Sabemi.Domain.Interfaces.Services.Admin;
using Sabemi.Services;
using Sabemi.Services.Admin;
using Sabemi.Services.Auth;
using Sabemi.Services.Validator;

namespace Sabemi.IoC
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            //Admin Repositories
            services.AddScoped<IAdminUserRepository, AdminUserRepository>();
            services.AddScoped<IPortalAccessRepository, PortalAccessRepository>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IAssistedModeRepository, AssistedModeRepository>();
            services.AddScoped<IPortalClientRepository, PortalClientRepository>();
            services.AddScoped<IWhiteListRepository, WhiteListRepository>();
            services.AddScoped<IBlockedAccessRepository, BlockedAccessRepository>();
            services.AddScoped<IAdminConfigurationRepository, AdminConfigurationRepository>();

            //Notification Pattern
            services.AddScoped<INotification, Notification>();

            //Portal Services
            services.AddScoped<IUserService, UserService>();            
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IContractService, ContractService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IContactService, ContactService>();

            //Admin Services
            services.AddScoped<IAdminUserService, AdminUserService>();
            services.AddScoped<IAdminLoginService, AdminLoginService>();
            services.AddScoped<IPortalAccessService, PortalAccessService>();
            services.AddScoped<IAssistedModeService, AssistedModeService>();
            services.AddScoped<IPortalClientService, PortalClientService>();
            services.AddScoped<IWhiteListService, WhiteListService>();
            services.AddScoped<IBlockedAccessService, BlockedAccessService>();
            services.AddScoped<IAdminConfigurationService, AdminConfigurationService>();

            //Authorization
            services.AddScoped<IAuthUser, AuthUser>();
            services.AddScoped<IAuthorizationManager, AuthorizationManager>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHttpClient<ICarinaService, CarinaService>();

            return services;
        }
    }
}
