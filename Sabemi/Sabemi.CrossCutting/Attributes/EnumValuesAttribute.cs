﻿using System;

namespace Sabemi.CrossCutting.Attributes
{
    public class EnumValuesAttribute : Attribute
    {
        public string Code { get; set; }
        public string Text { get; set; }
        public string[] Codes { get; set; }
        public string Url { get; set; }

        public EnumValuesAttribute(string code, string text)
        {
            Codes = code.Split(",");
            Code = Codes[0];
            Text = text;
        }

        public EnumValuesAttribute(string code, string text, string url) : this(code, text)
        {
            Url = url;
        }
    }
}
