﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace System
{
    public static class StringExtensions
    {
        public static string Replace(this string value, string[] oldValues, string newValue)
        {
            string retorno = value;
            foreach (string oldValue in oldValues)
            {
                retorno = retorno.Replace(oldValue, newValue);
            }
            return retorno;
        }

        public static string ToCapital(this string value)
        {
            string result = "";
            foreach (var word in value.Split(' '))
            {
                result += word.Substring(0, 1).ToUpper() + (word.Length > 1 ? word.Substring(1).ToLower() : "") + ' ';
            }
            return result.TrimEnd();
        }

        public static string ToCapitalInvariant(this string value)
        {
            string result = "";
            foreach (var word in value.Split(' '))
            {
                result += word.Substring(0, 1).ToUpperInvariant() + (word.Length > 1 ? word.Substring(1).ToLowerInvariant() : "") + ' ';
            }
            return result.TrimEnd();
        }

        public static string FirstNameOnly(this string value)
        {
            return value.Split(' ')[0].ToCapital();
        }

        public static string OnlyNumbers(this string value)
        {
            return Regex.Replace(value, "\\D", "");
        }

        public static string CellPhoneMask(this string value, bool hide)
        {
            value = value.OnlyNumbers();
            string hidden5 = "*****";
            string hidden4 = "****";

            if (value.Length == 8) return $"{(hide ? hidden4 : value.Substring(0, 4))} {value.Substring(4, 4)}";
            if (value.Length == 9) return $"{(hide ? hidden5 : value.Substring(0, 5))} {value.Substring(5, 4)}";
            if (value.Length == 10) return $"({value.Substring(0, 2)}) {(hide ? hidden4 : value.Substring(2, 4))} {value.Substring(6, 4)}";
            if (value.Length == 11) return $"({value.Substring(0, 2)}) {(hide ? hidden5 : value.Substring(2, 5))} {value.Substring(7, 4)}";
            return value;
        }

        public static string NullIfInterrogation(this string value)
        {
            return value == "?" ? null : value;
        }

        public static string Encrypt(this string value, string key = null, string iv = null)
        {
            if (key == null)
            {
                key = "";
            }
            key += "@!543eM1!";
            string encrypted = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                byte[] _keyByte = Encoding.UTF8.GetBytes(key.Substring(0, 8));
                byte[] _ivByte = { 0x01, 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78 };
                if (!string.IsNullOrEmpty(iv))
                {
                    _ivByte = Encoding.UTF8.GetBytes(iv.Substring(0, 8));
                }
                using DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                using MemoryStream ms = new MemoryStream();
                using CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(_keyByte, _ivByte), CryptoStreamMode.Write);
                byte[] inputByteArray = Encoding.UTF8.GetBytes(value);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                encrypted = Convert.ToBase64String(ms.ToArray());
            }
            return encrypted;
        }

        public static string Decrypt(this string value, string key = null, string iv = null)
        {
            if (key == null)
            {
                key = "";
            }
            key += "@!543eM1!";
            string decrypted = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                byte[] _keyByte = Encoding.UTF8.GetBytes(key.Substring(0, 8));
                byte[] _ivByte = { 0x01, 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78 };
                if (!string.IsNullOrEmpty(iv))
                {
                    _ivByte = Encoding.UTF8.GetBytes(iv.Substring(0, 8));
                }
                using DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                using MemoryStream ms = new MemoryStream();
                using CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(_keyByte, _ivByte), CryptoStreamMode.Write);
                byte[] inputByteArray = Convert.FromBase64String(value);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                decrypted = encoding.GetString(ms.ToArray());
            }
            return decrypted;
        }
    }
}
