﻿using Sabemi.CrossCutting.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace System
{
    public static class EnumExtensions
    {

        public static string Description(this IConvertible value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());

            try
            {
                var attributes = (DescriptionAttribute[]) fieldInfo
                    .GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
                else
                    return value.ToString();
            }
            catch
            {
                // TODO implementar logging
                return value.ToString();
            }
        }

        public static async Task<string> DescriptionAsync(this IConvertible value)
        {
            return await Task.Run(() => 
            {
                return Description(value);
            });
        }

        public static string Name(this Enum value)
        {
            return value.ToString();
        }

        public static int ValueAsInt(this Enum value)
        {
            return Convert.ToInt32(value);
        }

        public static char ValueAsChar(this Enum value)
        {
            return Convert.ToChar(value);
        }

        public static T FromChar<T>(string caracter) where T : struct, IConvertible
        {
            return FromChar<T>(caracter[0]);
        }

        public static T FromChar<T>(char caracter) where T : struct, IConvertible
        {
            try
            {
                if (Enum.IsDefined(typeof(T), Convert.ToInt32(caracter)))
                {
                    return (T)Enum.ToObject(typeof(T), Convert.ToInt32(caracter));
                }
                else
                {
                    return (T)(Enum.GetValues(typeof(T)).GetValue(0));
                }
            }
            catch
            {
                // TODO implementar logging
                return (T)(Enum.GetValues(typeof(T)).GetValue(0));
            }


        }

        public static T ToEnum<T>(this int numero) where T : struct, IConvertible
        {
            try
            {
                if (Enum.IsDefined(typeof(T), numero))
                {
                    return (T)Enum.ToObject(typeof(T), numero);
                }
                else
                {
                    return (T)(Enum.GetValues(typeof(T)).GetValue(0));
                }
            }
            catch
            {
                // TODO implementar logging
                return (T)(Enum.GetValues(typeof(T)).GetValue(0));
            }
        }

        public static T FromString<T>(string nome) where T : struct, IConvertible
        {
            try
            {
                return Enum.Parse<T>(nome.Replace(" ", ""), true);
            }
            catch
            {
                // TODO implementar logging
                return (T)(Enum.GetValues(typeof(T)).GetValue(0));
            }

        }       

        public static T FromDescription<T>(string nome) where T : struct, IConvertible
        {
            try
            {
                return GetItens<T>().FirstOrDefault(x => x.Description() == nome);
            }
            catch
            {
                // TODO implementar logging
                return (T)(Enum.GetValues(typeof(T)).GetValue(0));
            }
        }

        public static List<T> GetItens<T>() where T : struct, IConvertible
        {
            return Enum.GetNames(typeof(T)).Select(e => FromString<T>(e)).ToList();

        }

        public static string ValueAsString(this Enum value)
        {
            return value.ValueAsChar().ToString();
        }        

        public static T? ToEnum<T>(this string code) where T : struct, IConvertible
        {
            Array allValues = Enum.GetValues(typeof(T));

            foreach (var value in allValues)
            {
                if (code == GetCode((T?)value, code))
                    return (T)value;

                if (code.ToUpper() == GetText((T?)value).ToUpper())
                    return (T)value;
            }            

            return null;
        }

        public static string GetCode<T>(this T? @enum) where T : struct, IConvertible
        {
            if (!@enum.HasValue)
                return string.Empty;

            EnumValuesAttribute enumAttribute = GetAttribute(@enum);
            return enumAttribute.Code;
        }

        public static string GetCode<T>(this T? @enum, string value) where T : struct, IConvertible
        {
            EnumValuesAttribute enumAttribute = GetAttribute(@enum);
            if (enumAttribute.Codes != null && enumAttribute.Codes.Contains(value))
            {
                return enumAttribute.Codes.Where(x => x.Equals(value)).FirstOrDefault();
            }
            else
                return enumAttribute.Code;
        }

        public static string GetText<T>(this T? @enum) where T : struct, IConvertible
        {
            if (!@enum.HasValue)
                return string.Empty;

            EnumValuesAttribute enumAttribute = GetAttribute(@enum);
            return enumAttribute.Text;
        }

        public static string GetUrl<T>(this T? @enum) where T : struct, IConvertible
        {
            if (!@enum.HasValue)
                return string.Empty;

            EnumValuesAttribute enumAttribute = GetAttribute(@enum);
            return enumAttribute.Url;
        }

        public static IList<T> GetAll<T>() where T : struct, IConvertible
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }

        private static EnumValuesAttribute GetAttribute<T>(T? result) where T : struct, IConvertible
        {            
            MemberInfo memberInfo = GetMemberInfo(result);
            return (EnumValuesAttribute)Attribute.GetCustomAttribute(memberInfo, typeof(EnumValuesAttribute));
        }

        private static MemberInfo GetMemberInfo<T>(T? result) where T : struct, IConvertible
        {
            MemberInfo memberInfo = typeof(T).GetField(Enum.GetName(typeof(T), result));

            return memberInfo;
        }
    }
}
