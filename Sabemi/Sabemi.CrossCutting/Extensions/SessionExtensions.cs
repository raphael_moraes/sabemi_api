﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace System
{
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static void Add(this ISession session, string sessionKey, string key, string value)
        {
            var sessionValue = session.Get<IDictionary<string, string>>(sessionKey);

            if (sessionValue == null) sessionValue = new Dictionary<string, string>();

            if (sessionValue.ContainsKey(key))
                sessionValue[key] = value;
            else 
                sessionValue.Add(key, value);

            session.Set(sessionKey, sessionValue);
        }

        /// <summary>
        /// Extrai o item serializado na sessão na forma de um dictionary, e retorna o valor
        /// contido na chave fornecida.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="sessionKey">A chave da sessão que contém um dictionary serializado</param>
        /// <param name="key">A chave do dictionary que contém o valor requerido</param>
        /// <returns>O valor contido no dictionary serializado na sessão</returns>
        public static string Get(this ISession session, string sessionKey, string key)
        {
            var sessionValue = session.Get<IDictionary<string, string>>(sessionKey);
            return sessionValue != null && sessionValue.ContainsKey(key) ? sessionValue[key] : null;
        }

        public static void Remove(this ISession session, string sessionKey, string key)
        {
            var sessionValue = session.Get<IDictionary<string, string>>(sessionKey);
            if (sessionValue.ContainsKey(key))
                sessionValue.Remove(key);
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default :
                JsonConvert.DeserializeObject<T>(value);
        }
    }
}
