﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class ListExtensions
    {
        public static IList<T> PickRandom<T>(this IList<T> list, Random randomizer, int amount = 1) where T : class
        {
            IList<T> picked = new List<T>(amount);
            var limit = list.Count - 1;
            for (int i = 0; i < amount; i++)
            {
                T next;
                do
                {
                    next = list[randomizer.Next(limit)];
                }
                while (picked.Contains(next));

                picked.Add(next);
            }
            return picked;
        }
    }
}
