﻿using System.Threading;
using System.Threading.Tasks;

namespace Sabemi.CrossCutting.Util
{
    public abstract class CpfUtil
    {
        public static async Task<bool> IsValidAsync(string cpf, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                cpf = cpf.Trim();
                cpf = cpf.Replace(".", "").Replace("-", "");

                if (cpf.Length != 11)
                    return false;

                int[] firstMultiplier = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
                int[] secondMultiplier = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
                string tempCpf = cpf.Substring(0, 9);
                int sum = 0;

                for (int i = 0; i < 9; i++)
                    sum += int.Parse(tempCpf[i].ToString()) * firstMultiplier[i];

                int rest = sum % 11;

                if (rest < 2)
                    rest = 0;
                else
                    rest = 11 - rest;

                string digit = rest.ToString();
                tempCpf += digit;
                sum = 0;

                for (int i = 0; i < 10; i++)
                    sum += int.Parse(tempCpf[i].ToString()) * secondMultiplier[i];

                rest = sum % 11;

                if (rest < 2)
                    rest = 0;
                else
                    rest = 11 - rest;

                digit += rest.ToString();
                return cpf.EndsWith(digit);
            });
        }
    }
}
