﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sabemi.CrossCutting.Util
{
    public class JsonUtil
    {
        public static JObject ReadJson(string path)
        {
            var stream = new FileStream(path, FileMode.Open);
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                var json = reader.ReadToEnd();
                return JObject.Parse(json);
            }
        }
    }
}
