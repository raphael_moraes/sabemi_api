﻿using Sabemi.CrossCutting.Attributes;
using System.ComponentModel;

namespace Sabemi.CrossCutting.Util
{
    public class Enumerator
    {
        #region constants ids        
        private const string DEATHCODES = "1,5,6,22,23,26,28,29,30,31,32,33,35,36,37,38,39,40,42,44,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67";
        private const string PECULIOCODES = "2,3,4,70";
        private const string INVALIDITYCODES = "8,9,85";
        private const string FUNERALCODES = "20,45,46";
        private const string RAFFLECODES = "24,34,41,43,71,81,82,89";

        private const string DOCUMENT_ICON_CODES = "1,2,3,4,5,6,7,8,9,10,10,12,13,14,15,16,17,18,19,22,23,24,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,446,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,67,68,69,70,71,72,74,75,76,77,79,80";
        private const string CLOVER_ICON_CODES = "81,82,83,84,85,86,87,88,89";

        private const string TO_KNOW_MORE_LINK_CODES = DEATHCODES + "," + RAFFLECODES + ",68,69";
        private const string BENEFIT_LINK_CODES = FUNERALCODES + ",21,100";
        private const string ASSISTANCE_LINK_CODES = "73,86,87,88,92,93";
        #endregion

        public enum Sex
        {
            [EnumValues("M", "Masculino")]
            Male = 1,
            [EnumValues("F", "Feminino")]
            Female = 2
        }

        public enum MaritalStatus
        {            
            [EnumValues("0", "Casado")]
            Married,
            [EnumValues("1", "Solteiro")]
            Single,
            [EnumValues("2", "Divorciado")]
            Divorced,
            [EnumValues("3", "Divorciado Judicialmente")]
            JudiciallyDivorced,
            [EnumValues("4", "Viúvo")]
            Widowed,
            [EnumValues("5", "Outros")]
            Other
        }

        public enum ContactType
        {
            [EnumValues("4", "")]
            Email = 4,
            [EnumValues("6", "")]
            Telephone = 6,
            [EnumValues("1,8", "")]
            Cellphone = 8
        }

        public enum FavoriteContactType
        {
            [EnumValues("1", "E-mail")]
            Email,
            [EnumValues("2", "SMS")]
            SMS,
            [EnumValues("3", "WhatsApp")]
            WhatsApp,
            [EnumValues("4", "Telefone")]
            Telephone, 
            [EnumValues("5", "Celular")]
            Cellphone
        }

        public enum ProductType
        {
            [EnumValues("1", "#003153")]
            Previdencia,
            [EnumValues("2", "#ff7518")]
            Emprestimo, 
            [EnumValues("3", "#00adba")]
            Seguro
        }

        public enum Icons
        {
            [EnumValues(DOCUMENT_ICON_CODES, "document")]
            Document,
            [EnumValues(CLOVER_ICON_CODES, "clover")]
            Clover,
            [EnumValues("20,100", "headstone")]
            Headstone,
            [EnumValues("21,78", "apple")]
            Apple,
            [EnumValues("25", "medicine")]
            Medicine,
            [EnumValues("73", "home")]
            Home,
            [EnumValues("92", "washing-machine")]
            WashingMachine,
            [EnumValues("93", "pet-footprint")]
            Pet,
            [EnumValues("95", "male-doctor")]
            MaleDoctor
        }

        public enum Links
        {
            [EnumValues(TO_KNOW_MORE_LINK_CODES, "0800 880 1900")]
            [Description("Para saber mais, ligue para: ")]
            To_Know_More,
            [EnumValues(BENEFIT_LINK_CODES, "0800 770 8916")]
            [Description("Para acionar este benefício, ligue para: ")]
            Benefits,
            [EnumValues("25", "", "https://www.epharma.com.br")]
            [Description("Para consultar a rede credenciada, acesse: ")]
            MedicineDiscount, 
            [EnumValues(ASSISTANCE_LINK_CODES, "0800 770 8916")]
            [Description("Para acionar esta assistência, ligue para: ")]
            Assistance,
            [EnumValues("77", "0800 880 1900")]
            [Description("Para acionar esta cobertura, ligue para: ")]
            HospitalExpenses,
            [EnumValues("78", "0800 770 8916")]
            [Description("Para acionar esta cobertura, ligue para: ")]
            NutritionalAssistance,
            [EnumValues("95", "0800 880 1900", "https://www.meutem.com.br")]
            [Description("Para acionar esta assistência, ligue para: ")]
            TEMSaude
        }

       
        public enum BenefitType
        {            
            [EnumValues(DEATHCODES, "Proteção por Morte Acidental")]
            [Description("Indenização para seus beneficiários em caso de falecimento por acidente pessoal coberto.")]
            Morte,

            [EnumValues(PECULIOCODES, "Pecúlio")]
            [Description("Indenização para seus beneficiários em caso de falecimento por qualquer causa.")]
            Peculio,

            [EnumValues("7", "Proteção por Morte")]
            [Description("Indenização para seus beneficiários em caso de falecimento por qualquer causa.")]
            MorteQualquerCausa,

            [EnumValues(INVALIDITYCODES, "Invalidez Permanente Total ou Parcial por Acidente (IPA)")]
            [Description("Indenização para você em caso de invalidez permanente total ou parcial causada por acidente pessoal coberto.")]
            InvalidezPorAcidente,

            [EnumValues("10", "Invalidez Permanente Total por Doença - IPD")]
            [Description("Indenização para seus beneficiários em caso de falecimento por acidente pessoal coberto.")]
            Indenizacao,

            [EnumValues("11", "Indenização Especial de Morte por Acidente (IEA)")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            InvalidezPorDoenca,

            [EnumValues("12", "Vida Individual Resgatável")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            Vida,

            [EnumValues("13", "Saúde")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            Saude,

            [EnumValues("14", "Mensalidade Social")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            Mensalidade,

            [EnumValues("15", "Automóvel")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            Automovel,

            [EnumValues("16,17", "Pensão")]
            [Description("Pagamento de uma renda mensal aos beneficiários, por tempo determinado em contrato, a partir do mês seguinte ao falecimento do participante.")]
            Pensao,

            [EnumValues("18,19", "Aposentadoria")]
            [Description("Pagamento de uma renda mensal para você, após completar o período mínimo de pagamento das contribuições definido na contratação do plano.")]
            Aposentadoria,

            [EnumValues(FUNERALCODES, "Assistência Funeral 24h")]
            [Description("Os trâmites do funeral do Segurado são providenciados de forma rápida e sem burocracias.")]
            Funeral,            
           
            [EnumValues("21", "Assistência Alimentação")]
            [Description("No caso de falecimento do segurado, o beneficiário recebe por 6 meses, um cartão alimentação para auxiliar nas despesas.")]
            Alimentacao, 

            [EnumValues(RAFFLECODES, "Sorteio Mensal")]
            [Description("Com o pagamento do seu seguro em dia, você concorre a sorteios todos os meses pela Loteria Federal.")]
            Sorteio,

            [EnumValues("25", "Desconto em Medicamentos")]
            [Description("O segurado poderá usufruir de descontos de 15% a 60% em medicamentos listados na rede credenciada e-pharma.")]
            DescontoMedicamentos,

            [EnumValues("27", "Mensalidade CENTRAPE")]
            [Description("Não existe mais clientes ativos na CENTRAPE...")]
            Centrape,

            [EnumValues("68,69", "Sorteio Mensal")]
            [Description("Com o pagamento do seu seguro em dia, você concorre a sorteios todos os meses pela Loteria Federal.")]
            OutrasReceitas,

            [EnumValues("72", "Mensalidade AAPU")]
            [Description("Não trabalhamos mais com produtos de associação.")]
            AAPU,

            [EnumValues("73", "Assistência Residencial")]
            [Description("Apoio 24h para emergências domésticas como:  eletricista, chaveiro, bombeiro hidráulico entre outros.")]
            AssistenciaResidencial,

            [EnumValues("74", "Prestamista")]
            [Description(@"Garantia de  pagamento do Capital Segurado, destinado a amortizar a dívida contraída ou compromisso assumido com o Credor, caso o Segurado venha a falecer por causas naturais 
                           ou acidentais durante a vigência do Seguro.")]
            Prestamista,

            [EnumValues("75", "Invalidez Permanente Total por Acidente (IPTA)")]
            [Description(@"Garantia de pagamento do Capital Segurado, destinado a amortizar a dívida contraída ou compromisso assumido com o Credor, em decorrência de acidente pessoal coberto com o Segurado, 
                           em que cause a perda ou a impotência funcional definitiva total de um membro ou órgão, por lesão física, ocorrido durante a vigência da cobertura individual.")]
            IPTA,

            [EnumValues("76", "Invalidez Permanente Total por Acidente (IPA)")]
            [Description("Indenização para você em caso de invalidez permanente total causada por acidente pessoal coberto.")]
            IPA,

            [EnumValues("77", "Despesas Médicas e Hospitalares decorrentes de Queimaduras e Fraturas")]
            [Description("Reembolso de despesas efetuadas para seu tratamento, sob orientação médica, em casos de queimaduras ou fraturas originadas em função de um acidente pessoal coberto.")]
            DespesaHospitalar,

            [EnumValues("78", "Assistência Nutricional")]
            [Description("Orientações sobre nutrição e qualidade de vida, além de esclarecimentos de dúvidas sobre alimentação adequada e como manter uma rotina saudável.")]
            AssistenciaNutricional,

            [EnumValues("79", "Assistência Viagens")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            AssistenciaViagem,

            [EnumValues("80", "Morte por Qualquer Coisa - Educacional")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            QualquerMorte,

            [EnumValues("83", "Diárias por Internação Hospitalar por Acidente Pessoal")]
            [Description("Pagamento de uma diária segurada para você, de acordo com o valor e quantidade de diárias contratados, para cada dia de internação hospitalar decorrente de acidente pessoal coberto.")]
            DiariaInternacao,

            [EnumValues("84", "Mensalidade PAMPA Benefícios")]
            [Description("Entre em contato com o nosso SAC para saber mais informações sobre esse benefício.")]
            PAMPA,

            [EnumValues("86", "Assistência Inspeção Senior")]
            [Description(@"Serviços residenciais, tais como: instalação de barra de segurança no banheiro e de assento no box, instalação de luz de emergência nos cômodos, construção de rampa de acesso à residência, 
                           dicas sobre cuidados domésticos, etc.")]
            AssistenciaInspecaoSenior,

            [EnumValues("87", "Assistência Help Desk")]
            [Description("Suporte, por telefone ou acesso remoto, para instalação de computadores, orientação sobre equipamentos e softwares e outras facilidades.")]
            HelpDesk,

            [EnumValues("88", "Assistência Linha Branca")]
            [Description("Assistência para conserto de eletrodomésticos, como: fogão, geladeira, micro-ondas, lavadoura de roupas, entre outros.")]
            AssistenciaLinhaBranca,

            [EnumValues("92", "Assistência Linha Marrom")]
            [Description("Assistência para conserto de eletronicos, como: televisão, DVD, aparelhos de som, entre outros.")]
            AssistenciaLinhaMarrom,

            [EnumValues("93", "Assistência PET")]
            [Description("Assistência emergencial 24h a animais domésticos (cães e gatos).")]
            AssistenciaPet,

            [EnumValues("95", "Assistência TEM Saúde")]
            [Description("O segurado poderá aproveitar descontos de 20% à 80% na compra de medicamentos, e até 70% de economia em consultas, exames médicos e odontológicos, além de orientação de saúde 24h.")]
            PlanoTEMSaude,

            [EnumValues("100", "Assistência Funeral Familiar")]
            [Description("Os trâmites do funeral do Segurado e de seus dependentes (cônjuge e filho até 21 anos) são providenciados de forma rápida e sem burocracias.")]
            AssistenciaFuneralFamiliar
        }

        public enum PaymentType
        {
            [EnumValues("0", "Débito em Conta")]
            Debit,
            [EnumValues("1", "Cartão de Crédito")]
            Credit_Card,
            [EnumValues("2", "Desconto em Folha")]
            PayrollDiscount
        }

        public enum AccountType
        {
            ContaCorrente = 1, 
            Poupanca = 2
        }

        public enum Bank
        {
            Caixa = 1, 
            Santander = 2, 
            Itau = 3, 
            Basa = 4
        }

        public enum CasualtyType
        {
            [EnumValues("1", "Morte Acidental")]
            AccidentalDeath,
            [EnumValues("2", "Morte Natural")]
            NaturalDeath,
            [EnumValues("3", "Despesas Médicas Hospitalares e Odontológicas")]
            DMHO,
            [EnumValues("4", "Assistência Funeral")]
            FuneralAssistance
        }

        public enum State
        {
            [EnumValues("AC", "Acre")]
            AC,
            [EnumValues("AL", "Alagoas")]
            AL,
            [EnumValues("AP", "Amapá")]
            AP,
            [EnumValues("AM", "Amazonas")]
            AM,
            [EnumValues("BA", "Bahia")]
            BA,
            [EnumValues("CE", "Ceará")]
            CE,
            [EnumValues("DF", "Distrito Federal")]
            DF,
            [EnumValues("ES", "Espírito Santo")]
            ES,
            [EnumValues("GO", "Goiás")]
            GO,
            [EnumValues("MA", "Maranhão")]
            MA,
            [EnumValues("MT", "Mato Grosso")]
            MT,
            [EnumValues("MS", "Mato Grosso do Sul")]
            MS,
            [EnumValues("MG", "Minas Gerais")]
            MG,
            [EnumValues("PA", "Pará")]
            PA,
            [EnumValues("PB", "Paraíba")]
            PB,
            [EnumValues("PR", "Paraná")]
            PR,
            [EnumValues("PE", "Pernambuco")]
            PE,
            [EnumValues("PI", "Piauí")]
            PI,
            [EnumValues("RJ", "Rio de Janeiro")]
            RJ,
            [EnumValues("RN", "Rio Grande do Norte")]
            RN,
            [EnumValues("RS", "Rio Grande do Sul")]
            RS,
            [EnumValues("RO", "Rondônia")]
            RO,
            [EnumValues("RR", "Roraima")]
            RR,
            [EnumValues("SC", "Santa Catarina")]
            SC,
            [EnumValues("SP", "São Paulo")]
            SP,
            [EnumValues("SE", "Sergipe")]
            SE,
            [EnumValues("TO", "Tocantins")]
            TO
        }

        public enum QuestionType
        {
            motherName,
            fatherName,
            homeTown,
            age,
            birthDay
        }        

        public enum Error
        {
            [Description("Ocorreu um erro inesperado. Por favor, tente novamente mais tarde.")]
            Internal,
            [Description("Não foi possível atender a sua solicitação. Por favor, tente novamente mais tarde.")]
            Carina
        }

        public enum AdminMessageType
        {
            Empty,
            Info,
            Error,
            Success
        }

        public enum SolicitationType
        {
            [EnumValues("1", "Suspeita de Fraude")]
            SuspicionOfFraude,
            [EnumValues("2", "Corrupção")]
            Corruption,
            [EnumValues("3", "Lavagem de Dinheiro")]
            MoneyLaundry,
            [EnumValues("4", "Más Práticas")]
            BadPractices,
            [EnumValues("5", "Outros")]
            Other,
            [EnumValues("6", "Dúvida")]
            Doubt
        }        

        public enum FAQCategory
        {
            Documentation = 1, 
            PersonalInsurance = 2, 
            FinancialAssistance = 3
        }

        public enum FAQSubCategory
        {
            NaturalDeath = 1, 
            AccidentalDeath = 2,
            Redemption = 3
        }

        public enum ProfileEnum
        {
            [Description("Admin")]
            Admin = 1,
            [Description("Usuário do Portal")]
            Portal = 2,
            [Description("Modo Assistido")]
            Assisted_Mode = 3,
            [Description("Master")]
            Master = 4
        }

        public enum GrantType
        {
            [EnumValues("1", "password")]
            Password, 
            [EnumValues("2", "refresh_token")]
            RefreshToken
        }

        public enum OperationType
        {
            Beneficiary = 1, 
            ContactUs = 2, 
            ComplaintPortal = 3, 
            CasualtyNotification = 4
        }

        public enum CertificateType 
        {
            SemCertificado = 0,
            AcidentesPessoaisCentrape = 1,
            ApProtege = 2,
            APProtegePlus = 3,
            CertificadoSorteioInvest = 4,
            CertificadoSorteioZurich = 5,
            CertificadoSorteioInvestAPBEMESTAR = 6,
            IndivSSAcidentesPessoais = 7,
            IndivSSVidaemGrupo = 8,
            INVALIDEZPERMTOTAL = 9,
            PECULIONOVOPUPP = 10,
            PeculioNOVOSPP = 11,
            PeculioSPP = 12,
            PeculioSS = 13,
            PensaoSS = 14,
            PensaoSPP = 15,
            SorteioCentrape = 16,
            SSAcidentesPessoaisCOSSEGURO = 17,
        }
    }
}
