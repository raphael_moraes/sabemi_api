﻿namespace Sabemi.CrossCutting.Util
{
    public abstract class RegexpUtil
    {
        public static string Email => @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";
        public static string Cellphone => @"(\(\d{2}\) ?)?(\d{4,5}) ?(\d{4})";
        public static string Telephone => @"(\(\d{2}\) ?)?(\d{4}) ?(\d{4})";
        public static string CEP => @"(\d{5})-(\d{3})";
    }
}
