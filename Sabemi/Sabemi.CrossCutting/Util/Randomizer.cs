﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sabemi.CrossCutting.Util
{
    public static class Randomizer
    {
        public static Random Random 
        { 
            get
            {
                int seed = DateTime.Now.Ticks.GetHashCode();
                return new Random(seed);
            }
        }

        public static IList<string> RandomAges(int amount = 1, int except = 0)
        {
            int min = 18;
            int max = 60;
            if (except != 0)
            {
                min = except < 28 ? 18 : except - 10;
                max = except > 80 ? 90 : except + 10;
            }
            return PickRandom(min, max, amount, except.ToString());
        }

        public static IList<string> RandomMonthDays(int amount = 1, int except = 0)
        {
            return PickRandom(1, 31, amount, except.ToString());
        }

        public static IList<string> RandomMaleNames(int amount = 1, string except = null)
        {
            return RandomNames("male", amount, except, "names");
        }

        public static IList<string> RandomFemaleNames(int amount = 1, string except = null)
        {
            return RandomNames("female", amount, except, "names");
        }

        public static IList<string> RandomCityNames(int amount = 1, string except = null)
        {
            return RandomNames("city", amount, except, "names");
        }


        private static IList<string> RandomNames(string category, int amount, string except, string file)
        {
            var json = JsonUtil.ReadJson($"Files/{file}.json");
            var names = json.SelectToken(category).Values<string>().ToList();

            if (!string.IsNullOrEmpty(except))
            {
                names.Remove(except);
            }

            return names.PickRandom(Random, amount);
        }

        private static IList<string> PickRandom(int min, int max, int amount, string except)
        {
            IList<string> picked = new List<string>(amount);
            for (int i = 0; i < amount; i++)
            {
                string next;
                do
                {
                    next = Random.Next(min, max).ToString();
                }
                while (next == except || picked.Contains(next));

                picked.Add(next);
            }
            return picked;
        }

        public static string RandomSMSCode()
        {
            return Random.Next(100000, 999999).ToString();
        }
    }
}
