﻿using AutoMapper;
using Sabemi.Admin.Config.Converters;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;
using WsSabemi;

namespace Sabemi.Admin.Config
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<AdminUserViewModel, AdminUser>().ConvertUsing(typeof(AdminUserViewModelToAdminUser));
            CreateMap<AdminUser, AdminUserViewModel>().ConvertUsing(typeof(AdminUserToAdminUserViewModel));
            CreateMap<List<AdminUser>, List<AdminUserViewModel>>().ConvertUsing(typeof(ListAdminUserToListAdminUserViewModel));
            CreateMap<List<PortalAccess>, List<PortalAccessViewModel>>().ConvertUsing(typeof(ListPortalAccessToListPortalAccessViewModel));

            CreateMap<DadosPortalSegurado, User>()
                .ConvertUsing(typeof(DadosPortalSeguradoToUserConverter));
        }
    }
}
