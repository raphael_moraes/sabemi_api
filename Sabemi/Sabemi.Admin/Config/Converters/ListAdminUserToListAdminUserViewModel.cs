﻿using AutoMapper;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;

namespace Sabemi.Admin.Config.Converters
{
    public class ListAdminUserToListAdminUserViewModel : ITypeConverter<List<AdminUser>, List<AdminUserViewModel>>
    {
        public List<AdminUserViewModel> Convert(List<AdminUser> source, List<AdminUserViewModel> destination, ResolutionContext context)
        {
            destination = new List<AdminUserViewModel>();

            foreach (var user in source)
            {
                destination.Add(new AdminUserToAdminUserViewModel().Convert(user, new AdminUserViewModel(), null));                
            }

            return destination;
        }
    }
}
