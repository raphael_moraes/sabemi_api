﻿using AutoMapper;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;

namespace Sabemi.Admin.Config.Converters
{
    public class ListPortalAccessToListPortalAccessViewModel : ITypeConverter<List<PortalAccess>, List<PortalAccessViewModel>>
    {
        public List<PortalAccessViewModel> Convert(List<PortalAccess> source, List<PortalAccessViewModel> destination, ResolutionContext context)
        {
            destination = new List<PortalAccessViewModel>();
            
            foreach (var item in source)
            {
                destination.Add(new PortalAccessViewModel
                {
                    Id = item.Cod_Id,
                    IP = item.Cod_Ip,
                    CPF = System.Convert.ToUInt64(item.Cod_Cpf).ToString(@"000\.000\.000\-00"),
                    Date = item.Dta_Data.Date,
                    Hour = item.Dta_Data.DateTime.ToShortTimeString(),
                    Blocked = item.Sta_Bloqueado,
                    Situation = item.Dsc_Status.Replace("_", " ").ToLower()
                });
            }

            return destination;
        }
    }
}
