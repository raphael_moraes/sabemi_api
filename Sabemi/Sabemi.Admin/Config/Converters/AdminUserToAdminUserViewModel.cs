﻿using AutoMapper;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using System;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Admin.Config.Converters
{
    public class AdminUserToAdminUserViewModel : ITypeConverter<AdminUser, AdminUserViewModel>
    {
        public AdminUserViewModel Convert(AdminUser source, AdminUserViewModel destination, ResolutionContext context)
        {
            var @enum = EnumExtensions.FromString<ProfileEnum>(source.Cod_Nivel_Acesso);
            
            return destination = new AdminUserViewModel
            {
                Id = source.Cod_Id,
                Login = source.Cod_Login,
                Password = source.Cod_Senha,
                LastAccess = source.Dta_Atualizacao.DateTime,
                Profile = @enum.Description(),
                IdProfile = @enum.ValueAsInt(),
                Profiles = EnumExtensions.GetAll<ProfileEnum>(),
                Active = source.Sta_Ativo, 
                CreatedOn = source.Dta_Criacao.DateTime
            };
        }
    }
}
