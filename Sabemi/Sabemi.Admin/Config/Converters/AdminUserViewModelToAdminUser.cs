﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using System;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Admin.Config.Converters
{
    public class AdminUserViewModelToAdminUser : ITypeConverter<AdminUserViewModel, AdminUser>
    {
        //Mapper apenas para insert
        public AdminUser Convert(AdminUserViewModel source, AdminUser destination, ResolutionContext context)
        {
            var hasher = new PasswordHasher<AdminUserViewModel>();
            source.Password = hasher.HashPassword(source, source.Password);

            return destination = new AdminUser
            {
                Cod_Id = Guid.NewGuid().ToString(),
                Cod_Login = source.Login,
                Cod_Senha = source.Password,
                Dta_Atualizacao = DateTimeOffset.Now,
                Cod_Nivel_Acesso = source.IdProfile.ToEnum<ProfileEnum>().ToString(),
                Dta_Criacao = DateTimeOffset.Now, 
                Sta_Ativo = source.Active
            };
        }
    }
}
