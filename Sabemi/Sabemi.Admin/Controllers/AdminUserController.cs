﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Admin.Controllers
{
    public class AdminUserController : MainController
    {
        private readonly IAdminUserService _service;
        private readonly ILogger<AdminUserController> _logger;
        private readonly IMapper _mapper;

        public AdminUserController(INotification notification,
                                   IAdminUserService userService,
                                   ILogger<AdminUserController> logger,
                                   IMapper mapper) : base(notification)
        {
            _service = userService;
            _logger = logger;
            _mapper = mapper;
        }

        [Route("usuarios")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List(JQueryDataTableParamModel param)
        {
            List<string[]> data = new List<string[]>();
            string filter = param.sSearch ?? string.Empty;
            try
            {
                int start = param.iDisplayStart + 1;
                int end = param.iDisplayStart + param.iDisplayLength;
                var result = _mapper.Map<List<AdminUserViewModel>>(await _service.GetAll(filter, start, end));

                foreach (var item in result)
                {
                    int inactive = item.Active ? 0 : 1;

                    data.Add(new string[]
                    {
                        item.Profile,
                        item.Login,
                        item.LastAccess.ToShortDateString(),
                        item.Id + "#" + inactive.ToString(),
                        item.Id.ToString()
                    });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = data.Count,                
                iTotalDisplayRecords = await _service.GetTotalRecords(filter),
                aaData = data
            });

            return retorno;
        }

        [Route("novo-usuario")]
        public IActionResult Create(AdminUserViewModel model)
        {
            model.Profiles = EnumExtensions.GetAll<ProfileEnum>();
            model.Profile = model.IdProfile.ToEnum<ProfileEnum>().Description();
            return View(model);
        }

        [Route("editar-usuario/{id}")]
        public async Task<IActionResult> Edit(string id)
        {
            var adminUser = _mapper.Map<AdminUserViewModel>(await _service.Get(id));
            if (adminUser == null)
                return NotFound();

            return View(adminUser);
        }

        [HttpPost]
        public async Task<IActionResult> Save(AdminUserViewModel model)
        {
            ModelState.Remove("NewPassword");
            string action = string.IsNullOrEmpty(model.Id) ? "Create" : ModelState.Remove("Password") ? "Edit" : "";            
            if (!ModelState.IsValid)
            {
                Notify("Preencha todas as informações!");
                return CustomRedirect(action, model);
            }

            var admin = new AdminUser();
            if (string.IsNullOrEmpty(model.Id))
                admin = _mapper.Map<AdminUser>(model);
            else
            {
                admin = await _service.Get(model.Id);
                admin.Cod_Login = model.Login;
                admin.Dta_Atualizacao = DateTimeOffset.Now;
                admin.Cod_Nivel_Acesso = model.IdProfile.ToEnum<ProfileEnum>().ToString();
                admin.Sta_Ativo = model.Active;
            }

            if (string.IsNullOrEmpty(model.Id))
                await _service.Insert(admin);
            else await _service.Update(admin);

            if (!CheckOperationSuccess("Dados Salvos com Sucesso"))
                return CustomRedirect(action, model);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task Inactivate(string id, bool inativo)
        {
            var adminUser = await _service. Get(id);
            adminUser.Sta_Ativo = !inativo;
            await _service.Update(adminUser);
        }
    }
}