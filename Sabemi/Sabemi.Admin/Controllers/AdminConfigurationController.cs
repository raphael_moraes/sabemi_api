﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services.Admin;

namespace Sabemi.Admin.Controllers
{
    public class AdminConfigurationController : MainController
    {
        private readonly IAdminConfigurationService _service;

        public AdminConfigurationController(INotification notification, IAdminConfigurationService service) : base(notification)
        {
            _service = service;
        }

        [Route("configuracoes")]
        public async Task<IActionResult> Index()
        {
            var config = await _service.Get();

            return View(config);
        }

        [HttpPost("configuracoes")]
        public async Task<IActionResult> Index(AdminConfiguration configuration)
        {
            await _service.Update(configuration);

            if (!CheckOperationSuccess("Dados Salvos com Sucesso"))
                return CustomView(configuration);

            return RedirectToAction(nameof(Index));
        }
    }
}
