﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services.Admin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Admin.Controllers
{
    public class WhiteListController : MainController
    {
        private readonly ILogger<WhiteListController> _logger;
        private readonly IWhiteListService _service;

        public WhiteListController(INotification notification, ILogger<WhiteListController> logger, IWhiteListService service) : base(notification)
        {
            _logger = logger;
            _service = service;
        }
        
        [HttpGet("whitelist")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List(JQueryDataTableParamModel param)
        {
            IList<string[]> data = new List<string[]>();
            string filter = param.sSearch ?? string.Empty;

            try
            {
                int start = param.iDisplayStart + 1;
                int end = param.iDisplayStart + param.iDisplayLength;

                IList<WhiteList> result = await _service.GetAll(filter, start, end);
                foreach (var item in result)
                {                    
                    data.Add(new string[]
                    {
                        item.Cod_Ip,
                        item.Cod_Descricao
                    });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = await _service.GetTotalRecords(filter),
                iTotalDisplayRecords = data.Count,
                aaData = data
            });

            return retorno;
        }

        [HttpGet("novo_ip_whitelist")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost("novo_ip_whitelist")]
        public async Task<IActionResult> Create(WhiteList whiteList)
        {
            if (!ModelState.IsValid)
            {
                Notify("Preencha todas as informações!");
                return CustomView(whiteList);
            }

            whiteList.Cod_Id = Guid.NewGuid().ToString();
            await _service.Insert(whiteList);
            if (!CheckOperationSuccess("Dados Salvos com Sucesso"))
                return CustomRedirect("Create", whiteList);

            return RedirectToAction(nameof(Index));
        }
    }
}
