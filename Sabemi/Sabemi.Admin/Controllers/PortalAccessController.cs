﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.Admin.Models;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Admin.Controllers
{
    public class PortalAccessController : MainController
    {
        private readonly ILogger<PortalAccessController> _logger;
        private readonly IPortalAccessService _service;
        private readonly IMapper _mapper;

        public PortalAccessController(INotification notification,
                                      ILogger<PortalAccessController> logger,
                                      IPortalAccessService service,
                                      IMapper mapper) : base(notification)
        {
            _logger = logger;
            _service = service;
            _mapper = mapper;
        }

        [Route("acessos_portal")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> AccessList(JQueryDataTableParamModel param)
        {
            List<string[]> data = new List<string[]>();
            string filter = param.sSearch ?? string.Empty;

            try
            {
                int start = param.iDisplayStart + 1;
                int end = param.iDisplayStart + param.iDisplayLength;

                var result = _mapper.Map<List<PortalAccessViewModel>>(await _service.GetAccessPortal(filter, start, end));

                foreach (var item in result)
                {
                    var status = item.Blocked ? 0 : 1;

                    data.Add(new string[]
                    {
                        item.Date.ToShortDateString(),
                        item.Hour,
                        item.CPF,
                        item.IP,
                        item.Id + "#" + status.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = data.Count,
                iTotalDisplayRecords = await _service.GetTotalRecords(filter),
                aaData = data
            });

            return retorno;
        }

        [HttpGet]
        public async Task Blocked(string id, bool inativo)
        {
            var client = await _service.GetClient(id);
            client.Sta_Bloqueado = !inativo;
            await _service.UpdateClient(client);
        }
    }
}
