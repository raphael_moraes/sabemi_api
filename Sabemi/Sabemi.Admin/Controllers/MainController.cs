﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;

namespace Sabemi.Admin.Controllers
{    
    [Authorize]
    public abstract class MainController : Controller
    {
        private readonly INotification _notification;

        protected MainController(INotification notification)
        {
            _notification = notification;
        }

        protected bool IsValidOperation()
        {
            return !_notification.HasNotification();
        }

        protected IActionResult CustomRedirect(string actionName, object routeValues)
        {
            if (!IsValidOperation())
                TempData["Errors"] = _notification.GetNotificationsAsStrings();

            return RedirectToAction(actionName, routeValues);
        }

        protected IActionResult CustomView(object model)
        {
            if (!IsValidOperation())
                TempData["Errors"] = _notification.GetNotificationsAsStrings();

            return View(model);
        }

        protected void NotifyModelStateInvalid(ModelStateDictionary modelState)
        {
            var errors = modelState.Values.SelectMany(e => e.Errors);
            foreach (var error in errors)
            {
                var errorMsg = error.Exception == null ? error.ErrorMessage : error.Exception.Message;
                Notify(errorMsg);
            }
        }

        protected void Notify(string errorMsg)
        {
            _notification.Handle(new Message(errorMsg));
        }

        protected bool CheckOperationSuccess(string successMessage = "", string errorMessage = "", bool errorMessageOverride = false)
        {
            if (!IsValidOperation())
            {
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    if (errorMessageOverride)
                        _notification.ClearNotifications();

                    Notify(errorMessage);
                }
                TempData["Errors"] = _notification.GetNotificationsAsStrings();
                return false;
            }
            else
            {
                TempData["Success"] = successMessage;
                return true;
            }
        }
    }
}