﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.Admin.Models;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services.Admin;

namespace Sabemi.Admin.Controllers
{
    public class BlockedAccessController : MainController
    {
        private readonly ILogger<BlockedAccessController> _logger;
        private readonly IBlockedAccessService _service;

        public BlockedAccessController(INotification notification,
                                       ILogger<BlockedAccessController> logger,
                                       IBlockedAccessService service) : base(notification)
        {
            _logger = logger;
            _service = service;
        }

        [Route("acessos_bloqueados")]
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> List(JQueryDataTableParamModel param)
        {
            List<string[]> data = new List<string[]>();
            string filter = param.sSearch ?? string.Empty;

            try
            {
                int start = param.iDisplayStart + 1;
                int end = param.iDisplayStart + param.iDisplayLength;

                var result = await _service.GetAll(filter, start, end);

                foreach (var item in result)
                {
                    var status = item.Sta_Ativo ? 0 : 1;

                    data.Add(new string[]
                    {
                        item.Dta_Activation_Date.DateTime.ToShortDateString(),
                        item.Dta_Activation_Date.DateTime.ToShortTimeString(),
                        Convert.ToUInt64(item.Cod_Cpf).ToString(@"000\.000\.000\-00"),
                        item.Cod_Ip,
                        string.Empty,
                        item.Cod_Id + "#" + status.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = data.Count,
                iTotalDisplayRecords = await _service.GetTotalRecords(filter),
                aaData = data
            });

            return retorno;
        }

        [HttpGet]
        public async Task Blocked(string id, bool inativo)
        {
            var client = await _service.GetBlockedAccess(id);
            client.Sta_Ativo = !inativo;
            await _service.Update(client);
        }
    }
}
