﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.Admin.Models;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Admin.Controllers
{
    public class AssistedModeController : MainController
    {
        private readonly ILogger<AssistedModeController> _logger;
        private readonly IAssistedModeService _service;

        public AssistedModeController(INotification notification,
                                      ILogger<AssistedModeController> logger,
                                      IAssistedModeService service) : base(notification)
        {
            _logger = logger;
            _service = service;
        }

        [Route("modo_assistido")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List(JQueryDataTableParamModel param)
        {
            IList<string[]> data = new List<string[]>();
            string filter = param.sSearch ?? string.Empty;

            try
            {
                int start = param.iDisplayStart + 1;
                int end = param.iDisplayStart + param.iDisplayLength;

                IList<AssistedMode> result = await _service.GetAll(filter, start, end);
                foreach (var item in result)
                {
                    data.Add(new string[]
                    {
                        item.Cod_Ip,
                        item.Cod_Descricao
                    });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = await _service.GetTotalRecords(filter),
                iTotalDisplayRecords = data.Count,
                aaData = data
            });

            return retorno;
        }

        [Route("novo_ip_modo_assistido")]
        public IActionResult Create()
        {
            return View();
        }

        [Route("novo_ip_modo_assistido")]
        [HttpPost]
        public async Task<IActionResult> Create(AssistedMode assistedMode)
        {
            if (!ModelState.IsValid)
            {
                Notify("Preencha todas as informações!");
                return CustomView(assistedMode);
            }

            assistedMode.Cod_Id = Guid.NewGuid().ToString();
            await _service.Insert(assistedMode);
            if (!CheckOperationSuccess("Dados Salvos com Sucesso"))
                return CustomRedirect("Create", assistedMode);

            return RedirectToAction(nameof(Index));
        }
    }
}
