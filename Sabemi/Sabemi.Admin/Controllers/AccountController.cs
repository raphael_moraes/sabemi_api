﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Sabemi.Admin.Models;
using Sabemi.Domain.Configuration;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Admin.Controllers
{    
    public class AccountController : MainController
    {
        private readonly IAdminLoginService _loginService;
        private readonly IAuthorizationManager _authorization;
        private readonly BaseSettings _appSettings;

        public AccountController(IAdminLoginService loginService,
                                 INotification notification,
                                 IAuthorizationManager authorization, 
                                 IOptions<BaseSettings> appSettings) : base(notification)
        {
            _loginService = loginService;
            _authorization = authorization;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [Route("login")]
        public IActionResult Index(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Index(LoginViewModel loginModel, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (!ModelState.IsValid)
                return CustomView(loginModel);

            if (! await _loginService.Login(loginModel.Username, loginModel.Password))
                return CustomView(loginModel);

            var response = _authorization.GenerateJWT(loginModel.Username, ProfileEnum.Admin);

            await Authorize(response);

            if (string.IsNullOrEmpty(returnUrl))
                return RedirectToAction(nameof(Index), "AdminUser");

            return LocalRedirect(returnUrl);
        }

        [Route("sair")]        
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(Index));
        }

        [Route("alterar-senha")]
        public IActionResult ChangePassword()
        {            
            var model = new PasswordViewModel();

            return View(model);
        }

        [Route("alterar-senha")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(PasswordViewModel viewModel)
        {
            if(!ModelState.IsValid)
            {
                NotifyModelStateInvalid(ModelState);
                return CustomView(viewModel);
            }

            var id = HttpContext.User.Identity.Name;
            await _loginService.ChangePassword(id, viewModel.Password, viewModel.NewPassword);

            if (!CheckOperationSuccess("Senha alterada com sucesso"))
                return CustomView(viewModel);

            return RedirectToAction(nameof(Index), "AdminUser");
        }

        private async Task Authorize(Authorization authorization)
        {
            var token = GetFormattedToken(authorization.AccessToken);

            var claims = new List<Claim>();
            claims.Add(new Claim("JWT", authorization.AccessToken));
            claims.AddRange(token.Claims);

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(_appSettings.ExpirationMinutes),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                                          new ClaimsPrincipal(claimsIdentity),
                                          authProperties);
        }

        private static JwtSecurityToken GetFormattedToken(string jwtToken)
        {
            return new JwtSecurityTokenHandler().ReadToken(jwtToken) as JwtSecurityToken;
        }
    }
}