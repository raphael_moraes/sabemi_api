﻿
$(document).ready(function () {
    $("body").removeClass("hide");
});


$(".cssload-loader-inner").hide();
$("button, a").on("click", function (data) {
    var flag = false; 
    var classes = data.currentTarget.classList;

    for (let index = 0; index < classes.length; index++) {
        const element = classes[index];
        if (element === 'no-value')
            flag = true; 
    }

    if (flag === false) 
        dados(); 
    
    
});

function loader(element) {
    dados(); 
}
function dados() {
    var l = '<div class="cssload-cssload-loader-line-wrap-wrap">';
    l += '<div class="cssload-loader-line-wrap"></div>';
    l += '</div>';
    l += '<div class="cssload-cssload-loader-line-wrap-wrap">';
    l += '<div class="cssload-loader-line-wrap"></div>';
    l += '</div>';
    l += '<div class="cssload-cssload-loader-line-wrap-wrap">';
    l += '<div class="cssload-loader-line-wrap"></div>';
    l += '</div>';
    l += '<div class="cssload-cssload-loader-line-wrap-wrap">';
    l += '<div class="cssload-loader-line-wrap"></div>';
    l += '</div>';
    l += '<div class="cssload-cssload-loader-line-wrap-wrap">';
    l += '<div class="cssload-loader-line-wrap"></div>';
    l += '</div>';
    l += '</div>';

    $("#loader").html(l);
    $("#content-wrapper").hide();

}

function notify(message, type) {
    if (type == undefined) {
        type = "Info";
    }
    if (type == "Info") {
        $.growl({ title: "Opa!", message: message });
    }
    if (type == "Success") {
        $.growl.notice({ title: "OK!", message: message });        
    }
    if (type == "Error") {
        $.growl.error({ title: "Oops..", message: message });
    }
    if (type == "Warning") {
        $.growl.warning({ title: "Aviso", message: message });
    }    
}

$('.switch1').switcher({
    theme: 'square',
    on_state_content: '<span class="fa fa-check"></span>',
    off_state_content: '<span class="fa fa-times"></span>'
});

$(document).ready(function () {
    $("#msg_box").fadeOut(5000);
});

var dataTableLang = {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ itens",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "",
    "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
    },
    "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
    }
};