﻿$(document).ready(function () {

    $(".valor").each(function (index) {
        $(this).maskMoney({ prefix: 'R$ ', allowNegative: false, thousands: '', decimal: ',', affixesStay: false });
    });

    $(".numero-decimal").each(function (index) {
        $(this).maskMoney({ allowNegative: false, thousands: '', decimal: ',', affixesStay: false });
    });

    $(".cnpj").each(function (index) {
        $(this).mask("99.999.999/9999-99");
    });

    $(".cpf").each(function (index) {
        $(this).mask("999.999.999-99");
    });

    $(".foneMask").each(function (index) {
        $(this).mask("(99) 9999-99999");
    });

    $(".cep").each(function (index) {
        $(this).mask("99999-999");
    });

    $('.date-mask').mask('99/99/9999');

    $('.date_time').mask('99/99/9999 99:99');

    pickmeup('.date', {
        position: 'bottom',
        hide_on_select: true,
        format: 'd/m/Y'
    });

    $('.time').mask('99:99');

    $('.url').focusout(function (e) {
        if ($(e.target).val() != '') {
            if (!($(e.target).val().match('^http://') || $(e.target).val().match('^https://'))) {
                $(e.target).val('https://' + $(e.target).val());
            }
        }
    });

    
    jQuery(".textAreaSummernote").each(function (event) {

        $(this).summernote(/*{
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']]
            ]
        }*/);

    });

    jQuery(".removeCaracteresEspeciais").each(function (event) {
        $(this).keyup(function (event) {
            var str = event.target.value.toLowerCase()
            str = str.replace(' ', '');
            str = removerAcento(str);
            str = SubstituiAcento(str);
            event.target.value = str;
        });
    });


});


function removerAcento(palavra) {
    var palavraSemAcento = "";
    var caracterComAcento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
    var caracterSemAcento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";

    for (var i = 0; i < palavra.length; i++) {
        var char = palavra.substr(i, 1);
        var indexAcento = caracterComAcento.indexOf(char);
        if (indexAcento != -1) {
            palavraSemAcento += caracterSemAcento.substr(indexAcento, 1);
        } else {
            palavraSemAcento += char;
        }
    }

    return palavraSemAcento;
}

function SubstituiAcento(str) {
    var texto = str;
    var er = /[^a-z0-9]/gi;
    texto = texto.replace(er, "");

    return texto;
}