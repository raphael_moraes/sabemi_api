﻿var Mapa = {

    mapObj: "",
    markersArray: new Array(),
    optionsMap: {
        Zoom: 5
    },
  
    InitMap: function (localizacaoInicial) {
        
        var map = new google.maps.Map(document.getElementById('mapa'), {
            zoom: 10,
            center: { lat: -30.0277, lng: -51.2287}
        });

        Mapa.mapObj = map;

        if (localizacaoInicial != 'undefined' && localizacaoInicial != "" && localizacaoInicial.indexOf(',') > -1)
        {
            var latAndLng = localizacaoInicial.split(',');
            var latitude = latAndLng[0],
                long = latAndLng[1];

            this.SetCenterMap(latitude, long);
            this.CreateMarker({ lat: Number(latitude), lng: Number(long) });
        }
    },

    CreateMarker: function (coordenadas)
    {
        var marker = new google.maps.Marker({
            position: coordenadas,
            map: this.mapObj
        });
    },

    CleanMarkes: function ()
    {
        this.markersArray = new Array();
    },

    SetCenterMap: function(lat, long)
    {
        this.mapObj.setCenter(new google.maps.LatLng(lat, long));
    }

}