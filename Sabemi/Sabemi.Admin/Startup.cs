using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sabemi.Admin.Config;
using Sabemi.Domain.Configuration;
using Sabemi.IoC;
using System;

namespace Sabemi.Admin
{
    public class Startup
    {
        public Startup(IHostEnvironment hostEnvironment)
        {
            Environment.CurrentDirectory = hostEnvironment.ContentRootPath;

            var builder = new ConfigurationBuilder()
                .SetBasePath(hostEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{hostEnvironment.EnvironmentName}.json", true, true)
                .AddEnvironmentVariables();            

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentityConfiguration();

            services.AddMvcConfiguration();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<BaseSettings>(appSettingsSection);

            var loggingSection = Configuration.GetSection("Logging");
            services.Configure<Logging>(loggingSection);
            services.AddLogging(builder => new LoggingConfig(loggingSection, builder));            

            services.AddAutoMapper(typeof(Startup));            

            services.ResolveDependencies();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMvcConfiguration(env);
        }
    }
}
