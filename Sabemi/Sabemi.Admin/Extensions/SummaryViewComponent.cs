﻿using Microsoft.AspNetCore.Mvc;
using Sabemi.Domain.Interfaces;
using System.Threading.Tasks;

namespace Sabemi.Admin.Extensions
{
    public class SummaryViewComponent : ViewComponent
    {
        private readonly INotification _notification;

        public SummaryViewComponent(INotification notification)
        {
            _notification = notification;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var notififications = await Task.FromResult(_notification.GetNotifications());

            notififications.ForEach(x => ViewData.ModelState.AddModelError(string.Empty, x.Description));

            return View();
        }
    }
}
