﻿using System.ComponentModel.DataAnnotations;

namespace Sabemi.Admin.Models
{
    public class PasswordViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "A senha atual é obrigatória")]
        public string Password { get; set; }

        [Required(ErrorMessage = "A senha nova é obrigatória")]
        public string NewPassword { get; set; }

        [Compare(nameof(NewPassword), ErrorMessage = "As senhas não conferem.")]
        public string ConfirmPassword { get; set; }
    }
}
