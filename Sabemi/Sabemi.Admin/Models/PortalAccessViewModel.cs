﻿using System;

namespace Sabemi.Admin.Models
{
    public class PortalAccessViewModel
    {
        public string Id { get; set; }
        public string IP { get; set; }
        public string CPF { get; set; }
        public DateTime Date { get; set; }
        public string Hour { get; set; }
        public bool Blocked { get; set; }
        public string Situation { get; set; }
    }
}
