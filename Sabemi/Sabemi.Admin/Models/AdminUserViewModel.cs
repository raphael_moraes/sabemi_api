﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Admin.Models
{
    public class AdminUserViewModel
    {
        [Key]
        public string Id { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public int IdProfile { get; set; }
        public string Profile { get; set; }
        public bool Active { get; set; }
        public DateTime LastAccess { get; set; }
        public DateTime CreatedOn { get; set; }

        [Required]
        public string NewPassword { get; set; }
        public IList<ProfileEnum> Profiles { get; set; }
    }
}
