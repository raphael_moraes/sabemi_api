﻿using Sabemi.Domain.Entities;
using Sabemi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using WsSabemi;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Extensions
{
    public class ContractBuilder
    {
        private Contract Contract { get; set; }

        public ContractBuilder()
        {
            Contract = new Contract();
        }

        public ContractBuilder(DadosPropostaPortalSegurado source) : this(source.dadosPropostaAtiva)
        {
            if (Contract == null)
                Contract = new Contract();

            Contract.DigitalChannel = source.CanalDigital;
            Contract.PaymentType = source.FormaPagamento.ToEnum<PaymentType>().ToString();
            Contract.CertificateType = source.CodModeloCertificado.ToEnum<CertificateType>();
        }

        public ContractBuilder(DadosPropostasAtivas source)
        {
            
            Contract = new Contract
            {
                ID = (int)source.CodigoProposta,
                IdProductType = source.CodigoTipoProduto,
                Product = source.CodigoTipoProduto.ToString().ToEnum<ProductType>(),
                CPF = source.CPFCliente,
                ProductType = source.TipoProduto,
                Name = source.NomeComercialProduto ?? source.NomeProduto,
                ProductTradeName = source.NomeComercialProduto,
                LuckyNumber = source.NumeroSorte,
                StartExpiryDate = source.DataAssinaturaProposta,
                EndExpiryDate = source.DataAssinaturaProposta?.AddYears(1),
                Price = source.ValorParcela,
                RegistrationCode = source.CodigoMatricula,
                OrganName = source.NomeOrgao,
                Active = true,
                CertificateType = source.CodModeloCertificado.ToEnum<CertificateType>()
            };
        }

        public ContractBuilder AddBeneficiaries(BeneficiariosProposta[] beneficiarios)
        {
            foreach (var beneficiario in beneficiarios.OrderBy(x => x.NomeBeneficiario))
            {
                Contract.Beneficiaries.Add(new Beneficiary
                {
                    IdContract = beneficiario.CodProposta,
                    Name = beneficiario.NomeBeneficiario,
                    CPF = beneficiario.CPFBeneficiario
                });
            }

            return this;
        }

        public ContractBuilder AddBenefits(CoberturaProposta[] coberturas)
        {
            BenefitType? benefitType = null;
            Icons? icon = null;
            Links? link = null;

            foreach (var cobertura in coberturas)
            {

                benefitType = cobertura.CodigoCobertura.ToString().ToEnum<BenefitType>();
                icon = cobertura.CodigoCobertura.ToString().ToEnum<Icons>();
                link = cobertura.CodigoCobertura.ToString().ToEnum<Links>();

                Contract.Benefits.Add(new Benefit
                {
                    ID = cobertura.CodigoCobertura,
                    Title = benefitType.GetText(),
                    Description = benefitType.Description() ?? cobertura.DescricaoCobertura,
                    IdContract = cobertura.CodigoProposta,
                    Icon = icon.GetText(),
                    Contact = GetContact(link, cobertura.CodigoCobertura)
                });
            }

            return this;
        }

        private static BenefitContact GetContact(Links? contact, int id)
        {
            if (!contact.HasValue)
                return null;

            return new BenefitContact
            {
                ID = id,
                Description = contact.Description(),
                Phone = contact.GetText(),
                Url = contact.GetUrl()
            };
        }

        public ContractBuilder SetInstallmentData(ParcelasProposta[] parcelas)
        {
            SetInstallmentNumber(this.Contract, parcelas);                

            return this;
        }

        public void SetInstallmentDataToList(List<Contract> contracts)
        {
            foreach (var contract in contracts)
            {
                var client = WsServicesFactory.GetParcelsByProductType(contract.ID, contract.IdProductType).Result;

                if (!string.IsNullOrEmpty(client.Proposta.FormaPagamento))
                    contract.PaymentType = client.Proposta.FormaPagamento.ToEnum<PaymentType>().ToString();

                SetInstallmentNumber(contract, client.Proposta.ListaParcelas);
            }
        }

        private static void SetInstallmentNumber(Contract contract, ParcelasProposta[] parcelas)
        {
            if (parcelas.Any())
            {
                var installment = parcelas.Where(x => x.DataVencimento.Year == DateTime.Today.Year
                                                   && x.DataVencimento.Month >= DateTime.Today.Month).FirstOrDefault();

                if (installment == null)
                    installment = parcelas.LastOrDefault();

                contract.CurrentInstallmentNumber = installment.NumeroParcela;

                if (parcelas.Length != installment.NumeroParcela)
                    installment = parcelas.LastOrDefault();

                contract.LastInstallmentNumber = installment.NumeroParcela;
            }
        }

        public Contract Build()
        {
            return Contract;
        }
    }
}
