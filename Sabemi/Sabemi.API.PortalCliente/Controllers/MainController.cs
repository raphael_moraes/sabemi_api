﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class MainController : ControllerBase
    {
        private readonly INotification _notification;
        public readonly IAuthUser _authUser;

        public string UserCpf { get; set; }

        protected MainController(INotification notification, IAuthUser authUser)
        {
            _notification = notification;
            _authUser = authUser;

            UserCpf = _authUser.GetUserCpf();
        }

        protected bool IsValidOperation()
        {
            return !_notification.HasNotification();
        }

        protected ActionResult CustomResponse(object result = null)
        {
            if (IsValidOperation())
                if (result != null)
                    return Ok(new
                    {
                        Data = result
                    });
                else return Ok();

            return BadRequest(new 
            {
                Errors = _notification.GetNotifications().Select(message => message.Description)
            });
        }

        protected ActionResult CustomResponse(Exception ex)
        {
            // TODO Verificar se o erro foi interno ou durante a comunicação com a API da Sabemi, para retornar o erro apropriado
            Notify(Error.Internal.Description());
            return CustomResponse(new ObjectResult(ex)
            {
                StatusCode = 500
            });
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
                NotifyModelStateInvalid(modelState);

            return CustomResponse();
        }

        protected ActionResult CustomResponse(ObjectResult objectResult)
        {
            var errors = _notification.GetNotifications().Select(message => message.Description);
            switch (objectResult)
            {
                case NotFoundObjectResult result:
                    return NotFound(new 
                    {
                        Errors = errors
                    });
                case BadRequestObjectResult result:
                    return BadRequest(new 
                    {
                        Errors = errors
                    });
                default:
                    return StatusCode(objectResult.StatusCode ?? 500, new 
                    {
                        Errors = errors
                    });
            }
        }

        protected void NotifyModelStateInvalid(ModelStateDictionary modelState)
        {
            var errors = modelState.Values.SelectMany(e => e.Errors);
            foreach (var error in errors)
            {
                var errorMsg = error.Exception == null ? error.ErrorMessage : error.Exception.Message;
                Notify(errorMsg);
            }
        }

        protected void Notify(string errorMsg)
        {
            _notification.Handle(new Message(errorMsg));
        }
    }
}