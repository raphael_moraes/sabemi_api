﻿using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sabemi.API.PortalCliente.ViewModels;
using Sabemi.CrossCutting.Extensions;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;

namespace Sabemi.API.PortalCliente.Controllers
{
    [Authorize]
    [Route("api/user")]
    [ApiController]
    public class UserController : MainController
    {
        private readonly IUserService _service;
        private readonly IMapper _mapper;

        public UserController(IUserService service,
                              INotification notification,
                              IMapper mapper,
                              IAuthUser authUser) : base(notification, authUser)
        {
            _service = service;
            _mapper = mapper;
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPut]
        public async Task<ActionResult> Update(UserViewModel userViewModel)
        {
            if (UserCpf != userViewModel.CPF)
            {
                Notify("O CPF informado não corresponde ao CPF do usuário.");
                return CustomResponse();
            }

            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var user = _mapper.Map<User>(userViewModel);
            if (!await _service.Update(user))
                return CustomResponse();

            return CustomResponse(new { ChangeSuccess = true });
        }

        [HttpGet]
        public async Task<ActionResult<UserViewModel>> Get()
        {
            var user = await _service.Get(UserCpf);
            if (user == null)
                return CustomResponse(new NotFoundObjectResult(null));

            var userViewModel = _mapper.Map<UserViewModel>(user);

            return CustomResponse(userViewModel);
        }                
    }
}