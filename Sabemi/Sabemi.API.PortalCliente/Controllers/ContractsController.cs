﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.API.PortalCliente.ViewModels;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Sabemi.Domain.Entities;
using static Sabemi.CrossCutting.Util.Enumerator;

using Sabemi.CrossCutting.Extensions;
using System.Security.Claims;
using RestSharp;

namespace Sabemi.API.PortalCliente.Controllers
{
    [Authorize]
    [Route("api/contracts")]
    [ApiController]
    public class ContractsController : MainController
    {
        private readonly IContractService _contractService;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public ContractsController(INotification notification,
                                   IContractService contractService,
                                   IMapper mapper,
                                   ILogger<ContractsController> logger,
                                   IAuthUser authUser) : base(notification, authUser)
        {
            _contractService = contractService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IList<ContractViewModel>>> GetContracts()
        {
            try
            {
                var contracts = await _contractService.GetUserContracts(UserCpf);
                var model = _mapper.Map<IList<ContractViewModel>>(contracts);
                return CustomResponse(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CustomResponse(ex);
            }
        }

        [Route("{contract_id:int}")]
        [HttpGet]
        public async Task<ActionResult<ContractViewModel>> GetContract(int contract_id)
        {
            try
            {
                var contract = await _contractService.GetContractById(contract_id, UserCpf);
                var model = _mapper.Map<ContractViewModel>(contract);
                return CustomResponse(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return CustomResponse(ex);
            }
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [Route("payment")]
        [HttpPut]
        public async Task<ActionResult> UpdatePayment(Payment payment)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            if (! await _contractService.ContractExist(payment.IdContract, UserCpf))
                return CustomResponse(new NotFoundObjectResult(null));

            var protocol = await _contractService.UpdatePayment(UserCpf, payment);

            if (string.IsNullOrEmpty(protocol))
                return CustomResponse();

            return CustomResponse(new { protocol = protocol });
        }        

        [Route("{contract_id:int}/luckynumber")]
        [HttpGet]
        public async Task<ActionResult> GetLuckyNumber(int contract_id)
        {
            string luckyNumber = await _contractService.GetLuckyNumber(contract_id, UserCpf);

            return CustomResponse(new { luckyNumber = luckyNumber });
        }

        [Route("{contract_id:int}/benefits")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Benefit>>> GetBenefits(int contract_id)
        {
            var benefits = await _contractService.GetBenefits(contract_id, UserCpf);
            if (benefits == null)
                return CustomResponse(new NotFoundObjectResult(null));

            return CustomResponse(benefits);
        }

        [Route("{contract_id:int}/installments")]
        [HttpGet]
        public async Task<ActionResult<ContractInstallment>> GetInstallments(int contract_id, int productTypeId)
        {
            var contractIntallment = await _contractService.GetContractInstallment(contract_id, productTypeId);
            if (contractIntallment == null)
                return CustomResponse(new NotFoundObjectResult(null));

            return CustomResponse(contractIntallment);
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [Route("certificate")]
        [HttpPost]
        public async Task<ActionResult> Certificate(CertificateViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var certificate = _mapper.Map<Certificate>(viewModel);
            if (certificate.CertificateType.Equals(CertificateType.SemCertificado))
            {
                Notify("O contrato não possui certificado!");
                return CustomResponse();
            }

            var contentFile =  await _contractService.GetCertificate(UserCpf, certificate);

            if (contentFile == null)
                return CustomResponse();

            return CustomResponse(new { file = contentFile } );
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [Route("{contract_id:int}/beneficiary")]
        [HttpPost]
        public async Task<ActionResult<string>> SendBeneficiary(int contract_id, IList<BeneficiaryViewModel> beneficiaryList)
        {
            IList<object> list = new List<object>();

            foreach (var beneficiary in beneficiaryList)
            {
                var @object = new
                {
                    Nome = beneficiary.Name,
                    CpfBeneficiario = beneficiary.CPF,
                    GrauParentesco = beneficiary.KinshipDegree,
                    PercentualIndenizacao = beneficiary.IndemnityPercent                    
                };

                list.Add(@object);
            }
            
            string formSerialized = SimpleJson.SerializeObject(list);

            var protocol = await _contractService.SendBeneficiary(UserCpf, contract_id, formSerialized);

            if (string.IsNullOrEmpty(protocol))
                return CustomResponse();

            return CustomResponse(new { protocol = protocol });
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [Route("cancellation")]
        [HttpPost]
        public async Task<ActionResult<string>> RequestCancellation(Cancellation cancellation)
        {
            var protocol = await _contractService.RequestCancellation(UserCpf, cancellation);

            if (string.IsNullOrEmpty(protocol))
                return CustomResponse();

            return CustomResponse(new { protocol = protocol });
        }
    }
}