﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sabemi.API.PortalCliente.ViewModels;
using Sabemi.CrossCutting.Extensions;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Controllers
{
    [Authorize]
    [Route("api/login")]
    [ApiController]
    public class LoginController : MainController
    {
        private readonly ILoginService _loginService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ILogger<LoginController> _logger;
        private readonly IAuthorizationManager _authorizationManager;

        public LoginController(INotification notification,
                               ILoginService loginService,
                               IUserService userService,
                               IMapper mapper,
                               ILogger<LoginController> logger,
                               IAuthUser authUser,
                               IAuthorizationManager authorizationManager) : base(notification, authUser)
        {
            _loginService = loginService;
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
            _authorizationManager = authorizationManager;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<Authorization>> Login(LoginViewModel loginViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return CustomResponse(ModelState);

                if ((loginViewModel.GrantType.ToEnum<GrantType>() != GrantType.Password) &&
                    (loginViewModel.GrantType.ToEnum<GrantType>() != GrantType.RefreshToken))
                {
                    Notify("Invalid Grant Type!");
                    return CustomResponse();
                }

                if (loginViewModel.GrantType.ToEnum<GrantType>() == GrantType.Password)
                {
                    if (string.IsNullOrEmpty(loginViewModel.Password))
                    {
                        Notify("Informe a senha!");
                        return CustomResponse();
                    }

                    if (!await _loginService.Login(loginViewModel.CPF, loginViewModel.Password))
                        return CustomResponse();
                }
                else if ((loginViewModel.GrantType.ToEnum<GrantType>() == GrantType.RefreshToken) && (!_authorizationManager.ValidateCredentials(loginViewModel.CPF, loginViewModel.RefreshToken)))
                {
                    Notify("Não foi possível renovar o token!");
                    return CustomResponse();
                }

                return CustomResponse(await _authorizationManager.GenerateJWT(loginViewModel.CPF));
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro na autenticação do usuário!");
                _logger.LogError(ex, ex.Message);
                return CustomResponse();
            }
        }

        [AllowAnonymous]
        [HttpPost("assisted_mode")]
        public async Task<ActionResult> AssistedMode(LoginViewModel loginViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return CustomResponse(ModelState);

                if (loginViewModel.GrantType.ToEnum<GrantType>() != GrantType.Password) 
                {
                    Notify("Invalid Grant Type!");
                    return CustomResponse();
                }

                if (string.IsNullOrEmpty(loginViewModel.Password))
                {
                    Notify("Informe a senha!");
                    return CustomResponse();
                }

                if (!await _loginService.ValidateAssistedMode(loginViewModel.CPF, loginViewModel.Password))
                    return CustomResponse();

                return CustomResponse( _authorizationManager.GenerateJWT(loginViewModel.CPF, ProfileEnum.Assisted_Mode));
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro na autenticação do usuário!");
                _logger.LogError(ex, ex.Message);
                return CustomResponse();
            }
        }

        [HttpPost("assisted_mode/{cpf}")]
        public async Task<ActionResult<Authorization>> Login(string cpf)
        {
            if (string.IsNullOrEmpty(cpf))
            {
                Notify("Informe o CPF!");
                return CustomResponse();
            }

            return CustomResponse(await _authorizationManager.GenerateJWT(cpf, assistedMode: true));
        }

        [AllowAnonymous]
        [Route("firstaccess/{cpf}")]
        [HttpPost]
        public async Task<ActionResult> FirstAccess(string cpf)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            if (!await _loginService.ValidateFirstAccess(cpf))
                return CustomResponse();

            return CustomResponse(await _authorizationManager.GenerateJWT(cpf, firstAccess: true));
        }        

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpGet("questions")]
        public async Task<ActionResult<IDictionary<string, QuestionViewModel>>> GetQuestions()
        {
            var user = await _userService.Get(UserCpf, ignoreCache: true);
            var questions = _mapper.Map<IDictionary<string, QuestionViewModel>>(_loginService.GetQuestions(user));
            if (questions == null)
                return CustomResponse(new NotFoundObjectResult(null));

            return CustomResponse(questions);
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPost("questions")]
        public async Task<ActionResult<AnswerQuestionsResponseViewModel>> AnswerQuestions(IDictionary<string, string> answers)
        {
            var user = await _userService.Get(UserCpf, ignoreCache: true);
            var result = _loginService.AnswerQuestions(user, _mapper.Map<IDictionary<QuestionType, string>>(answers));

            return CustomResponse(new AnswerQuestionsResponseViewModel { IsCorrect = result });
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpGet("phone")]
        public async Task<ActionResult<GetCellphoneResponseViewModel>> GetCellPhone()
        {
            var result = await _userService.GetCellPhone(UserCpf, masked: true);

            if (string.IsNullOrEmpty(result))
                return CustomResponse(new NotFoundObjectResult(null));

            return CustomResponse(new GetCellphoneResponseViewModel { CellPhone = result });
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPost("phone")]
        public async Task<ActionResult> SendSMS()
        {
            string phoneNumber = await _userService.GetCellPhone(UserCpf, masked: false);
            await _loginService.SendSMSCode(phoneNumber);

            return CustomResponse();
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPost("phone/{code}")]
        public async Task<ActionResult<VerifySMSCodeResponseViewModel>> VerifySMSCode(string code)
        {
            string phoneNumber = await _userService.GetCellPhone(UserCpf, masked: false);

            bool isValid = _loginService.VerifySMSCode(phoneNumber, code);

            if (!isValid)
                return CustomResponse();

            return CustomResponse(new VerifySMSCodeResponseViewModel { IsValid = isValid });
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPost("createpassword")]
        public async Task<ActionResult<RegisterUserViewModel>> CreatePassword(RegisterUserViewModel registerUserViewModel)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            if (await _userService.RegisterPassword(registerUserViewModel.CPF, registerUserViewModel.Password))
                if (await _loginService.Login(registerUserViewModel.CPF, registerUserViewModel.Password))
                    return CustomResponse(registerUserViewModel);

            return CustomResponse();
        }

        [AllowAnonymous]
        [Route("forgetpassword/{cpf}")]
        [HttpPost]
        public async Task<ActionResult> ForgetPassword(string cpf)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            return CustomResponse(await _loginService.ForgetPassword(cpf));
        }


        [AllowAnonymous]
        [Route("updatepassword")]
        [HttpPost]
        public async Task<ActionResult> UpdatePassword(UpdatePasswordRequest dados)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            return CustomResponse(await _loginService.UpdatePassword(dados));
        }

    }
}
