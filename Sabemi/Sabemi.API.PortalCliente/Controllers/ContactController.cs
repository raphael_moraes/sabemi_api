﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Sabemi.API.PortalCliente.ViewModels;
using Sabemi.CrossCutting.Extensions;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;

namespace Sabemi.API.PortalCliente.Controllers
{
    [Authorize]
    [Route("api/contact")]
    [ApiController]
    public class ContactController : MainController
    {
        private readonly IContactService _service;
        private readonly IMapper _mapper;

        public ContactController(INotification notification,
                                 IContactService service,
                                 IAuthUser authUser,
                                 IMapper mapper) : base(notification, authUser)
        {
            _service = service;
            _mapper = mapper;
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPost("contact_us")]
        public async Task<ActionResult> SendContactForm(ContactUsForm form)
        {
            var @object = new
            {
                Nome = form.Name,
                Email = form.Email,
                Telefone = form.Telephone,
                Mensagem = form.Message
            };

            string formSerialized = JsonConvert.SerializeObject(@object);

            var protocol = await _service.SendContactUsForm(UserCpf, formSerialized);

            if (string.IsNullOrEmpty(protocol))
                return CustomResponse();

            return CustomResponse(new { protocol = protocol });
        }

        [AllowAnonymous]
        [Route("casualty")]
        [HttpPost]
        public async Task<ActionResult> SendCasualtyNotification(CasualtyViewModel viewModel)
        {
            var form = _mapper.Map<CasualtyForm>(viewModel);
            
            var protocol = await _service.SendCasualtyNotification(UserCpf, form);

            if (string.IsNullOrEmpty(protocol))
                return CustomResponse();

            return CustomResponse(new { protocol = protocol });
        }

        [Route("trackingsolicitations")]
        [HttpGet()]
        public async Task<ActionResult<List<Solicitation>>> GetSolicitations(DateTime? initDate, DateTime? finishDate, string situation)
        {
            var solicitations = await _service.GetSolicitations(UserCpf, initDate, finishDate, situation);

            if (solicitations == null)
                return CustomResponse();

            return CustomResponse(solicitations);
        }

        [Route("faq")]
        [HttpGet]
        public ActionResult<FAQViewModel> GetCommonQuestion(int idCategory, int idSubCategory)
        {
            var faq = _service.GetCommonQuestion(idCategory, idSubCategory);

            var faqViewModel = _mapper.Map<FAQViewModel>(faq);

            return CustomResponse(faqViewModel);
        }
    }
}