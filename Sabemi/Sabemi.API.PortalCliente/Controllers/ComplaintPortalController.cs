﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Sabemi.API.PortalCliente.ViewModels;
using Sabemi.CrossCutting.Extensions;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Controllers
{
    [Authorize]
    [Route("api/complaintportal")]
    [ApiController]
    public class ComplaintPortalController : MainController
    {
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;
        private readonly IContactService _contactService;

        public ComplaintPortalController(INotification notification,
                                         IFileService fileService,
                                         IMapper mapper,
                                         IContactService contactService,
                                         IAuthUser authUser) : base(notification, authUser)
        {
            _fileService = fileService;
            _mapper = mapper;
            _contactService = contactService;
        }

        [ClaimsAuthorize(ClaimTypes.Role, "Portal")]
        [HttpPost()]
        public async Task<ActionResult> Post(ComplaintViewModel complaintViewModel)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            //var imgName = Guid.NewGuid() + "_" + complaintViewModel.AttachmentName;
            //if (! _fileService.Upload(UserCpf, complaintViewModel.Attachment, imgName))
            //    return CustomResponse();

            //var complaint = _mapper.Map<Complaint>(complaintViewModel);

            var solicitationType = complaintViewModel.IdSubject.ToString().ToEnum<SolicitationType>();

            var @object = new
            {
                Nome = complaintViewModel.Name,
                Email = complaintViewModel.Email,
                Telefone = complaintViewModel.PhoneNumber,
                Assunto = solicitationType.GetText(),
                Descricao = complaintViewModel.Description,
                Arquivo = complaintViewModel.Attachment                
            };

            string formSerialized = JsonConvert.SerializeObject(@object);

            var protocol = await _contactService.SendComplaint(UserCpf, formSerialized);

            if (string.IsNullOrEmpty(protocol))
                return CustomResponse();

            return CustomResponse(new { protocol = protocol });
        }

        /// <summary>
        /// Método alternativo para trabalhar com IFormFile futuramente
        /// </summary>
        /// <param name="complaintViewModel"></param>
        /// <returns></returns>
        //[HttpPost("SendComplaint")]
        //public async Task<ActionResult<ComplaintViewModel>> AlternativePost(ComplaintViewModel complaintViewModel)
        //{
        //    if (!ModelState.IsValid)
        //        return CustomResponse(ModelState);

        //    if (! await _fileService.Upload(complaintViewModel.Attachment))
        //        return CustomResponse();

        //    var complaint = _mapper.Map<Complaint>(complaintViewModel);
        //    await _contactService.SendComplaint(complaint);

        //    return CustomResponse(complaintViewModel);
        //}
    }
}