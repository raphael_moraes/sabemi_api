using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sabemi.API.PortalCliente.Config;
using Sabemi.IoC;
using Sabemi.Domain.Configuration;
using System.Text.Json;
using System;
using Sabemi.Services;

namespace Sabemi.API.PortalCliente
{
    public class Startup
    {
        public Startup(IWebHostEnvironment hostEnvironment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(hostEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{hostEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            Environment.CurrentDirectory = hostEnvironment.ContentRootPath;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Controllers and Json response configuration
            services.AddControllers(option => option.EnableEndpointRouting = false)
                .AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                o.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
            });

            services.AddSwaggerConfig();            

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<BaseSettings>(appSettingsSection);
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            WsServicesFactory.Initialize(appSettings.SabemiWebService, appSettings.SMSWebService);

            var loggingSection = Configuration.GetSection("Logging");
            services.Configure<Logging>(loggingSection);

            services.AddJwtConfiguration(appSettingsSection);

            services.AddLogging(builder => new LoggingConfig(loggingSection, builder));

            services.AddHealthCheckConfiguration(Configuration);

            services.AddDistributedMemoryCache();

            services.AddHttpContextAccessor();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(appSettingsSection.GetValue("CookieExpirationMinutes", 20));
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAutoMapper(typeof(Startup));
            services.ResolveDependencies();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();            

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";

                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "Sabemi API V1");

            });


            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .WithMethods("POST", "GET", "DELETE", "PUT");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseSession();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseHealthCheckConfiguration();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
