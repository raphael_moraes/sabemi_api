﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sabemi.API.PortalCliente.Config.HealthChecks;

namespace Sabemi.API.PortalCliente.Config
{
    public static class HealthCheckConfig
    {
        public static IServiceCollection AddHealthCheckConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            var appSettingsConfig = configuration.GetSection("AppSettings");
            var loggingSection = configuration.GetSection("Logging");

            services.AddHealthChecks()
                .AddCheck("WsSabemi", new WebServiceHealthCheck(appSettingsConfig.GetValue<string>("SabemiWebService"), loggingSection))
                .AddCheck("WsSendSMS", new WebServiceHealthCheck(appSettingsConfig.GetValue<string>("SMSWebService"), loggingSection))            
                .AddGCInfoCheck("Memory Expense");

            services.AddHealthChecksUI();

            return services;
        }

        public static IApplicationBuilder UseHealthCheckConfiguration(this IApplicationBuilder app)
        {
            app.UseHealthChecks("/api/hc", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseHealthChecksUI(options =>
            {
                options.UIPath = "/api/hc-ui";
            });

            return app;
        }
    }
}
