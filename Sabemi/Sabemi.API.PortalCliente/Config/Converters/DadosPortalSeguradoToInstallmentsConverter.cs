﻿using AutoMapper;
using Sabemi.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using WsSabemi;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class DadosPortalSeguradoToInstallmentsConverter : ITypeConverter<DadosPortalSegurado, List<Installment>>
    {
        public List<Installment> Convert(DadosPortalSegurado source, List<Installment> destination, ResolutionContext context)
        {
            destination = new List<Installment>();

            foreach (var parcel in source.Proposta.ListaParcelas.OrderByDescending(x => x.DataVencimento))
            {
                var p = new Installment
                {
                    InstallmentNumber = parcel.NumeroParcela,
                    Price = parcel.ValorParcela,
                    ExpirationDate = parcel.DataVencimento,
                    Status = parcel.StatusPagamento,
                    Period = parcel.DataVencimento.ToString("MM/yyyy"),
                    PaymentDate = parcel.DataPagamento
                };

                destination.Add(p);
            }

            return destination;
        }
    }
}
