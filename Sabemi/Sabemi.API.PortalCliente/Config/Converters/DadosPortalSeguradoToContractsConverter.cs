﻿using AutoMapper;
using Sabemi.API.PortalCliente.Extensions;
using Sabemi.Domain.Entities;
using System.Collections.Generic;
using WsSabemi;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class DadosPortalSeguradoToContractsConverter : ITypeConverter<DadosPortalSegurado, List<Contract>>
    {
        public List<Contract> Convert(DadosPortalSegurado source, List<Contract> destination, ResolutionContext context)
        {
            destination = new List<Contract>();

            foreach (var proposta in source.ListaPropostasAtivas)
            {
                var builder = new ContractBuilder(proposta)
                    .AddBeneficiaries(proposta.Beneficiarios)
                    .AddBenefits(proposta.ListaCoberturasProposta);                    

                destination.Add(builder.Build());
            }

            new ContractBuilder().SetInstallmentDataToList(destination);

            return destination;
        }        
    }
}
