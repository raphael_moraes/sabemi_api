﻿using AutoMapper;
using System;
using System.Collections.Generic;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class StringDictionaryToQuestionTypeDictionary<T> : ITypeConverter<IDictionary<string, T>, IDictionary<QuestionType, T>>
    {
        public IDictionary<QuestionType, T> Convert(IDictionary<string, T> source, IDictionary<QuestionType, T> destination, ResolutionContext context)
        {
            destination = new Dictionary<QuestionType, T>();
            foreach (var item in source)
            {
                destination.Add(Enum.Parse<QuestionType>(item.Key, true), item.Value);
            }
            return destination;
        }
    }
}