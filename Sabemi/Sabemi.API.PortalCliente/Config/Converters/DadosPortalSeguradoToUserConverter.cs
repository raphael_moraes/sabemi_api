﻿using AutoMapper;
using Sabemi.Domain.Entities;
using System;
using WsSabemi;
using System.Linq;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class DadosPortalSeguradoToUserConverter : ITypeConverter<DadosPortalSegurado, User>
    {
        public User Convert(DadosPortalSegurado source, User destination, ResolutionContext context)
        {
            try
            {
                destination = new User();

                if (source.DadosCadastraisCliente != null)
                {
                    destination.ID = source.DadosCadastraisCliente.CodigoCliente;
                    destination.CPF = source.DadosCadastraisCliente.CPFCliente?.NullIfInterrogation();
                    destination.IDNumber = source.DadosCadastraisCliente.NumeroIdentidade?.NullIfInterrogation();
                    destination.BirthDate = source.DadosCadastraisCliente.DataNascimento;
                    destination.Name = source.DadosCadastraisCliente.NomeCliente?.NullIfInterrogation();
                    destination.MotherName = source.DadosCadastraisCliente.NomeMae?.Trim().NullIfInterrogation();
                    destination.FatherName = source.DadosCadastraisCliente.NomePai?.NullIfInterrogation();
                    destination.MaritalStatus = source.DadosCadastraisCliente.EstadoCivil.ToEnum<MaritalStatus>();
                    destination.Sex = source.DadosCadastraisCliente.Sexo.ToEnum<Sex>();
                    destination.Hometown = source.DadosCadastraisCliente.CidadeNascimento?.NullIfInterrogation();
                    destination.Citizenship = source.DadosCadastraisCliente.Naturalidade?.NullIfInterrogation();

                    destination.Nationality = source.DadosCadastraisCliente.Nacionalidade?.NullIfInterrogation();
                    destination.State = source.DadosCadastraisCliente.Estado.ToEnum<State>();
                    destination.CEP = source.DadosCadastraisCliente.CEP?.NullIfInterrogation();
                    destination.City = source.DadosCadastraisCliente.Cidade?.NullIfInterrogation();
                    destination.Neighborhood = source.DadosCadastraisCliente.Bairro?.NullIfInterrogation();
                    destination.Street = source.DadosCadastraisCliente.Endereco?.NullIfInterrogation();
                    destination.Complement = source.DadosCadastraisCliente.ComplementoEndereco?.NullIfInterrogation();
                    destination.AddressNumber = source.DadosCadastraisCliente.NumeroEndereco?.NullIfInterrogation();

                    source.DadosCadastraisCliente.ListaContatos = source.DadosCadastraisCliente.ListaContatos.OrderByDescending(c => c.DataAlteracao).ToArray();

                    destination.Telephone = source.DadosCadastraisCliente.ListaContatos?
                        .Where(c => (ContactType)c.CodTipoContato == ContactType.Telephone && 
                                    !string.IsNullOrEmpty(c.Valor.Trim())).FirstOrDefault()?.Valor?.Trim();

                    destination.Cellphone = source.DadosCadastraisCliente.ListaContatos?
                        .Where(c => c.CodTipoContato.ToString().ToEnum<ContactType>() == ContactType.Cellphone && 
                                    !string.IsNullOrEmpty(c.Valor.Trim())).FirstOrDefault()?.Valor?.Trim();
                    
                    destination.Email = source.DadosCadastraisCliente.ListaContatos?
                        .Where(c => (ContactType)c.CodTipoContato == ContactType.Email &&
                                    !string.IsNullOrEmpty(c.Valor.Trim())).FirstOrDefault()?.Valor?.Trim();
                }
                else if (source.ClienteAptoLogar != null)
                {
                    destination.ID = source.ClienteAptoLogar.CodigoPessoaFisica;
                    destination.CPF = source.ClienteAptoLogar.CPFCliente?.NullIfInterrogation();
                    destination.BirthDate = source.ClienteAptoLogar.DataNascimento;
                    destination.Name = source.ClienteAptoLogar.NomeCliente?.NullIfInterrogation();
                    destination.MotherName = source.ClienteAptoLogar.NomeMae?.Trim().NullIfInterrogation();
                    destination.Hometown = source.ClienteAptoLogar.CidadeNascimento?.NullIfInterrogation();
                    destination.Citizenship = source.ClienteAptoLogar.Naturalidade?.NullIfInterrogation();
                }

                return destination;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
