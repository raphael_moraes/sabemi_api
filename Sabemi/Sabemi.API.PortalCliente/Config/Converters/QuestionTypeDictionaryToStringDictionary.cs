﻿using AutoMapper;
using System;
using System.Collections.Generic;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class QuestionTypeDictionaryToStringDictionary<T> : ITypeConverter<IDictionary<QuestionType, T>, IDictionary<string, T>>
    {
        public IDictionary<string, T> Convert(IDictionary<QuestionType, T> source, IDictionary<string, T> destination, ResolutionContext context)
        {
            destination = new Dictionary<string, T>();
            foreach (var item in source)
            {
                destination.Add(item.Key.Name().ToLower(), item.Value);
            }
            return destination;
        }
    }
}