﻿using AutoMapper;
using Sabemi.API.PortalCliente.Extensions;
using Sabemi.Domain.Entities;
using WsSabemi;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class DadosPropostaPortalSeguradoToContractByIdConverter : ITypeConverter<DadosPropostaPortalSegurado, Contract>
    {
        public Contract Convert(DadosPropostaPortalSegurado source, Contract destination, ResolutionContext context)
        {
            ContractBuilder builder = new ContractBuilder(source)
                .AddBeneficiaries(source.Beneficiarios)
                .AddBenefits(source.ListaCoberturasProposta)
                .SetInstallmentData(source.ListaParcelas);
            
            return destination = builder.Build();
        }        
    }
}
