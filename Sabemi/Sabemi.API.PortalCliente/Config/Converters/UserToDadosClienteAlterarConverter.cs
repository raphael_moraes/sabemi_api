﻿using AutoMapper;
using Sabemi.Domain.Entities;
using System;
using System.Linq;
using WsSabemi;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.API.PortalCliente.Config.Converters
{
    public class UserToDadosClienteAlterarConverter : ITypeConverter<User, DadosClienteAlterar>
    {
        public DadosClienteAlterar Convert(User source, DadosClienteAlterar destination, ResolutionContext context)
        {
            destination = new DadosClienteAlterar
            {
                CEP = source.CEP,
                CidadeNascimento = source.Hometown,
                ComplementoEndereco = source.Complement,
                Endereco = source.Street,
                EstadoCivil = source.MaritalStatus.GetCode(),
                ListaContatoCliente = new ContatoClienteAlterar[]
                {
                    new ContatoClienteAlterar { TipoContato = ContactType.Email.ValueAsInt(), Valor = source.Email },
                    new ContatoClienteAlterar { TipoContato = ContactType.Telephone.ValueAsInt(), Valor = source.Telephone },
                    new ContatoClienteAlterar { TipoContato = ContactType.Cellphone.ValueAsInt(), Valor = source.Cellphone },
                },
                Nacionalidade = source.Nationality,
                Naturalidade = source.Citizenship,
                NomeCliente = source.Name,
                NomeMae = source.MotherName,
                NomePai = source.FatherName,
                NumeroEndereco = source.AddressNumber,
                NumeroIdentidade = source.IDNumber,
                Sexo = source.Sex.GetCode()
            };

            destination.ListaContatoCliente = destination.ListaContatoCliente.Where(x => x.Valor != null).ToArray();

            return destination;
        }
    }
}
