﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Sabemi.API.PortalCliente.Config.HealthChecks
{
    public class WebServiceHealthCheck : IHealthCheck
    {
        readonly string _url;
        private ILoggerFactory _loggerFactory = new LoggerFactory();
        private ILogger<WebServiceHealthCheck> _logger;

        public WebServiceHealthCheck(string url, IConfigurationSection logging)
        {
            _url = url;
            _loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConfiguration(logging);
                builder.AddConsole();
            });

            _logger = _loggerFactory.CreateLogger<WebServiceHealthCheck>();
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                HttpWebRequest request = WebRequest.CreateHttp(_url);
                request.Timeout = 60000; //1 minuto

                Stopwatch timer = new Stopwatch();

                timer.Start();

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                response.Close();

                timer.Stop();                

                return timer.Elapsed.TotalMilliseconds < request.Timeout ? 
                    HealthCheckResult.Healthy($"Web Service responding in {Math.Round(timer.Elapsed.TotalSeconds, 2)} seconds. Limit: 60 seconds") : 
                    HealthCheckResult.Unhealthy("Web Service has timed out!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return HealthCheckResult.Unhealthy($"Ocorreu uma exceção no destino da chamada: {ex.Message}");
            }
        }
    }
}
