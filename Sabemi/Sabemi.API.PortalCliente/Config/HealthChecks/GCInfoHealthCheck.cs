﻿using Humanizer.Bytes;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sabemi.API.PortalCliente.Config.HealthChecks
{
    public class GCInfoHealthCheck : IHealthCheck
    {
        private readonly IOptionsMonitor<GCInfoOptions> _options;

        public GCInfoHealthCheck(IOptionsMonitor<GCInfoOptions> options)
        {
            _options = options;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {            
            var options = _options.Get(context.Registration.Name);            

            var allocated = GC.GetTotalMemory(forceFullCollection: false);
            
            var result = allocated >= options.SizeLimit ? context.Registration.FailureStatus : HealthStatus.Healthy;

            var sizeLimit = ByteSize.FromBytes(options.SizeLimit);
            var consuming = ByteSize.FromBytes(allocated);

            return Task.FromResult(new HealthCheckResult(
                result,
                description: $"Api consuming {Math.Round(consuming.Megabytes,2)} MB. Limit: {sizeLimit}"));
        }
    }

    public class GCInfoOptions
    {
        public long SizeLimit { get; set; } = (1024L * 1024L) * 150; //150MB
    }
}
