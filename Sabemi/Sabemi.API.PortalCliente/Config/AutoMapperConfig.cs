﻿using AutoMapper;
using Sabemi.Domain.Entities;
using Sabemi.API.PortalCliente.ViewModels;
using WsSabemi;
using System.Collections.Generic;
using static Sabemi.CrossCutting.Util.Enumerator;
using System;
using Sabemi.API.PortalCliente.Config.Converters;

namespace Sabemi.API.PortalCliente.Config
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<DadosPortalSegurado, User>()
                .ConvertUsing(typeof(DadosPortalSeguradoToUserConverter));

            CreateMap<DadosPortalSegurado, List<Contract>>()
                .ConvertUsing(typeof(DadosPortalSeguradoToContractsConverter));

            CreateMap<DadosPortalSegurado, List<Installment>>()
                .ConvertUsing(typeof(DadosPortalSeguradoToInstallmentsConverter));

            CreateMap<User, DadosClienteAlterar>()
                .ConvertUsing(typeof(UserToDadosClienteAlterarConverter));

            CreateMap<DadosPropostaPortalSegurado, Contract>()
                .ConvertUsing(typeof(DadosPropostaPortalSeguradoToContractByIdConverter));

            CreateMap<User, UserViewModel>()
                .ForMember(destination => destination.Sex, source => source.MapFrom(x => x.Sex.GetText()))
                .ForMember(destination => destination.MaritalStatus, source => source.MapFrom(x => x.MaritalStatus.GetText()))
                .ForMember(destination => destination.State, source => source.MapFrom(x => x.State.GetText()))
                .ForMember(destination => destination.FavoriteContact, source => source.MapFrom(x => x.FavoriteContact.GetText()));

            CreateMap<UserViewModel, User>()
                .ForMember(destination => destination.Sex, source => source.MapFrom(x => x.Sex.ToEnum<Sex>()))
                .ForMember(destination => destination.MaritalStatus, source => source.MapFrom(x => x.MaritalStatus.ToEnum<MaritalStatus>()))
                .ForMember(destination => destination.State, source => source.MapFrom(x => x.State.ToEnum<State>()))
                .ForMember(destination => destination.FavoriteContact, source => source.MapFrom(x => x.FavoriteContact.ToEnum<FavoriteContactType>()));

            //CreateMap<UpdateUserViewModel, User>()
            //   .ForMember(destination => destination.Sex, source => source.MapFrom(x => x.Sex.ToEnum<Sex>()))
            //   .ForMember(destination => destination.MaritalStatus, source => source.MapFrom(x => x.MaritalStatus.ToEnum<MaritalStatus>()));

            CreateMap<ComplaintViewModel, Complaint>()
                .ForMember(destination => destination.Subject, source => source.MapFrom(x => x.IdSubject.ToString().ToEnum<SolicitationType>()));                        

            CreateMap<FAQ, FAQViewModel>()
                .ForMember(destination => destination.IdCategory, source => source.MapFrom(x => x.Category.ValueAsInt()))
                .ForMember(destination => destination.IdSubCategory, source => source.MapFrom(x => x.SubCategory.ValueAsInt()));

            CreateMap<Debit, DebitViewModel>()
                .ForMember(destination => destination.PaymentCode, source => source.MapFrom(x => x.PaymentType.GetText()))
                .ForMember(destination => destination.IdBank, source => source.MapFrom(x => x.Bank.ValueAsInt()))
                .ForMember(destination => destination.IdAccountType, source => source.MapFrom(x => x.AccountType.ValueAsInt()));

            CreateMap<DebitViewModel, Debit>()
                .ForMember(destination => destination.PaymentType, source => source.MapFrom(x => x.PaymentCode.ToEnum<PaymentType>()))
                .ForMember(destination => destination.Bank, source => source.MapFrom(x => x.IdBank.ToString().ToEnum<Bank>()))
                .ForMember(destination => destination.AccountType, source => source.MapFrom(x => x.IdAccountType.ToString().ToEnum<AccountType>()));

            CreateMap<CreditCard, CreditCardViewModel>()
                .ForMember(destination => destination.PaymentCode, source => source.MapFrom(x => x.PaymentType.GetText()));

            CreateMap<CreditCardViewModel, CreditCard>()
                .ForMember(destination => destination.PaymentType, source => source.MapFrom(x => x.PaymentCode.ToEnum<PaymentType>()));

            CreateMap<CertificateViewModel, Certificate>()
                .ForMember(destination => destination.CertificateType, source => source.MapFrom(x => EnumExtensions.FromString<CertificateType>(x.CertificateType)));

            CreateMap<Question, QuestionViewModel>();
            CreateMap<Contract, ContractViewModel>()
                .ForMember(destination => destination.CertificateType, source => source.MapFrom(x => x.CertificateType.Name()));

            CreateMap<DeclarantViewModel, Declarant>()
                .ForMember(destination => destination.Sex, source => source.MapFrom(x => x.Sex.ToEnum<Sex>()))
                .ForMember(destination => destination.MaritalStatus, source => source.MapFrom(x => x.MaritalStatus.ToEnum<MaritalStatus>()));

            CreateMap<InsuredVictimViewModel, InsuredVictim>()
                .ForMember(destination => destination.Sex, source => source.MapFrom(x => x.Sex.ToEnum<Sex>()))
                .ForMember(destination => destination.MaritalStatus, source => source.MapFrom(x => x.MaritalStatus.ToEnum<MaritalStatus>()));

            CreateMap<CasualtyViewModel, CasualtyForm>()
                .ForMember(destination => destination.Type, source => source.MapFrom(x => x.Type.ToEnum<CasualtyType>()));

            CreateMap<IDictionary<QuestionType, Question>, IDictionary<string, Question>>()
                .ConvertUsing(typeof(QuestionTypeDictionaryToStringDictionary<Question>));
            CreateMap<IDictionary<string, string>, IDictionary<QuestionType, string>>()
                .ConvertUsing(typeof(StringDictionaryToQuestionTypeDictionary<string>));            
        }
    }
}
