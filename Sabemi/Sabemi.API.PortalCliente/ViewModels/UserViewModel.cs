﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class UserViewModel
    {
        #region personal data        
        [Required(ErrorMessage = "O Nome é obrigatório")]
        public string Name { get; set; }

        [Key]
        [Required(ErrorMessage = "O CPF é obrigatório")]
        public string CPF { get; set; }
        public string IDNumber { get; set; }
        public string Password { get; set; }

        [Compare(nameof(Password), ErrorMessage = "As senhas devem ser iguais!")]
        public string PasswordRepeat { get; set; }

        [Required(ErrorMessage = "A Data de Nascimento obrigatória")]
        public DateTime? BirthDate { get; set; }

        [Required(ErrorMessage = "O Nome da Mãe é obrigatório")]
        public string MotherName { get; set; }
        public string FatherName { get; set; }        

        [Required(ErrorMessage = "O Sexo é obrigatório")]
        public string Sex { get; set; }        
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string Hometown { get; set; }

        [Required(ErrorMessage = "O Estado Civil é obrigatório")]
        public string MaritalStatus { get; set; }
        #endregion

        #region address data
        public string Neighborhood { get; set; }
        public string Street { get; set; }
        public string AddressNumber { get; set; }
        public string Complement { get; set; }
        public string CEP { get; set; }

        [Required(ErrorMessage = "A Cidade é obrigatória")]
        public string City { get; set; }

        [Required(ErrorMessage = "O Estado(UF) é obrigatório")]
        public string State { get; set; }
        #endregion

        #region contact data
        public string Telephone { get; set; }
        public string Cellphone { get; set; }

        [Required(ErrorMessage = "O E-mail é obrigatório")]
        public string Email { get; set; }
        public string FavoriteContact { get; set; }
        #endregion
    }
}
