﻿using Sabemi.Domain.Entities;
using System;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class DeclarantViewModel
    {
        public string Name { get; set; }
        public string CPF { get; set; }
        public string Sex { get; set; }
        public DateTime BirthDate { get; set; }
        public string MaritalStatus { get; set; }
        public string Attachment { get; set; }
        public string Observation { get; set; }
        public Address Address { get; set; }
    }
}
