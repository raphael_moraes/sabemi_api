﻿using System.Collections.Generic;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class QuestionViewModel
    {
        public string Description { get; set; }
        public List<string> Options { get; set; }
    }
}
