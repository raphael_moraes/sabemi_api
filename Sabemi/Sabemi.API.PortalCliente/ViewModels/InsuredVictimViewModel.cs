﻿using Sabemi.Domain.Entities;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class InsuredVictimViewModel
    {
        public string CPF { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MaritalStatus { get; set; }
        public string Sex { get; set; }
        public Address Address { get; set; }
    }
}
