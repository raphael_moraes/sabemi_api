﻿using System;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class CasualtyViewModel
    {
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public DeclarantViewModel Declarant { get; set; }
        public InsuredVictimViewModel InsuredVictim { get; set; }
    }
}
