﻿using Sabemi.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class ContractViewModel
    {
        public int ID { get; set; }
        public int IdProductType { get; set; }
        public string Name { get; set; }
        public string LuckyNumber { get; set; }
        public DateTime? StartExpiryDate { get; set; }
        public DateTime? EndExpiryDate { get; set; }
        public decimal? Price { get; set; }
        public int CurrentInstallmentNumber { get; set; }
        public int LastInstallmentNumber { get; set; }
        public string PaymentType { get; set; }
        public bool DigitalChannel { get; set; }
        public string CertificateType { get; set; }

        public List<Beneficiary> Beneficiaries { get; set; }
        public List<Benefit> Benefits { get; set; }
    }
}
