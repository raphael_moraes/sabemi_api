﻿namespace Sabemi.API.PortalCliente.ViewModels
{
    public class CreditCardViewModel
    {
        public string PaymentCode { get; set; }
        public string CardHolder { get; set; }
        public string CardNumber { get; set; }
        public string Expiring { get; set; }
        public int SecurityCode { get; set; }
    }
}
