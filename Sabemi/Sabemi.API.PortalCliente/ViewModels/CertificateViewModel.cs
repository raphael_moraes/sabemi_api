﻿using System.ComponentModel.DataAnnotations;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class CertificateViewModel
    {
        [Required]
        public int IdContract { get; set; }
        [Required]
        public int IdProductType { get; set; }
        [Required]
        public string CertificateType { get; set; }
    }
}
