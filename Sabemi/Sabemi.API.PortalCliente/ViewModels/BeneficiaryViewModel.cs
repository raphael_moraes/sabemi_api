﻿namespace Sabemi.API.PortalCliente.ViewModels
{
    public class BeneficiaryViewModel
    {
        public string CPF { get; set; }
        public string Name { get; set; }
        public string IndemnityPercent { get; set; }
        public string KinshipDegree { get; set; }
    }
}
