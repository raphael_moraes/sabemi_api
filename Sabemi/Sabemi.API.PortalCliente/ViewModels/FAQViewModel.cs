﻿namespace Sabemi.API.PortalCliente.ViewModels
{
    public class FAQViewModel
    {
        public int IdCategory { get; set; }
        public int IdSubCategory { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
