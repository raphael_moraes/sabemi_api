﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sabemi.API.PortalCliente.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.API.PortalCliente.ViewModels
{
    //[ModelBinder(typeof(JsonWithFilesFormDataModelBinder), Name = "Complaint")]
    public class ComplaintViewModel
    {
        public int IdSubject { get; set; }

        [Required(ErrorMessage = "O Nome é obrigatório")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Informe um E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe um telefone para contato")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Informe mais detalhes sobre sua denúncia no campo \"Denúncia\"")]
        public string Description { get; set; }

        public string Attachment { get; set; }

        //public string Attachment { get; set; }

        //public IFormFile Attachment { get; set; }
    }
}
