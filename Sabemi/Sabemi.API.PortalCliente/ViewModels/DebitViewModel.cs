﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class DebitViewModel
    {
        public string PaymentCode { get; set; }
        public int IdBank { get; set; }
        public string Agency { get; set; }
        public string CPF { get; set; }
        public string Name { get; set; }
        public int IdAccountType { get; set; }
        public string Account { get; set; }
        public string Digit { get; set; }
    }
}
