﻿namespace Sabemi.API.PortalCliente.ViewModels
{
    public class AnswerQuestionsResponseViewModel
    {
        public bool IsCorrect { get; set; }
    }

    public class GetCellphoneResponseViewModel
    {
        public string CellPhone { get; set; }
    }    

    public class VerifySMSCodeResponseViewModel
    {
        public bool IsValid { get; set; }
    }       
}
