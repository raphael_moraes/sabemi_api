﻿using System.ComponentModel.DataAnnotations;

namespace Sabemi.API.PortalCliente.ViewModels
{
    public class UpdateUserViewModel
    {
        [Key]
        [Required(ErrorMessage = "O CPF é obrigatório")]
        public string CPF { get; set; }
        public string CEP { get; set; }
        public string Hometown { get; set; }
        public string Complement { get; set; }
        public string Street { get; set; }        

        [Required(ErrorMessage = "O Estado Civil é obrigatório")]
        public string MaritalStatus { get; set; }

        [Required(ErrorMessage = "O E-mail é obrigatório")]
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }

        [Required(ErrorMessage = "O Nome é obrigatório")]
        public string Name { get; set; }

        [Required(ErrorMessage = "O Nome da Mãe é obrigatório")]
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public string AddressNumber { get; set; }
        public string IDNumber { get; set; }

        [Required(ErrorMessage = "O Sexo é obrigatório")]
        public string Sex { get; set; }                        
    }
}
