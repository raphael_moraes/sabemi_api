﻿using FluentValidation;
using Sabemi.CrossCutting.Util;
using Sabemi.Domain.Entities;
using System;

namespace Sabemi.Services.Validator
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar o usuário.");
                });

            #region personal info
            RuleFor(c => c.Name)
                .NotEmpty()
                    .WithMessage("Informe um nome.");

            RuleFor(c => c.BirthDate)
                .NotEmpty()
                    .WithMessage("Informe uma data de nascimento.")
                .GreaterThanOrEqualTo(DateTime.Now)
                    .WithMessage("A data de nascimento não pode ser igual ou maior que a data atual.");

            RuleFor(c => c.Nationality)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe uma nacionalidade.");
                });

            RuleFor(c => c.Citizenship)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe uma naturalidade.");
                });

            RuleFor(c => c.Sex)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe um sexo válido.");
                });

            RuleFor(c => c.CPF)
                .NotEmpty()
                .WithMessage("Informe um CPF.")
                .MustAsync(CpfUtil.IsValidAsync)
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("CPF inválido.");
                });

            RuleFor(c => c.MaritalStatus)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe um estado civil válido.");
                });
            #endregion

            #region address info
            RuleFor(c => c.Street)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe um logradouro.");
                });

            RuleFor(c => c.CEP)
                .Matches(RegexpUtil.CEP)
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("CEP inválido.");
                });

            RuleFor(c => c.City)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe uma cidade.");
                });
            
            RuleFor(c => c.State)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe um estado.");
                });

            RuleFor(c => c.AddressNumber)
                .NotEmpty()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Informe um número.");
                });

            #endregion

            #region contact info
            RuleFor(c => c.Email)
                .Matches(RegexpUtil.Email)
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("E-mail inválido.");
                });

            RuleFor(c => c.Cellphone)
                .Matches(RegexpUtil.Cellphone)
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Celular inválido.");
                });

            RuleFor(c => c.Telephone)
                .Matches(RegexpUtil.Telephone)
                .OnAnyFailure(x =>
                {
                    throw new ArgumentException("Telefone inválido.");
                });
            #endregion
        }
    }
}
