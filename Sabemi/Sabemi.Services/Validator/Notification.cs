﻿using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Sabemi.Services.Validator
{
    public class Notification : INotification
    {
        private List<Message> _messages;

        public Notification()
        {
            _messages = new List<Message>();
        }

        public List<Message> GetNotifications()
        {
            return _messages;
        }

        public List<string> GetNotificationsAsStrings()
        {
            return _messages.Select(n => n.Description).ToList();
        }

        public void Handle(Message message)
        {
            _messages.Add(message);
        }

        public bool HasNotification()
        {
            return _messages.Any();
        }

        public void ClearNotifications()
        {
            _messages.Clear();
        }
    }
}
