﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Sabemi.CrossCutting.Util;
using Sabemi.Domain.Configuration;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WsSabemi;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Services
{
    public class LoginService : BaseService, ILoginService
    {
        private readonly AppSettings _appSettings;
        private readonly ICarinaService _carinaService;
        private readonly IQuestionRepository _questionRepository;
        private readonly ILogger<LoginService> _logger;
        private IDistributedCache _cache;

        public LoginService(ICarinaService carinaService,
                            IQuestionRepository questionRepository,
                            INotification notification,
                            IOptions<AppSettings> options,
                            ILogger<LoginService> logger, 
                            IDistributedCache cache) : base(notification)
        {
            _appSettings = options.Value;
            _carinaService = carinaService;
            _questionRepository = questionRepository;
            _logger = logger;
            _cache = cache;
        }

        public async Task<bool> Login(string cpf, string password)
        {
            try
            {
                var userAuth = await _carinaService.ValidateLogin(cpf, password);

                if (!await ValidateUserAuth(userAuth))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar o login do usuário com CPF {cpf}.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<bool> ValidateAssistedMode(string cpf, string password)
        {
            try
            {
                var userAuth = await _carinaService.ValidateLogin(cpf, password);

                if (!await ValidateUserAuth(userAuth))
                    return false;

                if (!userAuth.AssistanceAccess)
                {
                    Notify("O usuário não tem perfil de modo assistido");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao validar o usuário com CPF {cpf}.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<bool> ValidateFirstAccess(string cpf)
        {
            try
            {
                DadosPortalSegurado clientData = await WsServicesFactory.GetUserData(cpf);

                if (!clientData.Sucesso)
                {
                    Notify(clientData.Mensagem);
                    return false;
                }

                var firstAccess = await WsServicesFactory.ValidateFirstAccess(clientData.ClienteAptoLogar.CodigoPessoaFisica);

                if (!firstAccess.IsPrimeiroAcesso)
                    Notify(firstAccess.Mensagem);

                return firstAccess.IsPrimeiroAcesso;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao tentar validar o primeiro acesso do CPF {cpf}.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public IDictionary<QuestionType, Question> GetQuestions(User user, int amount = 2)
        {
            try
            {
                return _questionRepository.Get(user, amount);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar as perguntas do CPF {user.CPF}.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public bool AnswerQuestions(User user, IDictionary<QuestionType, string> answers)
        {
            try
            {
                return _questionRepository.Answer(user, answers);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao responder as perguntas do CPF {user.CPF}.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task SendSMSCode(string phoneNumber)
        {
            try
            {
                string code = Randomizer.RandomSMSCode();
                SaveOnCache(phoneNumber, code);
                await WsServicesFactory.SendSMS(phoneNumber, $"Seu código de acesso é {code}.", _appSettings.WSWallet, "", _appSettings.WSWallet);
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao realizar o envio do SMS. Por favor tente novamente");
                _logger.LogError(ex, ex.Message);
            }
        }

        public bool VerifySMSCode(string phoneNumber, string code)
        {
            try
            {
                var storedCode = GetFromCache(phoneNumber);
                if (storedCode == null)
                {
                    Notify("Não foi possível realizar a validação do código! Gere um novo código ou tente novamente.");
                    return false;
                }

                if (storedCode.Code != code)
                {
                    Notify("O código informado está incorreto!");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao verificar o código SMS.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        private async Task<bool> ValidateUserAuth(UserAuthenticated userAuth)
        {
            if (!userAuth.Authenticated)
            {
                Notify(userAuth.Mensagem);
                return false;
            }

            if (!await _carinaService.ValidateToken(userAuth.Token))
            {
                Notify("Token Inválido");
                return false;
            }

            return true;
        }
        
        private void SaveOnCache(string phoneNumber, string code)
        {
            var data = new SMSCode
            {
                Code = code,
                ExpiresIn = DateTime.Now.AddMinutes(_appSettings.CookieExpirationMinutes)
            };

            TimeSpan Expiration = TimeSpan.FromMinutes(_appSettings.CookieExpirationMinutes);
            DistributedCacheEntryOptions optionsCache = new DistributedCacheEntryOptions().SetAbsoluteExpiration(Expiration);

            _cache.SetString(phoneNumber, JsonConvert.SerializeObject(data), optionsCache);

        }
        private SMSCode GetFromCache(string key)
        {
            var stored = _cache.GetString(key);

            SMSCode @object = null;
            if (!string.IsNullOrEmpty(stored))
                @object = JsonConvert.DeserializeObject<SMSCode>(stored);

            return @object;
        }        


        async Task<GenarateVoucher> ILoginService.ForgetPassword(string cpf)
        {
            try
            {

                var forget = await WsServicesFactory.ForgetPassword(cpf);

                if (!forget.Sucesso)
                    Notify(forget.Mensagem);

                GenarateVoucher rv = new GenarateVoucher();
                rv.Mensagem = forget.Mensagem;
                rv.Sucesso = forget.Sucesso;
                rv.MensagemSMS = forget.MensagemSMS;
                rv.Telefone = forget.Telefone;

                return rv;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao tentar gerar voucher para troca de senha com o CPF {cpf}.");
                _logger.LogError(ex, ex.Message);

                return null;
            }
        }

        async Task<UpdatePassword> ILoginService.UpdatePassword(UpdatePasswordRequest dados)
        {
            try
            {
                RequestDefinirSenha dd = new RequestDefinirSenha();
                dd.Cpf = dados.Cpf;
                dd.Password = dados.Password;
                dd.Voucher = dados.Voucher;
                var update = await WsServicesFactory.UpdatePassword(dd);

                if (!update.Sucesso)
                    Notify(update.Mensagem);

                UpdatePassword up = new UpdatePassword();
                up.Mensagem = update.Mensagem;
                up.Sucesso = update.Sucesso;

                return up;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao tentar atualizar a senha.");
                _logger.LogError(ex, ex.Message);

                return null;
            }
        }

    }

    internal class SMSCode
    {
        public string Code { get; set; }
        public DateTime ExpiresIn { get; set; }
    }
}
