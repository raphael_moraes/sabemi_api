﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services.Admin;
using System;
using System.Threading.Tasks;

namespace Sabemi.Services.Admin
{
    public class AdminConfigurationService : BaseService, IAdminConfigurationService
    {
        private readonly ILogger<AdminConfigurationService> _logger;
        private readonly IAdminConfigurationRepository _repository;

        public AdminConfigurationService(INotification notification,
                                         ILogger<AdminConfigurationService> logger,
                                         IAdminConfigurationRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<AdminConfiguration> Get()
        {
            try
            {
                return await _repository.Get();
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao carregar as configurações.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> Update(AdminConfiguration configuration)
        {
            try
            {
                if(! await _repository.Update(configuration))
                {
                    Notify("Não foi possível alterar os dados do usuário! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao alterar as configurações!");
                _logger.LogError(ex, ex.Message);
                return false;
            }
            throw new NotImplementedException();
        }
    }
}
