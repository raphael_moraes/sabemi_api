﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services.Admin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Services.Admin
{
    public class BlockedAccessService : BaseService, IBlockedAccessService
    {
        private readonly ILogger<BlockedAccessService> _logger;
        private readonly IBlockedAccessRepository _repository;

        public BlockedAccessService(INotification notification, ILogger<BlockedAccessService> logger, IBlockedAccessRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IList<BlockedAccess>> GetAll(string filter, int start, int end)
        {
            try
            {
                return await _repository.GetAll(filter, start, end);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar a lista de acessos bloqueados! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<BlockedAccess> GetBlockedAccess(string id)
        {
            try
            {
                return await _repository.GetBlockedAccess(id);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar a operação! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            try
            {
                return await _repository.GetTotalRecords(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }

        public async Task<bool> Update(BlockedAccess blockedAccess)
        {
            try
            {
                return await _repository.Update(blockedAccess);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar a operação! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}
