﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Services
{
    public class PortalAccessService : BaseService, IPortalAccessService
    {
        private readonly ILogger<PortalAccessService> _logger;
        private readonly IPortalAccessRepository _repository;

        public PortalAccessService(INotification notification, ILogger<PortalAccessService> logger, IPortalAccessRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IList<PortalAccess>> GetAccessPortal(string filter, int start, int end)
        {
            try
            {     
                return await _repository.GetAccessPortal(filter, start, end);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar os acessos ao portal! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<PortalAccess> GetClient(string id)
        {
            try
            {
                return await _repository.GetClient(id);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar a operação! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> UpdateClient(PortalAccess client)
        {
            try
            {
                return await _repository.Update(client);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar a operação! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }        

        public async Task<int> GetTotalRecords(string filter)
        {
            try
            {
                return await _repository.GetTotalRecords(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }
    }
}
