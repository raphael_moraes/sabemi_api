﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services.Admin;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Services.Admin
{
    public class WhiteListService : BaseService, IWhiteListService
    {
        private readonly ILogger<WhiteList> _logger;
        private readonly IWhiteListRepository _repository;

        public WhiteListService(INotification notification, ILogger<WhiteList> logger, IWhiteListRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IList<WhiteList>> GetAll(string filter, int start, int end)
        {
            try
            {
                return await _repository.GetAll(filter, start, end);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar a WhiteList! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            try
            {
                return await _repository.GetTotalRecords(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }

        public async Task<bool> Insert(WhiteList whiteList)
        {
            try
            {
                if (!await _repository.Insert(whiteList))
                {
                    Notify("Não foi possível incluir um novo IP! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro incluir o novo IP na WhiteList!");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}
