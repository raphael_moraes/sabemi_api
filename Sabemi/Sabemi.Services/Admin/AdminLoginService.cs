﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace Sabemi.Services
{
    public class AdminLoginService : BaseService, IAdminLoginService   
    {
        private readonly ILogger<AdminLoginService> _logger;
        private readonly IAdminUserRepository _adminUserRepository;
        private readonly ICarinaService _carinaService;

        public AdminLoginService(ILogger<AdminLoginService> logger,
                                 INotification notification,
                                 IAdminUserRepository adminUserRepository,
                                 ICarinaService carinaService) : base(notification)
        {
            _logger = logger;
            _adminUserRepository = adminUserRepository;
            _carinaService = carinaService;
        }

        public async Task<bool> Login(string username, string password)
        {
            try
            {
                var userAuth = await _carinaService.ValidateLogin(username, password);
                if(!userAuth.Authenticated)
                {
                    Notify(userAuth.Mensagem);
                    return false;
                }

                if (!userAuth.AdminAccess)
                {
                    Notify("Este usuário não tem permissão para acessar o módulo!");
                    return false;
                }

                if (! await _carinaService.ValidateToken(userAuth.Token))
                {
                    Notify("Login Inválido!");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar o login no admin com o usuário '{username}'.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<bool> ChangePassword(string id, string password, string newPassword)
        {
            try
            {
                var adminUser = await _adminUserRepository.Get(id);
                var hasher = new PasswordHasher<AdminUser>();
                
                if (hasher.VerifyHashedPassword(adminUser, adminUser.Cod_Senha, password) == PasswordVerificationResult.Failed)
                {
                    Notify("A senha atual informada está incorreta!");
                    return false;
                }

                adminUser.Cod_Senha = hasher.HashPassword(adminUser, newPassword);
                
                if(! await _adminUserRepository.Update(adminUser))
                {
                    Notify("Não foi possível alterar a senha! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao trocar a senha do usuário.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}
