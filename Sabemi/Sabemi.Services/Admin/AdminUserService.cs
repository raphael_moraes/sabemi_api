﻿using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Sabemi.Services
{
    public class AdminUserService : BaseService, IAdminUserService
    {
        private readonly ILogger<AdminUserService> _logger;
        private readonly IAdminUserRepository _repository;

        public AdminUserService(INotification notification,
                                ILogger<AdminUserService> logger, 
                                IAdminUserRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<AdminUser> Get(string id)
        {
            try
            {
                return await _repository.Get(id);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar o usuário.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }        

        public async Task<IList<AdminUser>> GetAll(string filter, int start, int end)
        {
            try
            {
                return await _repository.GetAll(filter, start, end);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar os usuários do admin.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> Update(AdminUser user)
        {
            try
            {
                if (!await _repository.Update(user))
                {
                    Notify("Não foi possível alterar os dados do usuário! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao alterar os dados do usuário.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<bool> Insert(AdminUser user)
        {
            try
            {
                if(!await _repository.Insert(user))
                {
                    Notify("Não foi possível incluir o novo usuário! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro incluir o novo usuário.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            try
            {
                return await _repository.GetTotalRecords(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }
    }
}
