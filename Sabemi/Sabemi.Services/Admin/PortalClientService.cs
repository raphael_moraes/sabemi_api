﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services.Admin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Services.Admin
{
    public class PortalClientService : BaseService, IPortalClientService
    {
        private readonly ILogger<PortalClientService> _logger;
        private readonly IPortalClientRepository _repository;
        public PortalClientService(INotification notification,
                                   ILogger<PortalClientService> logger,
                                   IPortalClientRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IList<PortalClient>> GetAll(string filter, int start, int end)
        {
            try
            {
                return await _repository.GetAll(filter, start, end);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar os clientes do portal! Tente novamente mais tarde.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<PortalClient> GetClient(string id)
        {
            try
            {
                return await _repository.GetClient(id);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao realizar a operação! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            try
            {
                return await _repository.GetTotalRecords(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }

        public async Task<bool> Update(PortalClient client)
        {
            try
            {
                if (!await _repository.Update(client))
                {
                    Notify("Não foi possível alterar o status do cliente! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao alterar o status do cliente.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}
