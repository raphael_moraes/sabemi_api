﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Repository;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Services.Admin
{
    public class AssistedModeService : BaseService, IAssistedModeService
    {
        private readonly ILogger<AdminUserService> _logger;
        private readonly IAssistedModeRepository _repository;

        public AssistedModeService(INotification notification,
                                   ILogger<AdminUserService> logger,
                                   IAssistedModeRepository repository) : base(notification)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IList<AssistedMode>> GetAll(string filter, int start, int end)
        {
            try
            {
                return await _repository.GetAll(filter, start, end);
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar os IP's em modo assistido! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            try
            {
                return await _repository.GetTotalRecords(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return 0;
            }
        }

        public async Task<bool> Insert(AssistedMode assistedMode)
        {
            try
            {
                if (!await _repository.Insert(assistedMode))
                {
                    Notify("Não foi possível incluir um novo IP! Tente novamente.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro incluir o novo IP em Modo Assistido!");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}
