﻿using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Services
{
    public class ContactService : BaseService, IContactService
    {
        private readonly ILogger<ContractService> _logger;

        public ContactService(INotification notification, ILogger<ContractService> logger) : base(notification)
        {
            _logger = logger;
        }

        public async Task<string> SendComplaint(string cpf, string formSerialized)
        {
            try
            {
                var client = await WsServicesFactory.SendComplaintForm(cpf, formSerialized);
                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                return client.ProtocoloAtendimento;
            }
            catch (Exception ex)
            {
                Notify("Não foi possível enviar a denúncia! Por favor, tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> SendContactUsForm(string cpf, string formSerialized)
        {
            try
            {
                var client =  await WsServicesFactory.SendContactForm(cpf, formSerialized);
                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                return client.ProtocoloAtendimento;
            }
            catch (Exception ex)
            {
                Notify("Não foi possível enviar seu contato! Por favor, tente novamente");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> SendCasualtyNotification(string cpf, CasualtyForm form)
        {
            try
            {
                var client = await WsServicesFactory.SendCasualtyNotification(cpf, form);

                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                return client.ProtocoloAtendimento;
            }
            catch (Exception ex)
            {
                Notify("Não foi possível enviar o sinistro! Por favor, tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<List<Solicitation>> GetSolicitations(string cpf, DateTime? initDate, DateTime? finishDate, string situation)
        {
            try
            {
                WsSabemi.RetornoSolicitacoesCliente client = await WsServicesFactory.GetSolicitations(cpf);

                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                var builder = new SolicitationsBuilder(client.dadosSolicitacoesCliente, initDate, finishDate, situation)
                    .FilterBySituation();

                if (initDate.HasValue && finishDate.HasValue)
                    builder = builder.FilterByRange();
                else
                {
                    builder = builder.FilterByInitDate()
                                     .FilterByFinishDate();
                }

                return builder.Build();
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao buscar as solicitações! Por favor, tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public FAQ GetCommonQuestion(int idCategory, int idSubCategory)
        {
            try
            {
                var faq = new FAQ
                {
                    Category = idCategory.ToEnum<FAQCategory>(),
                    SubCategory = idSubCategory.ToEnum<FAQSubCategory>()
                };

                return faq;
            }
            catch (Exception ex)
            {
                Notify(Error.Internal.Description());
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
