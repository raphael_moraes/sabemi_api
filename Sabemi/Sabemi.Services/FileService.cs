﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Sabemi.Services
{
    public class FileService : BaseService, IFileService
    {
        private readonly ILogger<FileService> _logger;

        public FileService(ILogger<FileService> logger, INotification notification) : base(notification)
        {
            _logger = logger;
        }

        public bool Upload(string cpf, string file, string imgName)
        {

            try
            {
                if (string.IsNullOrEmpty(file))
                {
                    Notify("Forneça uma imagem para este produto!");
                    return false;
                }

                var imageDataByteArray = Convert.FromBase64String(file);
                var filePath = HandlePathFile(imgName);

                if (string.IsNullOrEmpty(filePath))
                    return false;

                File.WriteAllBytes(filePath, imageDataByteArray);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<bool> Upload(string cpf, IFormFile file)
        {
            try
            {
                if (file == null || file.Length == 0)
                {
                    Notify("Forneça uma imagem para este produto!");
                    return false;
                }

                string imgName = Guid.NewGuid() + "_" + file.FileName;
                var filePath = HandlePathFile(imgName);

                if (string.IsNullOrEmpty(filePath))
                    return false;

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }           
        
        private string HandlePathFile(string imgName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "Resources");
            var filePath = Path.Combine(path, imgName);

            if (File.Exists(filePath))
            {
                Notify("Já existe um arquivo com este nome");
                return string.Empty;
            }

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return filePath;
        }
    }
}
