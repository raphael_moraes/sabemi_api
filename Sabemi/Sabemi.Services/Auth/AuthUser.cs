﻿using Microsoft.AspNetCore.Http;
using Sabemi.CrossCutting.Extensions;
using Sabemi.Domain.Interfaces;

namespace Sabemi.Services.Auth
{
    public class AuthUser : IAuthUser
    {
        private readonly IHttpContextAccessor _accessor;

        public AuthUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string GetUserCpf()
        {
            return IsAuthenticated() ? _accessor.HttpContext.User.GetUserId() : string.Empty;
        }

        public string GetUserEmail()
        {
            return IsAuthenticated() ? _accessor.HttpContext.User.GetUserEmail() : string.Empty;
        }

        public string GetUserName()
        {
            return IsAuthenticated() ? _accessor.HttpContext.User.GetUserName() : string.Empty;
        }

        public string GetUserToken()
        {
            return IsAuthenticated() ? _accessor.HttpContext.User.GetUserToken() : string.Empty;
        }

        public bool IsAuthenticated()
        {
            return _accessor.HttpContext.User.Identity.IsAuthenticated;
        }         
    }
}
