﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Sabemi.Domain.Configuration;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Services.Auth
{
    public class AuthorizationManager : IAuthorizationManager
    {
        private readonly BaseSettings _baseSettings;
        private readonly IUserService _userService;
        private IDistributedCache _cache;

        public AuthorizationManager(IOptions<BaseSettings> baseSettings,
                                    IUserService userService,
                                    IDistributedCache cache)
        {
            _baseSettings = baseSettings.Value;
            _userService = userService;
            _cache = cache;
        }

        public bool ValidateCredentials(string cpf, string refreshToken)
        {
            var storedRefreshToken = _cache.GetString(refreshToken);
            if (string.IsNullOrEmpty(storedRefreshToken) || string.IsNullOrEmpty(refreshToken))
                return false;

            RefreshTokenData tokenData = JsonConvert.DeserializeObject<RefreshTokenData>(storedRefreshToken);

            var result = (cpf == tokenData.ID) && (refreshToken == tokenData.RefreshToken);

            if (result)
                _cache.Remove(refreshToken);

            return result;
        }

        public async Task<Authorization> GenerateJWT(string cpf, bool firstAccess = false, bool assistedMode = false)
        {
            var profile = assistedMode ? ProfileEnum.Assisted_Mode : ProfileEnum.Portal;

            var user = await _userService.Get(cpf);

            string encodedToken = EncodeToken(user, firstAccess, profile);

            double expiresIn = assistedMode ? 120 : _baseSettings.ExpirationMinutes; //em modo assistido sessão irá expirar em 2 horas e sem opção de refresh token

            var auth = new Authorization
            {
                AccessToken = encodedToken,
                ExpiresIn = TimeSpan.FromMinutes(expiresIn).TotalSeconds,
                RefreshToken = Guid.NewGuid().ToString().Replace("-", string.Empty)
            };

            if (!assistedMode)
            {
                TimeSpan finalExpiration = TimeSpan.FromMinutes(expiresIn);
                DistributedCacheEntryOptions optionsCache = new DistributedCacheEntryOptions();
                optionsCache.SetAbsoluteExpiration(finalExpiration);

                RefreshTokenData tokenData = new RefreshTokenData
                {
                    ID = cpf,
                    RefreshToken = auth.RefreshToken
                };


                _cache.SetString(auth.RefreshToken, JsonConvert.SerializeObject(tokenData), optionsCache);
            }

            return auth;
        }

        public Authorization GenerateJWT(string login, ProfileEnum profile)
        {
            string encodedToken = EncodeToken(login, profile);
            double expiresIn = profile != ProfileEnum.Assisted_Mode ? _baseSettings.ExpirationMinutes : 2;

            var auth = new Authorization
            {
                AccessToken = encodedToken,
                ExpiresIn = TimeSpan.FromMinutes(expiresIn).TotalSeconds,
                RefreshToken = string.Empty
            };

            return auth;
        }

        private string EncodeToken(User user, bool firstAccess, ProfileEnum profile)
        {
            var key = Encoding.ASCII.GetBytes(_baseSettings.Secret);

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _baseSettings.Issuer,
                Audience = _baseSettings.Audience,
                Expires = DateTime.UtcNow.AddMinutes(_baseSettings.ExpirationMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.CPF),
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim("first_access", firstAccess.ToString()),
                    new Claim(ClaimTypes.Role, profile.Name())
                })
            });

            return tokenHandler.WriteToken(token);
        }

        private string EncodeToken(string identifier, ProfileEnum profile)
        {
            var key = Encoding.ASCII.GetBytes(_baseSettings.Secret);

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _baseSettings.Issuer,
                Audience = _baseSettings.Audience,
                Expires = DateTime.UtcNow.AddMinutes(_baseSettings.ExpirationMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, identifier),
                    new Claim(ClaimTypes.Role, profile.Name())
                })
            });

            return tokenHandler.WriteToken(token);
        }        
        
    }

    internal class RefreshTokenData
    {
        public string ID { get; set; }
        public string RefreshToken { get; set; }
    }    
}
