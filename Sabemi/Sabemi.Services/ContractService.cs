﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WsSabemi;

namespace Sabemi.Services
{
    public class ContractService : BaseService, IContractService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<ContractService> _logger;

        public ContractService(INotification notification, IMapper mapper, ILogger<ContractService> logger) : base(notification)
        {
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<List<Contract>> GetUserContracts(string cpf)
        {
            try
            {
                DadosPortalSegurado client = await WsServicesFactory.GetUserContracts(cpf);

                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                var contracts = _mapper.Map<List<Contract>>(client);

                //Para setar a informação de contrato formalizado por meio digital:
                //Foi necessária a rotina abaixo pois o serviço que retorna a lista de contratos está inconsistente e não trás a informação de meio digital corretamente.
                contracts.ForEach(x => x.DigitalChannel = IsDigitalChannel(cpf, x.ID));                

                return contracts;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao carregar os contratos! Por favor tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        private bool IsDigitalChannel(string cpf, int idContract)
        {
            var contract = GetContractById(idContract, cpf).Result;

            if (contract == null)
                return false;

            return contract.DigitalChannel;
        }

        public async Task<Contract> GetContractById(int contractId, string cpf)
        {
            try
            {
                DadosPropostaPortalSegurado client = await WsServicesFactory.GetContractsById(contractId, cpf);

                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                var contract = _mapper.Map<Contract>(client);

                return contract;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao carregar o contrato! Por favor tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }        

        public async Task<ContractInstallment> GetContractInstallment(int idContract, int idProductType)
        {
            try
            {
                var client = await WsServicesFactory.GetParcelsByProductType(idContract, idProductType);
                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                var contractInstallment = new ContractInstallment
                {
                    IdContract = idContract,
                    LastInstallmentNumber = client.Proposta.ListaParcelas.Length,
                    Installments = _mapper.Map<List<Installment>>(client)
                };

                return contractInstallment;
            }
            catch (Exception ex)
            {
                Notify("Não foi possível buscar as parcelas do contrato! Por favor tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<Benefit>> GetBenefits(int contractId, string cpf)
        {
            try
            {
                var contract = await GetContractById(contractId, cpf);

                if (contract == null)
                {
                    Notify("Contrato não encontrado.");
                    return null; 
                }

                if (!contract.Benefits.Any())
                {
                    Notify("Não foram encontrados benefícios para o contrato informado.");
                    return null;
                }

                return contract.Benefits;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro na busca dos benefícios do contrato {contractId.ToString()}! Por favor tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> GetLuckyNumber(int contractId, string cpf)
        {
            try
            {
                var contract = await GetContractById(contractId, cpf);

                if (contract == null)
                {
                    Notify("Contrato não encontrado!");
                    return string.Empty;
                }

                return contract.LuckyNumber;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao carregar o contrato solicitado! Por favor tente novamente.");
                _logger.LogError(ex, ex.Message);
                return string.Empty;
            }
        }        

        public async Task<string> UpdatePayment(string cpf, Payment payment)
        {
            try
            {                
                var client = await WsServicesFactory.UpdatePayment(cpf, payment);
                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                return client.ProtocoloAtendimento;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao atualizar a forma de pagamento! Por favor tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<bool> ContractExist(int idContract, string cpf)
        {
            var contract = await GetContractById(idContract, cpf);
            if (contract == null)
            {
                Notify("Contrato não encontrado!");
                return false;
            }

            return true;
        }

        public async Task<byte[]> GetCertificate(string cpf, Certificate certificate)
        {
            try
            {
                var result = await WsServicesFactory.GetCertificate(cpf, certificate);
                if(!result.Sucesso)
                {
                    Notify(result.Mensagem);
                    return null;
                }

                if (!result.Certificados.Any() || string.IsNullOrEmpty(result.Certificados[0]))
                {
                    Notify("Certificado não encontrado!");
                    return null;
                }

                byte[] contentFile = Convert.FromBase64String(result.Certificados[0]);
                return contentFile;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao gerar o certificado! Por favor, tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> SendBeneficiary(string cpf, int idContract, string formSerialized)
        {
            try
            {
                var client = await WsServicesFactory.SendBeneficiary(cpf, idContract, formSerialized);
                if (!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                return client.ProtocoloAtendimento;
            }
            catch (Exception ex)
            {
                Notify("Ocorreu um erro ao enviar o(s) beneficiário(s)! Tente novamente.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> RequestCancellation(string cpf, Cancellation cancellation)
        {
            try
            {
                var client = await WsServicesFactory.RequestCancellation(cpf, cancellation);
                if(!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return null;
                }

                return client.ProtocoloAtendimento;
            }
            catch (Exception ex)
            {
                Notify("Não foi possível solicitar o cancelamento! Por favor, tente novamente");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }
    }
}
