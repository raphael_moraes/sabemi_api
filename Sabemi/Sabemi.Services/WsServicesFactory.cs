﻿using Newtonsoft.Json;
using Sabemi.Domain.Entities;
using System;
using System.Threading.Tasks;
using WsSabemi;
using wsSendSMS;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Services
{
    public class WsServicesFactory
    {
        private static SACServicePortalSeguradoContractsClient _wsSabemi;            
        private static NetCoreClient _wsSMS;

        public static void Initialize(string urlWsSabemi, string urlSMS)
        {
            _wsSabemi = new SACServicePortalSeguradoContractsClient(SACServicePortalSeguradoContractsClient.EndpointConfiguration.BasicHttpBinding_SACServicePortalSeguradoContracts, urlWsSabemi);
            _wsSMS = new NetCoreClient(NetCoreClient.EndpointConfiguration.BasicHttpBinding_NetCore, urlSMS);
        }


        public static async Task<RetornoIsPrimeiroAcesso> ValidateFirstAccess(int codigoPessoaFisica)
            => await _wsSabemi.VerificarPrimeiroAcessoAsync(codigoPessoaFisica);

        public static async Task<DadosPortalSegurado> GetUserData(string cpf, DateTime? birthDate)
        {
            if (!birthDate.HasValue)
            {
                var userData = await GetUserData(cpf);
                if (!userData.Sucesso)
                    return userData;

                birthDate = userData.ClienteAptoLogar.DataNascimento;
            }

            return await _wsSabemi.BuscarDadosClienteSABEMIAsync(new FiltroClientePortalSegurado
            {
                CPFcliente = cpf,
                DataNascimento = birthDate
            });
        }

        public static async Task<DadosPortalSegurado> GetUserData(string cpf)
            => await _wsSabemi.VerificarClienteAptoLogarAsync(cpf);

        public static async Task<DadosPortalSegurado> GetUserContracts(string cpf)
        {
            var userData = await GetUserData(cpf);
            if (!userData.Sucesso)
                return userData;

            return await _wsSabemi.BuscarPropostasClienteSABEMIAsync(new FiltroClientePortalSegurado
            {
                CPFcliente = cpf,
                DataNascimento = userData.ClienteAptoLogar.DataNascimento
            });
        }

        public static async Task<DadosPropostaPortalSegurado> GetContractsById(int contractId, string cpf)
        {
            DadosPropostaPortalSegurado result = new DadosPropostaPortalSegurado();
            var userData = await GetUserData(cpf);
            if (!userData.Sucesso)
            {
                result.Sucesso = userData.Sucesso;
                result.Mensagem = userData.Mensagem;
                return result;
            }

            return await _wsSabemi.BuscarPropostaClienteSABEMIByCodAsync(new FiltroPropostaClientePortalSegurado
            {
                CodProposta = contractId,
                CPFcliente = cpf,
                DataNascimento = userData.ClienteAptoLogar.DataNascimento
            });
        }

        public static async Task<DadosPortalSegurado> GetParcelsByProductType(int contractId, int productTypeId)
            => await _wsSabemi.ConsultarSaldoDevedorPropostaAsync(new FiltrosPropostaSaldoDevedor
            {
                CodigoProposta = contractId,
                TipoProduto = productTypeId
            });

        public static async Task<PortalClienteAlteracaoResponseModel> UpdatePayment(string cpf, Payment payment)
        {            
            PaymentType? paymentEnum = EnumExtensions.FromString<PaymentType>(payment.PaymentType);

            var folhaPagamento = new FolhaPagamento();
            if (paymentEnum.Equals(PaymentType.PayrollDiscount))
            {
                if(!ValidatePayrollDiscount(payment))
                {
                    return new PortalClienteAlteracaoResponseModel
                    {
                        Sucesso = false,
                        Mensagem = "Para a forma de pagamento 'Desconto em Folha' é necessário informar os campos: Orgão, Matrícula e UPAG.", 
                        ProtocoloAtendimento = string.Empty
                    };
                }

                folhaPagamento.IdMatricula = payment.RegistrationCode;
                folhaPagamento.Orgao = payment.Organ;
                folhaPagamento.Upag = payment.UPAG;
            }

            var model = new AlterarFormaPagamentoModel
            {
                Cpf = cpf,
                CodProposta = payment.IdContract.ToString(),
                CodTipoProduto = payment.IdProductType,
                NomeCliente = payment.FullName,
                TipoFormaPagamento = Convert.ToInt32(paymentEnum.GetCode()).ToEnum<TipoFormaPagamento>(), 
                FolhaPagamento = folhaPagamento
            };

            return await _wsSabemi.AlterarFormaPagamentoContratoAsync(model);
        }        

        private static bool ValidatePayrollDiscount(Payment payment)
           => !string.IsNullOrEmpty(payment.RegistrationCode) && !string.IsNullOrEmpty(payment.Organ) && !string.IsNullOrEmpty(payment.UPAG);

        public static async Task<RetornoCertificadoCobertura> GetCertificate(string cpf, Certificate certificate)
        {
            var content = new CertificadoCoberturaRequest
            {
                CodigoProposta = certificate.IdContract, 
                CPFCliente = cpf, 
                TipoCertificado = certificate.CertificateType.ValueAsInt(), 
                TipoProduto = certificate.IdProductType
            };

            return await _wsSabemi.GerarCertificadoCoberturaPdfAsync(content);
        }

        public static async Task<RetornoSolicitacoesCliente> GetSolicitations(string cpf) 
            => await _wsSabemi.BuscaDadosSolicitacoesClienteAsync(cpf);

        public static async Task<string> SendSMS(string phoneNumber, string message, string wallet, string content, string userData)
            => await _wsSMS.EnviarSMSAsync(telefone: phoneNumber, mensagem: message, carteira: wallet, retorno: content, dadocliente: userData);

        public static async Task<DadosPortalSegurado> Save(string cpf, DadosClienteAlterar userData)
            => await _wsSabemi.AlterarDadosClienteAsync(cpf, userData);

        public static async Task<RetornoTipoPreferencia> GetMainContact(string cpf)
            => await _wsSabemi.BuscaTipoPreferenciaContatoAsync(cpf);

        public static async Task<RetornoEmail> SendBeneficiary(string cpf, int idContract, string formSerialized)
        {
            var model = GetEnviaEmailModel(cpf, idContract, formSerialized, OperationType.Beneficiary);            

            return await _wsSabemi.EnviaEmailAreasInteressadasAsync(model);
        }

        public static async Task<RetornoEmail> SendContactForm(string cpf, string formSerialized)
        {
            var model = GetEnviaEmailModel(cpf, 0, formSerialized, OperationType.ContactUs);
            return await _wsSabemi.EnviaEmailAreasInteressadasAsync(model);
        }

        public static async Task<RetornoEmail> SendComplaintForm(string cpf, string formSerialized)
        {
            var model = GetEnviaEmailModel(cpf, 0, formSerialized, OperationType.ComplaintPortal);

            return await _wsSabemi.EnviaEmailAreasInteressadasAsync(model);
        }

        public static async Task<RetornoEmail> SendCasualtyNotification(string cpf, CasualtyForm form)
        {
            var @object = new
            {
                TpSinistro = Convert.ToInt32(form.Type.GetCode()), 
                DtSinistro = form.Date, 
                DescricaoSinistro = form.Description,
                Declarante = new
                {
                    DtNascimento = form.Declarant.BirthDate, 
                    Anexo = form.Declarant.Attachment,
                    Observacoes = form.Declarant.Observation,
                    Nome = form.Declarant.Name, 
                    Cpf = form.Declarant.CPF, 
                    EstadoCivil = form.Declarant.MaritalStatus.GetText(), 
                    Sexo = form.Declarant.Sex.ValueAsInt(), 
                    Endereco = new
                    {
                        Cep = form.Declarant.Address.CEP, 
                        Endereco = form.Declarant.Address.Street, 
                        Cidade = form.Declarant.Address.City, 
                        Estado = form.Declarant.Address.State, 
                        Bairro = form.Declarant.Address.Neighborhood
                    }
                }, 
                Segurado = new
                {
                    Telefone = form.InsuredVictim.PhoneNumber, 
                    Email = form.InsuredVictim.Email, 
                    Nome = form.InsuredVictim.Name, 
                    Cpf = form.InsuredVictim.CPF, 
                    EstadoCivil = form.InsuredVictim.MaritalStatus.GetText(),
                    Sexo = form.InsuredVictim.Sex.ValueAsInt(),
                    Endereco = new
                    {
                        Cep = form.InsuredVictim.Address.CEP,
                        Endereco = form.InsuredVictim.Address.Street,
                        Cidade = form.InsuredVictim.Address.City,
                        Estado = form.InsuredVictim.Address.State,
                        Bairro = form.InsuredVictim.Address.Neighborhood
                    }
                }
            };

            string formSerialized = JsonConvert.SerializeObject(@object);

            var model = GetEnviaEmailModel(cpf, 0, formSerialized, OperationType.CasualtyNotification);

            return await _wsSabemi.EnviaEmailAreasInteressadasAsync(model);
        }

        private static EnviaEmailAreasInteressadasModel GetEnviaEmailModel(string cpf, int idContract, string formSerialized, OperationType operationType)
        {
            return new EnviaEmailAreasInteressadasModel
            {
                TipoAtendimento = operationType.ValueAsInt(),
                Cpf = cpf,
                ParametrosJson = formSerialized,
                CodProposta = idContract
            };
        }


        public static async Task<PortalClienteAlteracaoResponseModel> RequestCancellation(string cpf, Cancellation cancellation)
        {
            return await _wsSabemi.CancelarContratoAsync(new AlterarPropostaPortalClienteModel
            {
                Cpf = cpf,
                CodProposta = cancellation.IdContract.ToString(),
                NomeCliente = cancellation.FullName
            });
        }

        public static async Task<RetornoGerenciaVoucher> ForgetPassword(string cpf)
            => await _wsSabemi.RedefinirSenhaGerarVoucherAsync(cpf);

        public static async Task<RetornoDefinirSenha> UpdatePassword(RequestDefinirSenha dados)
            => await _wsSabemi.RedefinirSenhaAtualizarAsync(dados);

    }
}
