﻿using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces;
using Sabemi.Domain.Interfaces.Services;
using System;
using System.Threading.Tasks;
using WsSabemi;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Sabemi.Domain.Configuration;
using static Sabemi.CrossCutting.Util.Enumerator;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Distributed;

namespace Sabemi.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ICarinaService _carinaService;
        private readonly ILogger _logger;
        private IDistributedCache _cache;

        public UserService(INotification notification,
                           IMapper mapper,
                           IOptions<AppSettings> options,
                           ICarinaService carinaService,
                           ILogger<UserService> logger, 
                           IDistributedCache cache) : base(notification)
        {
            _mapper = mapper;
            _appSettings = options.Value;
            _carinaService = carinaService;
            _logger = logger;
            _cache = cache;
        }

        public async Task<bool> RegisterPassword(string cpf, string password)
        {
            try
            {
                if (!await _carinaService.RegisterPassword(cpf, password))
                {
                    Notify("Não foi possível registrar a senha.");
                    return false;
                }

                RemoveFromCache(cpf);

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao criar a senha do usuário {cpf}.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<User> Get(string cpf, DateTime? birthDate = null, bool ignoreCache = false)
        {
            try
            {
                User user = null;
                if (!ignoreCache)
                {
                    UserCacheData userCache = GetFromCache(cpf);

                    // Se houver sessão / usuário na sessão, e o usuário estiver com as informações completas ou são necessárias apenas informações parciais
                    if (userCache?.Data != null && DateTime.Now < userCache.ExpiresIn && (userCache.IsFullData || birthDate == null))
                    {
                        user = userCache.Data;
                    }
                }
                if (user == null)
                {
                    DadosPortalSegurado client = await WsServicesFactory.GetUserData(cpf, birthDate);

                    if (!client.Sucesso)
                    {
                        Notify(client.Mensagem);
                        return null;
                    }
                    
                    user = _mapper.Map<User>(client);
                    user.FavoriteContact = await GetMainContact(cpf);
                    SaveOnCache(user, isFullData: client.DadosCadastraisCliente != null);
                }

                return user;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar usuário CPF: {cpf}.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> GetCellPhone(string cpf, bool masked)
        {
            try
            {
                var user = await Get(cpf);

                if (user != null && string.IsNullOrEmpty(user.Cellphone))
                    Notify("O usuário não possui um número de celular cadastrado.");

                if (!string.IsNullOrEmpty(user?.Cellphone))
                    return masked ? user.Cellphone.CellPhoneMask(masked) : user.Cellphone;

                return null;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao carregar o número de telefone do usuário de CPF {cpf}.");
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }          

        public async Task<string> GetEmail(string cpf)
        {
            try
            {
                var user = await Get(cpf);

                return !string.IsNullOrEmpty(user?.Email) ? user.Email : string.Empty;
            }
            catch (Exception ex)
            {
                Notify(Error.Internal.Description());
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public async Task<string> GetName(string cpf)
        {
            try
            {
                var user = await Get(cpf);

                return !string.IsNullOrEmpty(user?.Name) ? user.Name : string.Empty;
            }
            catch (Exception ex)
            {
                Notify(Error.Internal.Description());
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }        

        public async Task<bool> Update(User user)
        {
            try
            {               
                var userDataToUpdate = _mapper.Map<DadosClienteAlterar>(user);                
                var client = await WsServicesFactory.Save(user.CPF, userDataToUpdate);

                if(!client.Sucesso)
                {
                    Notify(client.Mensagem);
                    return false;
                }

                RemoveFromCache(user.CPF);

                return true;
            }
            catch (Exception ex)
            {
                Notify($"Ocorreu um erro ao alterar os dados do usuário CPF: {user.CPF}.");
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }        

        private async Task<FavoriteContactType?> GetMainContact(string cpf)
        {
            try
            {
                var client = await WsServicesFactory.GetMainContact(cpf);
                if (!client.Sucesso || string.IsNullOrEmpty(client.tipoPreferencia))
                    return null;

                var mainContact = client.tipoPreferencia.ToEnum<FavoriteContactType>();

                return mainContact;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }        

        private UserCacheData GetFromCache(string cpf)
        {
            var storedUser = _cache.GetString(cpf);

            UserCacheData user = null;
            if (!string.IsNullOrEmpty(storedUser))
                user = JsonConvert.DeserializeObject<UserCacheData>(storedUser);

            return user;
        }

        private void SaveOnCache(User user, bool isFullData)
        {
            double expiresIn = _appSettings.CookieExpirationMinutes != 0 ? _appSettings.CookieExpirationMinutes : 60;
            var data = new UserCacheData()
            {
                Data = user,
                ExpiresIn = DateTime.Now.AddMinutes(expiresIn),
                IsFullData = isFullData
            };

            TimeSpan Expiration = TimeSpan.FromMinutes(expiresIn);
            DistributedCacheEntryOptions optionsCache = new DistributedCacheEntryOptions().SetAbsoluteExpiration(Expiration);

            _cache.SetString(user.CPF, JsonConvert.SerializeObject(data), optionsCache);
        }

        private void RemoveFromCache(string cpf)
        {
            _cache.Remove(cpf);
        }
    }
}
