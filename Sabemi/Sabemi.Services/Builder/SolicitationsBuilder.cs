﻿using Sabemi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using WsSabemi;

namespace Sabemi.Services
{
    public class SolicitationsBuilder
    {
        private List<Solicitation> Solicitations { get; set; }
        private DadosSolicitacoesCliente[] Source { get; set; }
        private DateTime? InitDate { get; set; }
        private DateTime? FinishDate { get; set; }
        private string Situation { get; set; }

        public SolicitationsBuilder(DadosSolicitacoesCliente[] source, DateTime? initDate, DateTime? finishDate, string situation)
        {
            Solicitations = new List<Solicitation>();
            Source = source;
            InitDate = initDate;
            FinishDate = finishDate;
            Situation = situation;
        }

        public SolicitationsBuilder FilterBySituation()
        {
            if (!string.IsNullOrEmpty(Situation))
                Source = Source.Where(x => x.Situacao.ToUpper().Contains(Situation.ToUpper())).ToArray();

            return this;
        }

        public SolicitationsBuilder FilterByInitDate()
        {
            if (InitDate.HasValue)
                Source = Source.Where(x => x.InicioAtendimento.Date == InitDate).ToArray();

             return this;
        }

        public SolicitationsBuilder FilterByFinishDate()
        {
            if (FinishDate.HasValue)
                Source = Source.Where(x => x.FimAtendimento.Date == FinishDate).ToArray();

            return this;
        }

        public SolicitationsBuilder FilterByRange()
        {
            Source = Source.Where(x => x.InicioAtendimento.Date >= InitDate && x.FimAtendimento.Date <= FinishDate).ToArray();

            return this;
        }

        public List<Solicitation> Build()
        {            
            foreach (var item in Source)
            {
                var solicitation = new Solicitation
                {
                    Description = item.Descricao,
                    Origin = item.Origem,
                    Protocol = item.Protocolo,
                    InitDate = item.InicioAtendimento,
                    FinishDate = item.FimAtendimento,
                    LastActivity = item.UltimaAtualizacao,
                    Situation = item.Situacao
                };

                Solicitations.Add(solicitation);
            }            

            return Solicitations;
        }
    }
}
