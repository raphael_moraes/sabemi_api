﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces.Services;
using Sabemi.Domain.Configuration;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System.Text.Json;

namespace Sabemi.Services
{
    public class CarinaService : ICarinaService
    {
        private readonly AppSettings _appSettings;
        private readonly ILogger<CarinaService> _logger;
        private readonly HttpClient _httpClient;

        public CarinaService(IOptions<AppSettings> appSettings, IOptions<BaseSettings> baseSettings, ILogger<CarinaService> logger, HttpClient httpClient)
        {
            _appSettings = appSettings.Value;
            _logger = logger;

            httpClient.BaseAddress = new Uri(baseSettings.Value.SabemiAPI);

            _httpClient = httpClient;
        }        

        public async Task<UserAuthenticated> ValidateLogin(string cpf, string password)
        {            
            UserAuthenticated auth = new UserAuthenticated();

            try
            {
                var content = GetContent(new
                {
                    Usuario = cpf,
                    Senha = password
                });

                var response = await _httpClient.PostAsync("api/portalcliente/login", content);
                auth = await DeserializeResponse<UserAuthenticated>(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return auth;
        }

        public async Task<bool> RegisterPassword(string cpf, string password)
        {                        
            try
            {
                var content = GetContent(new
                {
                    Cpf = cpf,
                    Password = password
                });

                var response = await _httpClient.PostAsync(_appSettings.RegisterPasswordAPI, content);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return false;
            }
        }

        public async Task<bool> ValidateToken(Guid token)
        {            
            Token tokenObj = new Token();

            try
            {
                var content = GetContent(new { Token = token });

                var response = await _httpClient.PostAsync("api/token", content);
                tokenObj = await DeserializeResponse<Token>(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return tokenObj.isValid;
        }

        private StringContent GetContent(object data)
        {
            return new StringContent(JsonSerializer.Serialize(data), Encoding.UTF8, "application/json");
        }

        private async Task<T> DeserializeResponse<T>(HttpResponseMessage responseMessage)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };

            return JsonSerializer.Deserialize<T>(await responseMessage.Content.ReadAsStringAsync(), options);
        }
    }
}
