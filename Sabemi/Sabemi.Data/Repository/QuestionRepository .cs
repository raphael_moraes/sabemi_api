﻿using Sabemi.Domain.Entities;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Linq;
using System;
using Sabemi.CrossCutting.Util;
using Microsoft.Extensions.Options;
using Sabemi.Domain.Configuration;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Data.Repository
{
    public class QuestionRepository : IQuestionRepository
    {
        private IDictionary<QuestionType, Question> Questions { get; set; }
        private AppSettings Options { get; set; }
        public QuestionRepository(IOptions<AppSettings> options)
        {
            Options = options.Value;
            Questions = new Dictionary<QuestionType, Question>();

            AddQuestionsToList();
        }

        private void AddQuestionsToList()
        {
            if (Options.IncludeQuestions.Contains(QuestionType.age))
            {
                Questions.Add(QuestionType.age, new AgeQuestion("Qual a sua idade?"));
            }
            if (Options.IncludeQuestions.Contains(QuestionType.homeTown))
            {
                Questions.Add(QuestionType.homeTown, new HometownQuestion("Qual a sua cidade natal?"));
            }
            if (Options.IncludeQuestions.Contains(QuestionType.motherName))
            {
                Questions.Add(QuestionType.motherName, new MotherNameQuestion("Qual o primeiro nome da sua mãe?"));
            }
            if (Options.IncludeQuestions.Contains(QuestionType.fatherName))
            {
                Questions.Add(QuestionType.fatherName, new FatherNameQuestion("Qual o primeiro nome do seu pai?"));
            }
            if (Options.IncludeQuestions.Contains(QuestionType.birthDay))
            {
                Questions.Add(QuestionType.birthDay, new BirthdayQuestion("Qual o dia do seu aniversário?"));
            }
        }

        public IDictionary<QuestionType, Question> Get(User user, int amount)
        {
            IDictionary<QuestionType, Question> picked = new Dictionary<QuestionType, Question>(amount);
            if (Questions.Count == amount)
            {
                foreach (var question in Questions)
                {
                    question.Value.User = user;
                    if (question.Value.HasAnswer)
                    {
                        picked.Add(question);
                    }
                }

                return picked;
            }
            var limit = Questions.Count - 1;
            for (int i = 0; i < amount; i++)
            {
                KeyValuePair<QuestionType, Question> next;
                int tries = 0;
                do
                {
                    next = Questions.ElementAt(Randomizer.Random.Next(limit));
                    next.Value.User = user;
                }
                while (++tries < amount && (!next.Value.HasAnswer || picked.Contains(next)));

                if (tries >= amount)
                    break;

                picked.Add(next);
            }
            return picked;
        }

        public bool Answer(User user, IDictionary<QuestionType, string> answers)
        {
            foreach (var answer in answers)
            {
                if (!Questions.ContainsKey(answer.Key) || !Questions.First(q => answer.Key == q.Key).Value.IsCorrectAnswer(answer.Value, user))
                    return false;
            }
            return true;
        }
    }
}
