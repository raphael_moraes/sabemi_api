﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class BlockedAccessRepository : BaseRepository<BlockedAccess>, IBlockedAccessRepository
    {
        public BlockedAccessRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IList<BlockedAccess>> GetAll(string filter, int start, int end)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append(" WITH BLOQUEIOS AS ");
            SQL.Append(" ( ");
            SQL.Append(" SELECT ROW_NUMBER() OVER (ORDER BY DTA_ACTIVATION_DATE DESC) AS RowNumber, COD_ID, COD_IP, COD_CPF, DTA_ACTIVATION_DATE, STA_ATIVO, DTA_ATUALIZACAO ");
            SQL.Append(" FROM PCBLO_BLOQUEIOS (NOLOCK) ");
            if (!string.IsNullOrEmpty(filter))
                SQL.Append($" WHERE COD_CPF LIKE '%{filter}%' ");

            SQL.Append(" ) ");
            SQL.Append(" SELECT * FROM BLOQUEIOS ");
            SQL.Append($" WHERE RowNumber BETWEEN {start} AND {end}");

            return await SelectMany(SQL.ToString());
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            string SQLRecords = "SELECT COUNT(*) FROM PCBLO_BLOQUEIOS (NOLOCK) ";

            if (!string.IsNullOrEmpty(filter))
                SQLRecords += $"WHERE COD_CPF LIKE '%{filter}%'";

            return await SelectCount(SQLRecords);
        }

        public async Task<BlockedAccess> GetBlockedAccess(string id)
        {
            string SQL = "SELECT COD_ID, COD_IP, COD_CPF, DTA_ACTIVATION_DATE, STA_ATIVO, DTA_ATUALIZACAO FROM PCBLO_BLOQUEIOS (NOLOCK) WHERE COD_ID = @id";

            return await Select(id, SQL);
        }

        public async Task<bool> Update(BlockedAccess blockedAccess)
        {
            string updateSQL = @" UPDATE PCBLO_BLOQUEIOS 
                                  SET STA_ATIVO = @Sta_Ativo, 
                                      DTA_ATUALIZACAO = GETDATE() 
                                  WHERE COD_ID = @Cod_Id";

            return await Update(blockedAccess, updateSQL);
        }
    }
}
