﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private string dbString;

        public BaseRepository(IConfiguration configuration)
        {
            dbString = configuration.GetConnectionString("DatabaseConnection");
        }

        protected SqlConnection GetConnection()
        {
            SqlConnection con = new SqlConnection(dbString);
            return con;
        }

        public void Delete(int id, string SQL)
        {
            using (SqlConnection con = GetConnection())
            {
                con.Open();
                con.Execute(SQL, new { ID = id });
                con.Close();
            }
        }

        public async Task<bool> Insert(TEntity obj, string SQL)
        {
            using (SqlConnection con = GetConnection())
            {
                con.Open();
                var result = await con.ExecuteAsync(SQL, obj);
                con.Close();

                return result > 0;
            }
        }

        public async Task<TEntity> Select(string id, string SQL)
        {
            TEntity result;

            using (SqlConnection con = GetConnection())
            {
                con.Open();
                result = await con.QueryFirstOrDefaultAsync<TEntity>(SQL, new { ID = id });
                con.Close();
            }
            return result;
        }

        public async Task<IList<TEntity>> SelectMany(string SQL)
        {
            List<TEntity> list = null;

            using (SqlConnection con = GetConnection())
            {
                con.Open();
                var result = await con.QueryAsync<TEntity>(SQL);
                list = result.ToList();
                con.Close();
            }

            return list;
        }

        public async Task<bool> Update(TEntity obj, string SQL)
        {
            using (SqlConnection con = GetConnection())
            {
                con.Open();
                var result = await con.ExecuteAsync(SQL, obj);
                con.Close();

                return result > 0;
            }
        }

        public async Task<int> SelectCount(string SQL)
        {
            int result = 0;

            using (SqlConnection con = GetConnection())
            {
                con.Open();
                result = await con.QueryFirstOrDefaultAsync<int>(SQL);
                con.Close();
            }

            return result;
        }
    }
}
