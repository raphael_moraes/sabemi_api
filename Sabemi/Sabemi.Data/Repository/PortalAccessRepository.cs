﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class PortalAccessRepository : BaseRepository<PortalAccess>, IPortalAccessRepository
    {
        public PortalAccessRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IList<PortalAccess>> GetAccessPortal(string filter, int start, int end)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append(" WITH ACESSOS AS ");
            SQL.Append(" ( ");
            SQL.Append(" SELECT ROW_NUMBER() OVER (ORDER BY DTA_DATA DESC) AS RowNumber, COD_ID, COD_IP, COD_CPF, DTA_DATA, STA_BLOQUEADO, DSC_STATUS ");
            SQL.Append(" FROM PCACE_ACESSOS (NOLOCK) ");
            if (!string.IsNullOrEmpty(filter))
                SQL.Append($" WHERE COD_CPF LIKE '%{filter}%' ");

            SQL.Append(" ) ");
            SQL.Append(" SELECT * FROM ACESSOS ");
            SQL.Append($" WHERE RowNumber BETWEEN {start} AND {end}");

            return await SelectMany(SQL.ToString());
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            string SQLRecords = "SELECT COUNT(*) FROM PCACE_ACESSOS (NOLOCK) ";

            if (!string.IsNullOrEmpty(filter))
                SQLRecords += $"WHERE COD_CPF LIKE '%{filter}%'";

            return await SelectCount(SQLRecords);
        }

        public async Task<PortalAccess> GetClient(string id)
        {
            string SQL = "SELECT * FROM PCACE_ACESSOS (NOLOCK) WHERE COD_ID = @id";

            return await Select(id, SQL);
        }

        public async Task<bool> Update(PortalAccess client)
        {
            string updateSQL = @" UPDATE PCACE_ACESSOS 
                                  SET STA_BLOQUEADO = @Sta_Bloqueado 
                                  WHERE COD_ID = @Cod_Id";

            return await Update(client, updateSQL);
        }
    }
}
