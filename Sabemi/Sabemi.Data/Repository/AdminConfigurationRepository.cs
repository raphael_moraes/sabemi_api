﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Linq;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class AdminConfigurationRepository : BaseRepository<AdminConfiguration>, IAdminConfigurationRepository
    {
        public AdminConfigurationRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<AdminConfiguration> Get()
        {
            string SQL = @" SELECT COD_ID, NUM_MAX_USUARIOS_IP, NUM_DIAS_VERIFICACAO, NUM_DIAS_BLOQUEADO, DTA_ATUALIZACAO
                            FROM PCCON_CONFIGURACAO_IP (NOLOCK)
                            ORDER BY DTA_ATUALIZACAO DESC";

            var result = await SelectMany(SQL);

            return result.FirstOrDefault();
        }

        public Task<bool> Update(AdminConfiguration configuration)
        {
            string updateSQL = @"UPDATE PCCON_CONFIGURACAO_IP 
                                 SET NUM_MAX_USUARIOS_IP = @Num_Max_Usuarios_Ip, 
                                     NUM_DIAS_VERIFICACAO = @Num_Dias_Verificacao, 
                                     NUM_DIAS_BLOQUEADO = @Num_Dias_Bloqueado, 
                                     DTA_ATUALIZACAO = GETDATE() 
                                 WHERE COD_ID = @Cod_Id";

            return Update(configuration, updateSQL);
        }
    }
}
