﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class PortalClientRepository : BaseRepository<PortalClient>, IPortalClientRepository
    {
        public PortalClientRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IList<PortalClient>> GetAll(string filter, int start, int end)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("WITH USERS AS");
            SQL.Append(" ( ");
            SQL.Append(" SELECT ROW_NUMBER() OVER (ORDER BY DTA_CRIACAO DESC) AS RowNumber, NUM_CPF, STA_DESATIVACAO, DTA_ATUALIZACAO ");
            SQL.Append(" FROM PCUSR_USUARIOS (NOLOCK) ");
            if (!string.IsNullOrEmpty(filter))
                SQL.Append($" WHERE NUM_CPF LIKE '%{filter}%' ");

            SQL.Append(" ) ");
            SQL.Append(" SELECT * FROM USERS ");
            SQL.Append($" WHERE RowNumber BETWEEN {start} AND {end}");

            return await SelectMany(SQL.ToString());
        }

        public async Task<bool> Update(PortalClient client)
        {
            string updateSQL = @"UPDATE PCUSR_USUARIOS 
                                 SET STA_DESATIVACAO = @Sta_Desativacao, 
                                     DTA_ATUALIZACAO = GETDATE()
                                 WHERE NUM_CPF = @Num_Cpf";

            return await Update(client, updateSQL);
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            string SQLRecords = "SELECT COUNT(*) FROM PCUSR_USUARIOS (NOLOCK) ";

            if (!string.IsNullOrEmpty(filter))
                SQLRecords += $"WHERE NUM_CPF LIKE '%{filter}%'";

            return await SelectCount(SQLRecords);
        }

        public async Task<PortalClient> GetClient(string id)
        {
            string SQL = "SELECT * FROM PCUSR_USUARIOS (NOLOCK) WHERE NUM_CPF = @id";

            return await Select(id, SQL);
        }
    }
}
