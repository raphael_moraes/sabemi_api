﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class AssistedModeRepository : BaseRepository<AssistedMode>, IAssistedModeRepository
    {
        public AssistedModeRepository(IConfiguration configuration) : base(configuration) { }        

        public async Task<IList<AssistedMode>> GetAll(string filter, int start, int end)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("WITH MODO_ASSISTIDO AS");
            SQL.Append(" ( ");
            SQL.Append(" SELECT ROW_NUMBER() OVER (ORDER BY COD_DESCRICAO) AS RowNumber, * ");
            SQL.Append(" FROM PCIPS_IPS_SABEMI (NOLOCK) ");
            if (!string.IsNullOrEmpty(filter))
                SQL.Append($" WHERE COD_IP LIKE '%{filter}%' ");

            SQL.Append(" ) ");
            SQL.Append(" SELECT * FROM MODO_ASSISTIDO ");
            SQL.Append($" WHERE RowNumber BETWEEN {start} AND {end}");

            return await SelectMany(SQL.ToString());
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            string SQLRecords = "SELECT COUNT(*) FROM PCIPS_IPS_SABEMI (NOLOCK) ";

            if (!string.IsNullOrEmpty(filter))
                SQLRecords += $"WHERE COD_IP LIKE '%{filter}%'";

            return await SelectCount(SQLRecords);
        }

        public async Task<bool> Insert(AssistedMode assistedMode)
        {
            string insertSQL = @"INSERT INTO PCIPS_IPS_SABEMI (COD_ID, COD_IP, COD_STATUS, COD_DESCRICAO, DTA_CRIACAO, DTA_ATUALIZACAO) 
                                 VALUES (@Cod_Id, @Cod_Ip, @Cod_Status, @Cod_Descricao, GETDATE(), GETDATE())";

            return await Insert(assistedMode, insertSQL);
        }
    }
}
