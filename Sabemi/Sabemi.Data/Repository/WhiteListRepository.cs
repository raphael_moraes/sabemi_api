﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class WhiteListRepository : BaseRepository<WhiteList>, IWhiteListRepository
    {
        public WhiteListRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IList<WhiteList>> GetAll(string filter, int start, int end)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("WITH WHITE_LIST AS");
            SQL.Append(" ( ");
            SQL.Append(" SELECT ROW_NUMBER() OVER (ORDER BY COD_DESCRICAO) AS RowNumber, COD_ID, COD_IP, COD_DESCRICAO, DTA_CRIACAO, DTA_ATUALIZACAO ");
            SQL.Append(" FROM PCEXC_EXCECAO (NOLOCK) ");
            if (!string.IsNullOrEmpty(filter))
                SQL.Append($" WHERE COD_IP LIKE '%{filter}%' ");

            SQL.Append(" ) ");
            SQL.Append(" SELECT * FROM WHITE_LIST ");
            SQL.Append($" WHERE RowNumber BETWEEN {start} AND {end}");

            return await SelectMany(SQL.ToString());
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            string SQLRecords = "SELECT COUNT(*) FROM PCEXC_EXCECAO (NOLOCK) ";

            if (!string.IsNullOrEmpty(filter))
                SQLRecords += $"WHERE COD_IP LIKE '%{filter}%'";

            return await SelectCount(SQLRecords);
        }

        public async Task<bool> Insert(WhiteList whiteList)
        {
            string insertSQL = @"INSERT INTO PCEXC_EXCECAO (COD_ID, COD_IP, COD_DESCRICAO, DTA_CRIACAO, DTA_ATUALIZACAO) 
                                 VALUES (@Cod_Id, @Cod_Ip, @Cod_Descricao, GETDATE(), GETDATE())";

            return await Insert(whiteList, insertSQL);
        }
    }
}
