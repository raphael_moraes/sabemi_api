﻿using Microsoft.Extensions.Configuration;
using Sabemi.Domain.Entities.Admin;
using Sabemi.Domain.Interfaces.Repository;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Data.Repository
{
    public class AdminUserRepository : BaseRepository<AdminUser>, IAdminUserRepository
    {
        public AdminUserRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<AdminUser> Get(string id)
        {
            string SQL = $@"SELECT COD_ID, COD_LOGIN, COD_NIVEL_ACESSO, COD_SENHA, DTA_CRIACAO, DTA_ATUALIZACAO, STA_ATIVO
                            FROM PCADM_ADMIN (NOLOCK) 
                            WHERE COD_ID = '{id}'";

            return await Select(id, SQL);
        }

        public async Task<IList<AdminUser>> GetAll(string filter, int start, int end)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.Append("WITH ADMINUSERS AS");
            SQL.Append(" ( ");
            SQL.Append(" SELECT ROW_NUMBER() OVER (ORDER BY DTA_ATUALIZACAO DESC) AS RowNumber, COD_ID, COD_LOGIN, COD_NIVEL_ACESSO, COD_SENHA, DTA_CRIACAO, DTA_ATUALIZACAO, STA_ATIVO ");
            SQL.Append(" FROM PCADM_ADMIN (NOLOCK) ");
            if (!string.IsNullOrEmpty(filter))
                SQL.Append($" WHERE COD_LOGIN LIKE '%{filter}%' ");

            SQL.Append(" ) ");
            SQL.Append(" SELECT * FROM ADMINUSERS ");
            SQL.Append($" WHERE RowNumber BETWEEN {start} AND {end}");

            return await SelectMany(SQL.ToString());
        }

        public async Task<bool> Insert(AdminUser user)
        {
            string insertSQL = @"INSERT INTO PCADM_ADMIN (COD_ID, COD_LOGIN, COD_NIVEL_ACESSO, COD_SENHA, DTA_CRIACAO, DTA_ATUALIZACAO, STA_ATIVO) 
                                 VALUES (@Cod_Id, @Cod_Login, @Cod_Nivel_Acesso, @Cod_Senha, @Dta_Criacao, @Dta_Atualizacao, @Sta_Ativo)";

            return await Insert(user, insertSQL);
        }

        public async Task<bool> Update(AdminUser user)
        {
            string updateSQL = @"UPDATE PCADM_ADMIN 
                                 SET COD_LOGIN = @Cod_Login, 
                                     COD_NIVEL_ACESSO = @Cod_Nivel_Acesso, 
                                     COD_SENHA = @Cod_Senha, 
                                     DTA_CRIACAO = @Dta_Criacao, 
                                     DTA_ATUALIZACAO = @Dta_Atualizacao, 
                                     STA_ATIVO = @Sta_Ativo
                                 WHERE COD_ID = @Cod_Id";

            return await Update(user, updateSQL);
        }

        public async Task<int> GetTotalRecords(string filter)
        {
            string SQLRecords = "SELECT COUNT(*) FROM PCADM_ADMIN (NOLOCK) ";

            if (!string.IsNullOrEmpty(filter))
                SQLRecords += $"WHERE COD_LOGIN LIKE '%{filter}%'";

            return await SelectCount(SQLRecords);
        }
    }
}
