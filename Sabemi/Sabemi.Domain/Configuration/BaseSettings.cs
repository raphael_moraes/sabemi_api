﻿namespace Sabemi.Domain.Configuration
{
    public class BaseSettings
    {
        public string SabemiAPI { get; set; }

        #region JWT
        public string Secret { get; set; }
        public int ExpirationMinutes { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        #endregion
    }
}
