﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Configuration
{
    public class AppSettings : BaseSettings
    {
        public string RegisterPasswordAPI { get; set; }
        public QuestionType[] IncludeQuestions { get; set; }
        public string WSWallet { get; set; }
        public double CookieExpirationMinutes { get; set; }
        public string SabemiWebService { get; set; }
        public string SMSWebService { get; set; }
    }
}
