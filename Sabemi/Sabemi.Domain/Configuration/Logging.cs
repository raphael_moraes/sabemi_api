﻿namespace Sabemi.Domain.Configuration
{
    public class Logging
    {
        public object LogLevel { get; set; }
        public int? LogFileSizeLimitMb { get; set; }
        public int? LogFileCountLimit { get; set; }
        public string LogDirectory { get; set; }
        public string LogFileExtension { get; set; }
    }
}
