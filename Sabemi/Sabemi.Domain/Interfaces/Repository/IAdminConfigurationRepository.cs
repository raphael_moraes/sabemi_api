﻿using Sabemi.Domain.Entities.Admin;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IAdminConfigurationRepository : IRepository<AdminConfiguration>
    {
        Task<AdminConfiguration> Get();
        Task<bool> Update(AdminConfiguration configuration);
    }
}
