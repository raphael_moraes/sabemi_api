﻿using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IAssistedModeRepository : IRepository<AssistedMode>
    {
        Task<IList<AssistedMode>> GetAll(string filter, int start, int end);
        Task<int> GetTotalRecords(string filter);
        Task<bool> Insert(AssistedMode assistedMode);
    }
}
