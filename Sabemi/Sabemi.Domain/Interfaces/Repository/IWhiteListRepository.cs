﻿using Sabemi.Domain.Entities.Admin;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IWhiteListRepository : IRepository<WhiteList>
    {
        Task<IList<WhiteList>> GetAll(string filter, int start, int end);
        Task<int> GetTotalRecords(string filter);
        Task<bool> Insert(WhiteList whiteList);
    }
}
