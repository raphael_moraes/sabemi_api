﻿using Sabemi.Domain.Entities;
using System.Collections.Generic;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IQuestionRepository 
    {
        IDictionary<QuestionType, Question> Get(User user, int amount);
        bool Answer(User user, IDictionary<QuestionType, string> answers);
    }
}
