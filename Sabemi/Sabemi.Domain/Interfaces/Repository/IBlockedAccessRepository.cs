﻿using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IBlockedAccessRepository : IRepository<BlockedAccess>
    {
        Task<IList<BlockedAccess>> GetAll(string filter, int start, int end);
        Task<int> GetTotalRecords(string filter);
        Task<BlockedAccess> GetBlockedAccess(string id);
        Task<bool> Update(BlockedAccess blockedAccess);
    }
}
