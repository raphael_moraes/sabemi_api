﻿using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IAdminUserRepository : IRepository<AdminUser>
    {
        Task<AdminUser> Get(string id);
        Task<IList<AdminUser>> GetAll(string filter, int start, int end);
        Task<bool> Insert(AdminUser user);
        Task<bool> Update(AdminUser user);
        Task<int> GetTotalRecords(string filter);
    }
}
