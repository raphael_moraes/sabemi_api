﻿using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IPortalAccessRepository : IRepository<PortalAccess>
    {
        Task<IList<PortalAccess>> GetAccessPortal(string filter, int start, int end);
        Task<int> GetTotalRecords(string filter);
        Task<PortalAccess> GetClient(string id);
        Task<bool> Update(PortalAccess client);
    }
}
