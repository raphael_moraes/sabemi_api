﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<bool> Insert(TEntity obj, string SQL);

        Task<bool> Update(TEntity obj, string SQL);

        void Delete(int id, string SQL);

        Task<TEntity> Select(string id, string SQL);

        Task<IList<TEntity>> SelectMany(string SQL);
        Task<int> SelectCount(string SQL);
    }   
}
