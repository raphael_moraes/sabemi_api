﻿using Sabemi.Domain.Entities;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Interfaces
{
    public interface IAuthorizationManager
    {
        Task<Authorization> GenerateJWT(string cpf, bool firstAccess = false, bool assistedMode = false);
        Authorization GenerateJWT(string login, ProfileEnum profile);
        bool ValidateCredentials(string cpf, string refreshToken);
    }
}
