﻿namespace Sabemi.Domain.Interfaces
{
    public interface IAuthUser
    {
        string GetUserCpf();
        string GetUserEmail();
        bool IsAuthenticated();
        string GetUserToken();
        string GetUserName();
    }
}
