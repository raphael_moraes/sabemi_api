﻿using Sabemi.Domain.Entities;
using System.Collections.Generic;

namespace Sabemi.Domain.Interfaces
{
    public interface INotification
    {
        bool HasNotification();
        List<Message> GetNotifications();
        void Handle(Message message);
        void ClearNotifications();
        List<string> GetNotificationsAsStrings();
    }
}
