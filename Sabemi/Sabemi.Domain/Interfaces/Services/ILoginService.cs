﻿using Sabemi.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface ILoginService
    {
        Task<bool> Login(string cpf, string password);
        Task<bool> ValidateAssistedMode(string cpf, string password);
        Task<bool> ValidateFirstAccess(string cpf);
        IDictionary<QuestionType, Question> GetQuestions(User user, int amount = 2);
        bool AnswerQuestions(User user, IDictionary<QuestionType, string> answers);
        Task SendSMSCode(string phoneNumber);                
        bool VerifySMSCode(string phoneNumber, string code);
        Task<GenarateVoucher> ForgetPassword(string cpf);
        Task<UpdatePassword> UpdatePassword(UpdatePasswordRequest dados);
    }
}
