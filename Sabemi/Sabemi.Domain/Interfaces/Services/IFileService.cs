﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IFileService
    {
        bool Upload(string cpf, string file, string imgName);
        Task<bool> Upload(string cpf, IFormFile file);
    }
}
