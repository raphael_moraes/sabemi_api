﻿using Sabemi.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IContractService
    {
        Task<List<Contract>> GetUserContracts(string cpf);
        Task<string> GetLuckyNumber(int contractId, string cpf);
        Task<IEnumerable<Benefit>> GetBenefits(int contractId, string cpf);
        Task<ContractInstallment> GetContractInstallment(int idContract, int idProductType);
        Task<string> UpdatePayment(string cpf, Payment payment);
        Task<Contract> GetContractById(int contractId, string cpf);
        Task<bool> ContractExist(int idContract, string cpf);
        Task<byte[]> GetCertificate(string cpf, Certificate certificate);
        Task<string> SendBeneficiary(string cpf, int idContract, string formSerialized);
        Task<string> RequestCancellation(string cpf, Cancellation cancellation);
    }
}
