﻿using Sabemi.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface ICarinaService
    {
        Task<bool> ValidateToken(Guid token);

        Task<UserAuthenticated> ValidateLogin(string cpf, string password);
        Task<bool> RegisterPassword(string cpf, string password);
    }
}
