﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sabemi.Domain.Entities.Admin;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IAdminUserService
    {
        Task<bool> Update(AdminUser user);
        Task<AdminUser> Get(string id);
        Task<IList<AdminUser>> GetAll(string filter, int start, int end);
        Task<bool> Insert(AdminUser user);
        Task<int> GetTotalRecords(string filter);
    }
}
