﻿using Sabemi.Domain.Entities.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services.Admin
{
    public interface IPortalClientService
    {
        Task<IList<PortalClient>> GetAll(string filter, int start, int end);
        Task<bool> Update(PortalClient client);
        Task<int> GetTotalRecords(string filter);
        Task<PortalClient> GetClient(string id);
    }
}
