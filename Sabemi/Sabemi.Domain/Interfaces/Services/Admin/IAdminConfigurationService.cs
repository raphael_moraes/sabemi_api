﻿using Sabemi.Domain.Entities.Admin;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services.Admin
{
    public interface IAdminConfigurationService
    {
        Task<AdminConfiguration> Get();
        Task<bool> Update(AdminConfiguration configuration);
    }
}
