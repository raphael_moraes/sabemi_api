﻿using Sabemi.Domain.Entities.Admin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IPortalAccessService
    {
        Task<IList<PortalAccess>> GetAccessPortal(string filter, int start, int end);
        Task<PortalAccess> GetClient(string id);
        Task<bool> UpdateClient(PortalAccess client);        
        Task<int> GetTotalRecords(string filter);
    }
}
