﻿using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IAdminLoginService
    {
        Task<bool> Login(string username, string password);
        Task<bool> ChangePassword(string id, string password, string newPassword);
    }
}
