﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sabemi.Domain.Entities;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IUserService
    {
        Task<bool> Update(User user);
        Task<User> Get(string cpf, DateTime? birthDate = null, bool ignoreCache = false);
        Task<string> GetCellPhone(string cpf, bool masked = false);
        Task<string> GetEmail(string cpf);
        Task<string> GetName(string cpf);        
        Task<bool> RegisterPassword(string cpf, string password);        
    }
}
