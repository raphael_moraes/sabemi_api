﻿using Sabemi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sabemi.Domain.Interfaces.Services
{
    public interface IContactService
    {
        Task<string> SendContactUsForm(string cpf, string formSerialized);
        Task<string> SendComplaint(string cpf, string formSerialized);
        Task<string> SendCasualtyNotification(string cpf, CasualtyForm form);
        Task<List<Solicitation>> GetSolicitations(string cpf, DateTime? initDate, DateTime? finishDate, string situation);
        FAQ GetCommonQuestion(int idCategory, int idSubCategory);
    }
}
