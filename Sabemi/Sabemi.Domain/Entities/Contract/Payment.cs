﻿using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities
{
    public class Payment
    {
        [Required]
        public int IdContract { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public int IdProductType { get; set; }
        [Required]
        public string PaymentType { get; set; }
        public string RegistrationCode { get; set; }
        public string Organ { get; set; }
        public string UPAG { get; set; }
    }
}
