﻿using System;
using System.Collections.Generic;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class Contract : BaseEntity
    {
        public int IdProductType { get; set; }
        public ProductType? Product { get; set; }
        public string CPF { get; set; }        
        public string ProductType { get; set; }
        public string Name { get; set; }
        public string ProductTradeName { get; set; }
        public string ProductColor { get; set; }
        public string LuckyNumber { get; set; } 
        public DateTime? StartExpiryDate { get; set; }
        public DateTime? EndExpiryDate { get; set; }
        public string RegistrationCode { get; set; }
        public string OrganName { get; set; }
        public decimal? Price { get; set; }
        public int CurrentInstallmentNumber { get; set; }
        public int LastInstallmentNumber { get; set; }
        public string PaymentType { get; set; }
        public bool DigitalChannel { get; set; }
        public CertificateType CertificateType { get; set; }

        public List<Beneficiary> Beneficiaries { get; set; } = new List<Beneficiary>();
        public List<Benefit> Benefits { get; set; } = new List<Benefit>();       
    }
}
