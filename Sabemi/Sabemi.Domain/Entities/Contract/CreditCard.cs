﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class CreditCard
    {
        public PaymentType? PaymentType { get; set; }
        public string CardHolder { get; set; }
        public string CardNumber { get; set; }
        public string Expiring { get; set; }
        public int SecurityCode { get; set; }
    }
}
