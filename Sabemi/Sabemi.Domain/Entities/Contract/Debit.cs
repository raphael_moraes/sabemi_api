﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class Debit
    {
        public PaymentType? PaymentType { get; set; }
        public Bank Bank { get; set; }
        public string Agency { get; set; }
        public string CPF { get; set; }
        public string Name { get; set; }
        public AccountType AccountType { get; set; }
        public string Account { get; set; }
        public string Digit { get; set; }
    }
}
