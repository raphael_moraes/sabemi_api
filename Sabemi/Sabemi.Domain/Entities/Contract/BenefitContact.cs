﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sabemi.Domain.Entities
{
    public class BenefitContact
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Url { get; set; }
    }
}
