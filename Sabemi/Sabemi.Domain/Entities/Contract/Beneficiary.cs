﻿namespace Sabemi.Domain.Entities
{
    public class Beneficiary
    {
        public int IdContract { get; set; }
        public string CPF { get; set; }
        public string Name { get; set; }
        public int IndemnityPercent { get; set; }
        public string KinshipDegree { get; set; }
    }
}
