﻿using System.Collections.Generic;

namespace Sabemi.Domain.Entities
{
    public class ContractInstallment
    {
        public int IdContract { get; set; }
        public int LastInstallmentNumber { get; set; }
        public List<Installment> Installments { get; set; } = new List<Installment>();
    }
}
