﻿namespace Sabemi.Domain.Entities
{
    public class Benefit
    {
        public int ID { get; set; }
        public int IdContract { get; set; }
        public string Title { get; set; }        
        public string Description { get; set; }
        public string Icon { get; set; }
        public BenefitContact Contact { get; set; }
    }
}
