﻿namespace Sabemi.Domain.Entities
{
    public class Cancellation
    {
        public int IdContract { get; set; }
        public string FullName { get; set; }
    }
}
