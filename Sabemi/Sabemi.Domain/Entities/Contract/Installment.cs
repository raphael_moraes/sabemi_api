﻿using System;

namespace Sabemi.Domain.Entities
{
    public class Installment 
    {
        public DateTime ExpirationDate { get; set; }
        public int InstallmentNumber { get; set; }
        public bool Status { get; set; }
        public decimal Price { get; set; }
        public string Period { get; set; }
        public DateTime? PaymentDate { get; set; }
    }
}
