﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class Certificate
    {
        public int IdContract { get; set; }
        public int IdProductType { get; set; }
        public CertificateType CertificateType { get; set; }
    }
}
