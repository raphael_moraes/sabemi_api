﻿using System;
using System.Collections.Generic;
using Sabemi.CrossCutting.Util;

namespace Sabemi.Domain.Entities
{
    public class MotherNameQuestion : Question
    {
        public override bool HasAnswer => !string.IsNullOrEmpty(User?.MotherName);
        public MotherNameQuestion(string description) : base(description)
        {
        }

        protected override IList<string> GenerateOptions()
        {
            if (string.IsNullOrEmpty(User?.MotherName)) return null;
            var options = Randomizer.RandomFemaleNames(4, User.MotherName.FirstNameOnly());
            options.Add(User.MotherName.FirstNameOnly());
            return options;
        }

        public override bool IsCorrectAnswer(string answer, User user)
        {
            return answer.ToLower() == user.MotherName.FirstNameOnly().ToLower();
        }
    }
}
