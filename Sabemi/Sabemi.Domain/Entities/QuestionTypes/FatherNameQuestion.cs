﻿using System;
using Sabemi.Domain.Interfaces;
using System.Collections.Generic;
using System.Text;
using Sabemi.CrossCutting.Util;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class FatherNameQuestion : Question
    {
        public override bool HasAnswer => !string.IsNullOrEmpty(User?.FatherName);
        public FatherNameQuestion(string description) : base(description)
        {
        }

        protected override IList<string> GenerateOptions()
        {
            if (string.IsNullOrEmpty(User?.FatherName)) return null;
            var options = Randomizer.RandomMaleNames(4, User.FatherName);
            options.Add(User.FatherName.Split(' ')[0].ToCapital());
            return options;
        }

        public override bool IsCorrectAnswer(string answer, User user)
        {
            return answer.ToLower() == user.FatherName.ToLower();
        }
    }
}
