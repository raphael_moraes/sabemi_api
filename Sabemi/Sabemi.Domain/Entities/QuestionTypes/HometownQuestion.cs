﻿using System;
using Sabemi.Domain.Interfaces;
using System.Collections.Generic;
using System.Text;
using Sabemi.CrossCutting.Util;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class HometownQuestion : Question
    {
        public override bool HasAnswer => !string.IsNullOrEmpty(User?.Hometown);
        public HometownQuestion(string description) : base(description)
        {
        }

        protected override IList<string> GenerateOptions()
        {
            if (string.IsNullOrEmpty(User?.Hometown)) return null;
            var options = Randomizer.RandomCityNames(4, User.Hometown);
            options.Add(User.Hometown.ToCapital());
            return options;
        }

        public override bool IsCorrectAnswer(string answer, User user)
        {
            return answer.ToLower() == user.Hometown.ToLower();
        }
    }
}
