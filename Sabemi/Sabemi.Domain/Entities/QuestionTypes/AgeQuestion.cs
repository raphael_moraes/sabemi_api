﻿using System.Collections.Generic;
using Sabemi.CrossCutting.Util;

namespace Sabemi.Domain.Entities
{
    public class AgeQuestion : Question
    {
        public override bool HasAnswer => (User?.Age ?? 0) != 0;

        public AgeQuestion(string description) : base(description)
        {
        }

        protected override IList<string> GenerateOptions()
        {
            var options = Randomizer.RandomAges(4, User.Age);
            options.Add(User.Age.ToString());
            return options;
        }

        public override bool IsCorrectAnswer(string answer, User user)
        {
            return answer == user.Age.ToString();
        }
    }
}
