﻿using System;
using Sabemi.Domain.Interfaces;
using System.Collections.Generic;
using System.Text;
using Sabemi.CrossCutting.Util;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class BirthdayQuestion : Question
    {
        public override bool HasAnswer => User?.BirthDate != null;

        public BirthdayQuestion(string description) : base(description)
        {
        }

        protected override IList<string> GenerateOptions()
        {
            if (User?.BirthDate == null) return null;
            var options = Randomizer.RandomMonthDays(4, User.BirthDate.Value.Day);
            options.Add(User.BirthDate.Value.Day.ToString());
            return options;
        }

        public override bool IsCorrectAnswer(string answer, User user)
        {
            return user?.BirthDate != null && answer == user.BirthDate.Value.Day.ToString();
        }
    }
}
