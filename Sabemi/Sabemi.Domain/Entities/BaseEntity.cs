﻿using System;

namespace Sabemi.Domain.Entities
{
    public abstract class BaseEntity
    {
        public int ID { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }        
    }
}
