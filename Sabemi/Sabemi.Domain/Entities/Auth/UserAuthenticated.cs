﻿using System;

namespace Sabemi.Domain.Entities
{
    public class UserAuthenticated
    {
        public bool Authenticated { get; set; }
        public string Mensagem { get; set; }     
        public Guid Token { get; set; }
        public bool AdminAccess { get; set; }
        public bool AssistanceAccess { get; set; }
    }
}
