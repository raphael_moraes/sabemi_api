﻿namespace Sabemi.Domain.Entities
{
    public class GenarateVoucher
    {
        public string Mensagem { get; set; }
        public string MensagemSMS { get; set; }
        public bool Sucesso { get; set; }
        public string Telefone { get; set; }
    }
}