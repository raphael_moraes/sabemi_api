﻿namespace Sabemi.Domain.Entities
{
    public class Authorization
    {
        public string AccessToken { get; set; }
        public double ExpiresIn { get; set; }
        public string RefreshToken { get; set; }
    }
}
