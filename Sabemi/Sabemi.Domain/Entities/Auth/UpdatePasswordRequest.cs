﻿namespace Sabemi.Domain.Entities
{
    public class UpdatePasswordRequest
    {
        public string Cpf { get; set; }
        public string Password { get; set; }
        public string Voucher { get; set; }
    }
}
