﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class InsuredVictim
    {
        public string CPF { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public MaritalStatus? MaritalStatus { get; set; }
        public Sex? Sex { get; set; }
        public Address Address { get; set; }
    }
}
