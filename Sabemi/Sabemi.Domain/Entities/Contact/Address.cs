﻿namespace Sabemi.Domain.Entities
{
    public class Address
    {
        public string CEP { get; set; }
        public string Street { get; set; }
        public string Neighborhood { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
