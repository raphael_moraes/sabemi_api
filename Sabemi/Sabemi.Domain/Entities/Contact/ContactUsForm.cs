﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class ContactUsForm
    {
        public SolicitationType ContactCode => SolicitationType.Doubt;

        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Message { get; set; }
    }
}
