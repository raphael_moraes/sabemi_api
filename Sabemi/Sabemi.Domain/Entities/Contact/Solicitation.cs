﻿using System;

namespace Sabemi.Domain.Entities
{
    public class Solicitation
    {
        public string Description { get; set; }
        public string Origin { get; set; }
        public long Protocol { get; set; }
        public DateTime? InitDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public DateTime? LastActivity { get; set; }
        public string Situation { get; set; }
    }
}
