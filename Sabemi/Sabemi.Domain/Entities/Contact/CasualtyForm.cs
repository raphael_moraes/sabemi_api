﻿using System;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class CasualtyForm
    {
        public CasualtyType? Type { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public Declarant Declarant { get; set; }
        public InsuredVictim InsuredVictim { get; set; }
    }
}
