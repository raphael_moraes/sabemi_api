﻿using System;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class Declarant
    {
        public string Name { get; set; }
        public string CPF { get; set; }
        public Sex? Sex { get; set; }
        public DateTime BirthDate { get; set; }
        public MaritalStatus? MaritalStatus { get; set; }
        public string Attachment { get; set; }
        public string Observation { get; set; }
        public Address Address { get; set; }
    }
}
