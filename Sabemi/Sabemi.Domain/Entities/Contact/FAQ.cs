﻿using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class FAQ
    {
        public FAQCategory Category { get; set; }
        public FAQSubCategory SubCategory { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
