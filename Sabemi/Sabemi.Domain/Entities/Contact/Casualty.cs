﻿using System;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class Casualty
    {
        public CasualtyType Type { get; set; }
        public DateTime Date { get; set; }
        public string Adress { get; set; }
        public string Description { get; set; }
    }
}
