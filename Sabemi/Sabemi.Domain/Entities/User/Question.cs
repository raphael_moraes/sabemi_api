﻿using Sabemi.CrossCutting.Util;
using System.Collections.Generic;
using System.Linq;

namespace Sabemi.Domain.Entities
{
    public abstract class Question : BaseEntity
    {
        public string Description { get; set; }
        public User User { get; set; }
        public IList<string> Options
        {
            get
            {
                var options = GenerateOptions();
                if (options == null) return null;
                var random = Randomizer.Random;
                return options.OrderBy(o => random.Next()).ToList();
            }
        }
        public abstract bool HasAnswer { get; }

        public Question(string description)
        {
            Description = description;
        }
        protected abstract IList<string> GenerateOptions();
        public abstract bool IsCorrectAnswer(string answer, User user);
    }
}
