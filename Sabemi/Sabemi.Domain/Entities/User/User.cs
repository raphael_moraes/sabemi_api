﻿using System;
using static Sabemi.CrossCutting.Util.Enumerator;

namespace Sabemi.Domain.Entities
{
    public class User : BaseEntity
    {
        #region personal data
        public string Name { get; set; }
        public string CPF { get; set; }
        public string IDNumber { get; set; }
        public string Password { get; set; }
        public DateTime? BirthDate { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public Sex? Sex { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string Hometown { get; set; }
        public MaritalStatus? MaritalStatus { get; set; }
        #endregion

        #region address data
        public string Neighborhood { get; set; }
        public string Street { get; set; }
        public string AddressNumber { get; set; }
        public string Complement { get; set; }
        public string CEP { get; set; }
        public string City { get; set; }
        public State? State { get; set; }
        #endregion

        #region contact data
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public FavoriteContactType? FavoriteContact { get; set; }
        #endregion

        #region extra properties
        public int Age 
        { 
            get
            {
                if (BirthDate == null) return 0;
                return DateTime.Now.Year - BirthDate.Value.Year - (DateTime.Now.DayOfYear < BirthDate.Value.DayOfYear ? 1 : 0);
            }
        }
        #endregion
    }
}
