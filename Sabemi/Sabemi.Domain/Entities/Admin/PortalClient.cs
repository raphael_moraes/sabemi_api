﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities.Admin
{
    public class PortalClient
    {
        [Key]
        public string Num_Cpf { get; set; }
        public bool Sta_Desativacao { get; set; }
    }
}
