﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities.Admin
{
    public class WhiteList
    {
        [Key]
        public string Cod_Id { get; set; }

        [Required(ErrorMessage = "Informe o IP")]
        public string Cod_Ip { get; set; }

        [Required(ErrorMessage = "Informe uma descrição")]
        public string Cod_Descricao { get; set; }
    }
}
