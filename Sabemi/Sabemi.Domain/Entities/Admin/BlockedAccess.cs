﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities.Admin
{
    public class BlockedAccess
    {
        [Key]
        public string Cod_Id { get; set; }
        public string Cod_Ip { get; set; }
        public string Cod_Cpf { get; set; }
        public DateTimeOffset Dta_Activation_Date { get; set; }
        public bool Sta_Ativo { get; set; }
    }
}
