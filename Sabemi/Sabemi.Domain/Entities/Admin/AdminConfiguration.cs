﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities.Admin
{
    public class AdminConfiguration
    {
        [Key]
        public int Cod_Id { get; set; }
        public int Num_Max_Usuarios_Ip { get; set; }
        public int Num_Dias_Verificacao { get; set; }
        public int Num_Dias_Bloqueado { get; set; }
    }
}
