﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities.Admin
{
    public class PortalAccess
    {
        [Key]
        public string Cod_Id { get; set; }
        public string Cod_Ip { get; set; }
        public string Cod_Cpf { get; set; }        
        public DateTimeOffset Dta_Data { get; set; }
        public bool Sta_Bloqueado { get; set; }
        public string Dsc_Status { get; set; }
    }
}
