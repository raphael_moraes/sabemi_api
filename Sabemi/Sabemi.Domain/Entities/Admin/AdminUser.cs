﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sabemi.Domain.Entities.Admin
{
    public class AdminUser
    {
        [Key]
        public string Cod_Id{ get; set; }
        public string Cod_Login { get; set; }
        public string Cod_Nivel_Acesso { get; set; }
        public string Cod_Senha { get; set; }
        public DateTimeOffset Dta_Criacao { get; set; }
        public DateTimeOffset Dta_Atualizacao { get; set; }
        public bool Sta_Ativo { get; set; }
    }
}
