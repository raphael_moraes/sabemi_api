﻿using System;

namespace Sabemi.Domain.Entities
{
    public class CacheData<T>
    {
        public T Data { get; set; }
        public DateTime? ExpiresIn { get; set; }
    }

    public class UserCacheData : CacheData<User>
    {
        public bool IsFullData { get; set; }
    }
}
