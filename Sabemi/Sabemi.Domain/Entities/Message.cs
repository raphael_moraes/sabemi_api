﻿namespace Sabemi.Domain.Entities
{
    public class Message
    {
        public Message(string description)
        {
            Description = description;
        }

        public string Description { get; }
    }
}
